% page 261

\ssection{Relations with the functors $\Ext_S^q$}
\setcounter{subsection}{67}

\ssubsection{The functors $\Ext_S^q$}
\label{section-\arabic{subsection}}

We keep the notations of \no \ref{section-56}. If $M$ and $N$ are two graded $S$-modules,
we denote by $\Hom_S(M, N)_n$ the group of homogeneous $S$-homomorphisms 
of degree $n$ from $M$ to $N$, and by $\Hom_S(M, N)$ the graded group
$\bigoplus_{n\in\Zz} \Hom_S(M, N)_n$; it is a graded $S$-module; when $M$
is of finite type it coincides with the $S$-module of all $S$-homomorphisms
from $M$ to $N$.

The derived functors (cf. \cite{6}, Chapter V) of the functor $\Hom_S(M, N)$
are the functors $\Ext_S^q(M, N)$, $q = 0, 1, \ldots$. Let us briefly recall
their definition:
\footnote{When $M$ is not of finite type, the $\Ext_S^q(M, N)$
defined above can differ from the $\Ext_S^q(M, N)$ defined in \cite{6}: 
it is due to the fact that $\Hom_S(M, N)$ does not have the same meaning
in both cases. However, all the proofs of \cite{6} are valid without change 
in the case considered here: this is seen either directly or by applying 
Appendix of \cite{6}.}  

One chooses a ,,resolution'' of $M$, that is, an exact sequence:
\[ \ldots \to L^{q+1} \to L^q \to \ldots \to L^0 \to M \to 0, \]
where the $L^q$ are free graded $S$-modules and the maps are homomorphisms
(that is, as usual, homogeneous $S$-homomorphisms of degree $0$). If we
set $C^q = \Hom_S(L^q, N)$, the homomorphism $L^{q+1}\to L^q$ defines 
by transposition a homomorphism $d: C^q\to C^{q+1}$ satisfying
$d\circ d = 0$; therefore $C = \bigoplus_{q\geq 0} C^q$ is endowed with
a structure of a complex, and the $q$-th cohomology group of $C$ is just,
by definition, equal to $\Ext_S^q(M, N)$; one shows that it does not depend
on the chosen resolution. As the $C^q$ are graded $S$-modules and since
$d: C^q\to C^{q+1}$ is homogeneous of degree $0$, the $\Ext^q_S(M, N)$ are
$S$-modules graded by the subspaces $\Ext_S^q(M, N)_n)$; the $\Ext_S^q(M, N)$
are the cohomology groups of the complex formed by the $\Hom_S(L^q, N)_n)$,
i.e., are the derived functors of the functor $\Hom_S(M, N)_n)$.

Recall the main properties of $\Ext_S^q$:

$\Ext_S^0(M, N) = \Hom_S(M, N)$; $\Ext_S^q(M, N) = 0$ for $q>r+1$ if $M$ is
of finite type (due to the Hilbert syzygy theorem, cf. \cite{6}, Chapter VIII,
theorem 6.5); $\Ext^q_S(M, N)$ is an $S$-module of finite type if $M$ and $N$
are both of finite type (because we can choose a resolution with the $L^q$
of finite type); for all $n\in\Zz$ we have the canonical isomorphisms:
\[ \Ext_S^q(M(n), N) \isom \Ext_S^q(M, N(-n)) \isom \Ext_S^q(M, N)(-n). \]

The exact sequences:
\[ 0\to N\to N'\to N''\to 0 \quad\text{and}\quad 0\to M\to M'\to M''\to 0 \]
%
% page 262
%
give rise to exact sequences:
\[ \ldots \to \Ext_S^q(M, N) \to \Ext_S^q(M, N') \to \Ext_S^q(M, N'')
	\to \Ext_S^{q+1}(M, N) \to \ldots \]
\[ \ldots \to \Ext_S^q(M'', N) \to \Ext_S^q(M', M)\to \Ext_S^q(M, N)
	\to \Ext_S^{q-1}(M'', N) \to \ldots \]

\ssubsection{Interpretation of $\HH^q_k(M)$ in terms of $\Ext_S^q$}
\label{section-\arabic{subsection}}

Let $M$ be a graded $S$-module and let $k$ be an integer $\geq 0$. Set:
\[ B_k^q(M) = \bigoplus_{n\in\Zz} \HH^q_k(M(n)), \]
with the notations of \no \ref{section-61}.

We thus obtain a graded group, isomorphic with the $q$-th cohomology group
of the complex $\bigoplus_{n\in\Zz} C_k(M(n))$; this complex can be given
a structure of an $S$-module, compatible with the grading by setting
\[ (P\cdot m)\langle i_0\cdots i_q\rangle = P\cdot m\langle i_0\cdots i_q\rangle,
	\text{ if }P\in S_p\text{ and } m\langle i_0\cdots i_q \rangle
	\in C_k^q(M(n)); \]
as the coboundary operator is a homogeneous $S$-homomorphism of degree $0$,
it follows that the $B_k^q(M)$ are themselves graded $S$-modules.

We put
\[ B^q(M) = \lim_{k\to\infty} B_k^q(M) = \bigoplus_{n\in\Zz} \HH^q(M(n)). \]
The $B^q(M)$ are graded $S$-modules. For $q=0$ we have
\[ B^0(M) =  \bigoplus_{n\in\Zz} \HH^0(M(n)), \]
and we recognize the module denoted by $M^\natural$ in \no \ref{section-67} (when $M$ satisfies
the condition (TF)). For each $n\in\Zz$, we have defined in \no \ref{section-62} a linear
map $\alpha: M_n\to \HH^0(M(n))$; we verify immediately that the sum of these
maps defines a homomorphism, which we denote also by $\alpha$, from $M$ to
$B^0(M)$.

{\bf Proposition 1.} \emph{Let $k$ be an integer $\geq 0$ and let $J_k$ be
the ideal $(t_0^k, \ldots, t_r^k)$ of $S$. For every graded $S$-module $M$,
the graded $S$-modules $B_k^q(M)$ and $\Ext_S^q(J_k, M)$ are isomorphic.}

Let $L_k^q$, $q = 0, \ldots, r$ be the free graded $S$-module with a base
consisting of the elements $e\langle i_0\cdots i_q\rangle$, 
$0\leq i_0<i_1<\ldots < i_q\leq r$ of degree $k(q+1)$; we define an
operator $d: L_k^{q+1}\to L_k^q$ and an operator $\varepsilon:L_k^0\to J_k$
by the formulas:
\begin{align*}
 d(e\langle i_0\cdots i_{q+1}\rangle) &= \sum_{j=0}^{j=q+1}
	(-1)^j t^k_{i_j}\cdot e\langle i_0\cdots \hat i_j\cdots i_{q+1}\rangle, \\
  \varepsilon(e\langle i\rangle) &= t_i^k. 
\end{align*}

{\bf Lemma 1.} \emph{The sequence of homomorphisms:
\[ 0\to L_k^r \xto{d} L_k^{r-1} \to \ldots \to L_k^0 \xto{\varepsilon} J_k\to 0 \]
is an exact sequence.}

For $k=1$, this result is well known (cf. \cite{6}, Chapter VIII, \S 4);
the general case is shown in the same way (or reduced to it); we can also
use the theorem shown in \cite{11}.

%
% page 263
%

Proposition 1 follows immediately from the Lemma, if we observe that the complex
formed by the $\Hom_S(L_k^q, M)$ and the transposition of $d$ is just the
complex $\bigoplus_{n\in \Zz} C_k(M(n))$.

{\bf Corollary 1.} \emph{$\HH_k^q(M)$ is isomorphic to $\Ext_S^q(J_k, M)_0$.}

Indeed, these groups are the degree $0$ components of the graded groups
$B_k^q(M)$ and $\Ext_S^q(J_k, M)$.

{\bf Corollary 2.} \emph{$\HH^q(M)$ is isomorphic to $\lim_{k\to\infty} 
\Ext_S^q(J_k, M)_0$.}

We easily see that the homomorphism $\rho_k^h: \HH^q_k(M)\to \HH^q_h(M)$ from
\no \ref{section-61} is transformed by the isomorphism from Corollary 1 to a homomorphism
from
\[ \Ext_S^q(J_k, M)_0 \text{ to } \Ext_S^q(J_h, M)_0 \]
induced by the inclusion $J_h\to J_k$; hence the Corollary 2.

{\bf Remark.} Let $M$ be a graded $S$-module of finite type; $M$ defines
(cf. \no \ref{section-48}) a coherent algebraic sheaf $\Ff'$ on $K^{r+1}$, thus on
$Y = K^{r+1} - \{ 0 \}$ and we can verify that $\HH^q(Y, \Ff')$ is 
isomorphic to $B^q(M)$.

\ssubsection{Definition of the functors $T^q(M)$}
\label{section-\arabic{subsection}}

Let us first define the notion of a \emph{dual module}
to a graded $S$-module. Let $M$ be a graded $S$-module;
for all $n\in\Zz$, $M_n$ is a vector space over $K$, whose
dual vector space we denote by $(M_n)'$. Set
\[ M^\ast = \bigds_{n\in\Zz} M_n^\ast, \quad \text{with}\quad M_n^\ast = (M_{-n})'. \]
We give $M^\ast$ the structure of an $S$-module compatible
with the grading; for all $P\in S_p$, the mapping 
$m\mapsto P\cdot m$ is a $K$-linear map from $M_{-n-p}$
to $M_{-n}$, so defines by transposition a $K$-linear
map from $(M_{-n})' = M_n^\ast$ to $(M_{-n-p})' = M_{n+p}^\ast$ ; this defines the structure of an $S$-module on $M^\ast$.
We could also define $M^\ast$ as $\Hom_S(M, K)$, denoting
by $K$ the $S$-graded module $S/(t_0, \ldots, t_r)$.

The graded $S$-module $M^\ast$ is called the module \emph{dual} to M; we have $M^{\ast\ast}=M$ if every $M_n$ is of finite dimension over $K$, which holds if $M=\Gamma(\Ff)$, $\Ff$ being a coherent algebraic sheaf on $X$, or if $M$ is of finite type. Every homomorphism $\phi:M\to N$ defines by transposition a homomorphism from $N^\ast$ to $M^\ast$. If the sequence $M\to N\to P$ is exact, so is the sequence $P^\ast\to N^\ast\to M^\ast$; in other words, $M^\ast$ is a \emph{contravariant} and \emph{exact} functor of the module $M$. When $I$ is a homogeneous ideal of $S$, the dual of $S/I$ is exactly the ,,inverse system'' of $I$, in the sense of Macaulay (cf. \cite{9}, \no \ref{section-25}).

Let now $M$ be a graded $S$-module and $q$ an integer $\geq 0$. In the preceding \no, we have defined the graded $S$-module $B^q(M)$; the \emph{module dual to $B^q(M)$ will be denoted by $T^q(M)$}. We thus have, by definition:
\[ T^q(M) = \bigds_{n\in\Zz} T^q(M)_n, \quad\text{with}\quad T^q(M)_n = (\HH^q(M(-n)))'. \]

Every homomorphism $\phi:M\to N$ defines a homomorphism from $B^q(M)$ to $B^q(N)$, thus a homomorphism from $T^q(N)$ to $T^q(M)$; thus the $T^q(M)$ are \emph{contravariant} functors of $M$ (we shall see in \no \ref{section-72} that they can
%
% 264
%
expressed very simply in terms of $\Ext_S$). Every exact sequence:
\[ 0\to M\to N\to P\to 0 \]
gives rise to an exact sequence:
\[ \ldots B^q(M) \to B^q(N) \to B^q(P) \to B^{q+1}(M)\to\ldots ,\]
thus, by transposition, an exact sequence:
\[ \ldots T^{q+1}(M) \to T^q(P) \to T^q(N) \to T^{q}(M)\to\ldots .\]

The homomorphism $\alpha: M\to B^0(M)$ defines by transposition a homomorphism $\alpha^\ast: T^0(M)\to M^\ast$.

Since $B^q(M) = 0$ for $q>r$, we have $T^q(M) = 0$ for $q>r$.

\ssubsection{Determination of $T^r(M)$.}
\label{section-\arabic{subsection}}

(In this \no, and in the following, we assume that we have
$r\geq 1$; the case $r = 0$ leads to somehow different, and trivial, statements).

We denote by $\Omega$ the graded $S$-module $S(-r-1)$; this is a free module, with a base consisting of an element of degree $r+1$. We have seen in \no \ref{section-62} that $\HH^r(\Omega) = \HH_k^r(\Omega)$ for $k$ sufficiently large, and that $\HH_k^r(\Omega)$ admits a base over $K$ consisting of a single element $(t_0\ldots t_r)^k/t_0\ldots t_r$; the image in $\HH^r(\Omega)$ of this element will be denoted by $\xi$; $\xi$ is thus a basis of $\HH^r(\Omega)$.

We will now define a scalar product $\langle h,\phi\rangle$ between elements $h\in B^r(M)_{-n}$ and $\phi\in \Hom_S(M, \Omega)_n$, $M$ being an arbitrary graded $S$-module. The element $\phi$ can be identified with an element of $\Hom_S(M(-n), \Omega)_0$, that is, with a homomorphism from $M(-n)$ to $\Omega$; it thus defines, by passing to cohomology groups, a homomorphism from $\HH^r(M(-n)) = B^r(M)_{-n}$ to $\HH^r(\Omega)$, which we also denote by $\phi$. The image of $h$ under this homomorphism is thus a scalar multiple of $\xi$, and we define $\langle h, \phi\rangle$ by the formula:
\[ \phi(h) = \langle h, \phi\rangle \xi .\]

For every $\phi\in\Hom_S(M, \Omega)_n$, the function $h\mapsto \langle h, \phi\rangle$ is a linear form on $B^r(M)_{-n}$, thus can be identified with an element $\nu(\phi)$ of the dual of $B^r(M)_{-n}$, which is $T^r(M)_n$. We have thus
defined a homogeneous mapping of degree $0$
\[ \nu: \Hom_S(M, \Omega)\to T^r(M), \]
and the formula $\langle P\cdot h, \phi\rangle = \langle h, P\cdot \phi\rangle$ shows that $\nu$ is an $S$-homomorphism.

{\bf Proposition 2.} \emph{The homomorphism $\nu: \Hom_S(M,\Omega)\to T^r(M)$ is bijective.}

We shall first prove the Proposition when $M$ is a \emph{free} module. If $M$ is a direct sum of homogeneous submodules $M^\alpha$, we have:
\[ \Hom_S(M, \Omega)_n = \prod_\alpha \Hom_S(M^\alpha, \Omega)_n\quad\text{and}\quad T^r(M)_n = \prod_\alpha T^r(M^\alpha)_n.\]
So, if the proposition holds for the $M^\alpha$, it holds for $M$, and this reduces the case of free modules to the particular case of a free module with a single generator,
%
% page 265
%
that is, to the case when $M = S(m)$. We can identify $\Hom_S(M, \Omega)_n$ with $\Hom_S(S, S(n-m-r-1))_0$, that is, with the vector space of homogeneous polynomials of degree $n-m-r-1$. Thus $\Hom_S(M, \Omega)_n$ has for a base the family of monomials $t_0^{\gamma_0}\ldots t_r^{\gamma_r}$ with $\gamma_i\geq 0$ and $\sum_{i=0}^{i=r} \gamma_i = n - m - r - 1$.
On the other hand, we have seen in \no \ref{section-62} that $\HH^r_k(S(m-n))$ has for a base (if $k$ is large enough) the family of monomials $(t_0\ldots t_r)^k/t_0^{\beta_0}\ldots t_r^{\beta_r}$ with $\beta_i>0$ and $\sum_{i=0}^{i=r} \beta_i = n-m$. By setting $\beta_i = \gamma'_i + 1$, we can write these monomials in the form $(t_0\ldots t_r)^{k-1}/t_0^{\gamma'_0}\ldots t_r^{\gamma'_r}$, with $\gamma'_i\geq 0$ and $\sum_{i=0}^{i=r} \gamma'_i = n - m - r - 1$. Comparing the definition of $\langle h,\phi\rangle$, we observe that the scalar product
\[ \langle (t_0\ldots t_r)^{k-1}/t_0^{\gamma'_0}\ldots t_r^{\gamma'_r},  t_0^{\gamma_0}\ldots t_r^{\gamma_r} \rangle \]
is always zero, unless $\gamma_i = \gamma'_i$ for all $i$, in which case it is equal to $1$. This means that $\nu$ transforms the basis of $t_0^{\gamma_0}\ldots t_r^{\gamma_r}$
to the dual basis of $(t_0\ldots t_r)^{k-1}/t_0^{\gamma'_0}\ldots t_r^{\gamma'_r}$, thus is bijective, which shows the Proposition in the case when $M$ is free.

Let us now pass to the general case. We choose an exact sequence 
\[ L^1\to L^0 \to M\to 0\]
where $L^0$ and $L^1$ are free. Consider the following
commutative diagram
\[
\begin{tikzpicture}[description/.style={fill=white,inner sep=2pt}]
    \matrix (m) [matrix of math nodes, row sep=3em,
    column sep=2.5em, text height=1.5ex, text depth=0.25ex]
    {   0 & \Hom_S(M, \Omega) & \Hom_S(L^0, \Omega) & \Hom_S(L^1, \Omega) \\
        0 & T^r(M) & T^r(L^0) & T^r(L^1). \\ };
    \path[->,font=\scriptsize]
    (m-1-1) edge node[auto] {$ $} (m-1-2)
    (m-1-2) edge node[auto] {$ $} (m-1-3)
    (m-1-3) edge node[auto] {$ $} (m-1-4);
    \path[->,font=\scriptsize]
    (m-2-1) edge node[auto] {$ $} (m-2-2)
    (m-2-2) edge node[auto] {$ $} (m-2-3)
    (m-2-3) edge node[auto] {$ $} (m-2-4);
    \path[->,font=\scriptsize]
    (m-1-1) edge node[auto] {$ \nu $} (m-2-1);
    \path[->,font=\scriptsize]
    (m-1-2) edge node[auto] {$ \nu $} (m-2-2);
    \path[->,font=\scriptsize]
    (m-1-3) edge node[auto] {$ \nu $} (m-2-3);
    \path[->,font=\scriptsize]
    (m-1-4) edge node[auto] {$ \nu $} (m-2-4);
\end{tikzpicture}
\]
The first row of this diagram is an exact sequence, by the general properties of the functor $\Hom_S$; the second is also exact, because it is dual to the sequence
\[ B^r(L^1) \to B^r(L^0) \to B^r(M) \to 0, \]
which is exact by the cohomology exact sequence of $B^q$ and the fact that $B^{r+1}(M) = 0$ for any $M$. On the other hand, the two vertical homomorphisms
\[ \nu: \Hom_S(L^0, \Omega)\to T^r(L^0)\quad\text{and}\quad \nu: \Hom_S(L^1, \Omega) \to T^r(L^1) \]
are bijective, as we have just seen. It follows that 
\[ \nu : \Hom_S(M, \Omega) \to T^r(M) \]
is also bijective, which completes the proof.

\ssubsection{Determination of $T^q(M)$.}
\label{section-\arabic{subsection}}

We shall now prove the following theorem, which generalizes Proposition 2:

{\bf Theorem 1.} \emph{Let $M$ be a graded $S$-module. For $q\neq r$, the graded $S$-modules $T^{r-q}(M)$ and $\Ext_S^q(M, \Omega)$ are isomorphic. Moreover, we have an exact sequence:}
\[ 0 \to \Ext_S^r(M,\Omega)\to T^0(M) \xto{\alpha^\ast} M^\ast \to \Ext_S^{r+1}(M, \Omega) \to 0. \]

% 
% page 266
%

We will use the axiomatic characterization of derived functors given in \cite{6}, Chap. III, \S 5. For this, we first define new functors $E^q(M)$ in the following manner:
\begin{align*}
&\text{For }q\neq r, r+1,& E^q(M) &= T^{r-q}(M), \\
&\text{For } q=r, & E^r(M) &= \Ker(\alpha^\ast),  \\
&\text{For } q=r+1, & E^{r+1}(M) &= \Coker(\alpha^\ast).
\end{align*}
The $E^q(M)$ are additive functors of $M$, enjoying the following properties:

{\bf (i)} \emph{$E^0(M)$ is isomorphic to $\Hom_S(M, \Omega)$.}

\noindent This follows from Proposition 2.

{\bf (ii)} \emph{If $L$ is free, $E^q(L) = 0$ for $q>0$.}

\noindent It suffices to verify this for $L = S(n)$, in which case it follows from \no \ref{section-62}.

{\bf (iii)} \emph{To every exact sequence $0\to M\to N\to P\to 0$ there is associated a sequence of coboundary operators $d^q: E^q(M)\to E^{q+1}(P)$ and the sequence:
\[ \ldots E^q(P) \to E^q(N) \to E^q(M) \xto{d^q} E^{q+1}(P)\to \ldots \]
is exact. }

\noindent The definition of $d^q$ is obvious if $q\neq r-1, r$: this is the homomorphism from $T^{r-q}(M)$ to $T^{r-q-1}(P)$ defined in \no \ref{section-70}. For $q = r-1$ or $r$, we use the following commutative diagram:
\[
\begin{tikzpicture}[description/.style={fill=white,inner sep=2pt}]
    \matrix (m) [matrix of math nodes, row sep=3em,
    column sep=2.5em, text height=1.5ex, text depth=0.25ex]
    {   T^1(M) & T^0(P) & T^0(N) & T^0(M) & 0  \\
        0 & P^\ast & N^\ast & M^\ast & 0.  \\ };
    \path[->,font=\scriptsize]
    (m-1-1) edge node[auto] {$ $} (m-1-2)
    (m-1-2) edge node[auto] {$ $} (m-1-3)
    (m-1-3) edge node[auto] {$ $} (m-1-4)
    (m-1-4) edge node[auto] {$ $} (m-1-5);
    \path[->,font=\scriptsize]
    (m-2-1) edge node[auto] {$ $} (m-2-2)
    (m-2-2) edge node[auto] {$ $} (m-2-3)
    (m-2-3) edge node[auto] {$ $} (m-2-4)
    (m-2-4) edge node[auto] {$ $} (m-2-5);
    \path[->,font=\scriptsize]
    (m-1-2) edge node[auto] {$ \alpha^\ast $} (m-2-2);
    \path[->,font=\scriptsize]
    (m-1-3) edge node[auto] {$ \alpha^\ast $} (m-2-3);
    \path[->,font=\scriptsize]
    (m-1-4) edge node[auto] {$ \alpha^\ast $} (m-2-4);
    \path[->,font=\scriptsize]
    (m-1-5) edge node[auto] {$ \alpha^\ast $} (m-2-5);
\end{tikzpicture}
\]

This diagram shows immediately that the image of $T^1(M)$ is contained in the kernel of $\alpha^\ast: T^0(P)\to P^\ast$, which is just $E^r(P)$. This defines $d^{r-1}: E^{r-1}(M)\to E^r(P)$.

To define $d^r: \Ker(T^0(M)\to M^\ast)\to \Coker(T^0(P)\to P^\ast)$, we use the process from \cite{6}, Chap. III, Lemma 3.3: if $x\in \Ker(T^0(M)\to M^\ast)$, there exists $y\in P^\ast$ and $z\in T^0(N)$ such that $x$ is the image of $z$ and that $y$ and $z$ have the same image in $N^\ast$; we then set $d^r(x) = y$.

The exactness of the sequence
\[ \ldots \to E^q(P) \to E^q(N) \to E^q(M) \xto{d^q} E^{q+1}(P) \to \ldots \]
follows from the exactness of the sequence
\[ \ldots T^{r-q}(P)\to T^{r-q}(N) \to T^{r-q}(M) \to T^{r-q-1}(P)\to \ldots \]
and from \cite{6}, loc. cit.

{\bf (iv)} \emph{The isomorphism from (i) and the operators $d^q$ from (iii) are ,,natural''} 

\noindent This follows immediately from the definitions.

%
% page 267
%

As the properties (i) to (iv) characterize the derived functors of the functor $\Hom_S(M, \Omega)$, we have $E^q(M) \isom \Ext_S^q(M, \Omega)$, which proves the Theorem.

{\bf Corollary 1.} \emph{If $M$ satisfies (TF), $\HH^q(M)$ is isomorphic to the vector space dual to $\Ext_S^{r-q}(M, \Omega)_0$ for all $q\geq 1$. }

In fact, we know that $\HH^q(M)$ is a vector space of finite dimension, whose dual is isomorphic to $\Ext_S^{r-q}(M, \Omega)_0$.

{\bf Corollary 2.} \emph{If $M$ satisfies (TF), the $T^q(M)$ are graded $S$-modules of finite type for $q\geq 1$, and $T^0(M)$ satisfies (TF).}

We can replace $M$ by a module of finite type without changing the $B^q(M)$, thus $T^q(M)$. The $\Ext_S^{r-q}(M, \Omega)$ are then $S$-modules of finite type, and we have $M^\ast\in\Ca$, hence the Corollary.



