\noindent
From now on, $K$ denotes a commutative algebraically closed field of arbitrary 
characteristic.

% page 223

\ssection{Algebraic varieties}
\setcounter{subsection}{29}

\ssubsection{Spaces satisfying condition (A)}
\label{section-\arabic{subsection}}

Let $X$ be a topological space. The condition (A) is the following:\\
\wciecie (A) --- \emph{Any decreasing sequence of closed subsets of $X$ is stationary.}

In other words, if we have $F_1\supset F_2\supset F_3\supset\ldots $, $F_i$ being
closed in $X$, there exists an integer $n$ such that $F_m=F_n$ for $m\geq n$. Or:\\
\wciecie (A') --- \emph{The set of closed subsets of $X$, ordered by inclusion,
satisfies the minimality condition}

{\bf Examples.} Equip a set $X$ with the topology where the closed subsets are
the finite subsets of $X$ and the whole $X$; the condition (A) is then satisfied.
More generally, any algebraic variety, equipped with Zariski topology, satisfies
(A) (cf. \no \ref{section-34}).

{\bf Proposition 1.} \emph{(a) If $X$ satisfies the condition (A), then $X$
is quasi-compact, \\
\wciecie (b) If $X$ satisfies (A), any subspace of $X$ satisfies it also.\\
\wciecie (c) If $X$ is a finite union of $Y_i$, the $Y_i$ satisfying (A),
then $X$ also satisfies (A). }

If $F_i$ is a filtering decreasing set of closed subsets of $X$, and if
$X$ satisfies (A'), then there exists an $F_i$ contained in all others;
if $\bigcap F_i=\emptyset$, there is therefore an $i$ such that $F_i=\emptyset$,
which shows (a).

Let $G_1 \supset G_2 \supset G_3 \supset \ldots$ be a decreasing sequence of
closed subsets of a subspace $Y$ of $X$; if $X$ satisfies (A), there exists
an $n$ for which $\bar G_m = \bar G_n$ for $m\geq n$, hence $G_m = Y\cap \bar G_m
= Y\cap \bar G_n = G_n$, which shows (b).

Let $F_1\supset F_2 \supset F_3 \supset \ldots $ be a decreasing sequence
of closed subsets of a space $X$ satisfying (c); since all $Y_i$ satisfy
(A), there exists for all $i$ an $n_i$ such that $F_m \cap Y_i = F_{n_i}\cap Y_i$
for $m\geq n_i$; if $n = Sup(n_i)$, we then have $F_m = F_n$ for $m\geq n$, 
which shows (c).

A space $X$ is said to be \emph{irreducible} if it is not a union of two
closed subspaces, distinct from $X$ itself; or equivalently, if any two
non-empty open subsets have a non-empty intersection. Any finite family
of non-empty open subsets of $X$ then has a non-empty intersection, and any
open subset of $X$ is also irreducible.

{\bf Proposition 2.} \emph{Any space $X$ satisfying the condition (A) is a union
of a finite number of irreducible closed subsets $Y_i$. If we suppose that
that $Y_i$ is not contained in $Y_j$ for any pair $(i, j)$, $i\neq j$, the
set of $Y_i$ is uniquely determined by $X$; the $Y_i$ are then called the
irreducible components of $X$.}

The existence of a decomposition $X = \bigcup Y_i$ follows immediately from (A).
If $Z_k$ is another such decomposition of $X$, we have $Y_i = \bigcup Y_i\cap Z_k$,
and, since $Y_i$ is irreducible, this implies of an index $k$ such that 
$Z_k \supset Y_i$; interchanging the roles of $Y_i$ and $Z_k$, we conclude 
analogously that there exists an index $i'$ for which $Y_{i'} \supset Z_k$;
thus $Y_i\subset Z_k \subset Y_{i'}$, which by the assumption made on $Y_i$
leads to $i = i'$ and $Y_i = Z_k$, hence the uniqueness of the decomposition.

{\bf Proposition 3.} \emph{Let $X$ be a topological space that is a finite union
of non-empty open subsets $V_i$. Then $X$ is irreducible if and only if all $V_i$
are irreducible and $V_i\cap V_j\neq 0$ for all pairs $(i, j)$.}

% page 224 

The necessity of these conditions was noted above; we show that they are sufficient.
If $X = Y\cup Z$, where $Y$ and $Z$ are closed, we have $V_i = (V_i\cap Y)\cup
(V_i\cap Z)$, which shows that each $V_i$ is contained either in $Y$ or in $Z$.
Suppose that $Y$ and $Z$ are distinct from $X$; we can then find two indices
$i, j$ such that $V_i$ is not contained in $Y$ and $V_j$ is not contained in $Z$;
according to our assumptions on $Y_i$,
we then have $V_i\subset Z$ and $V_j \subset Y$. Set $T = V_j - V_i\cap V_j$; $T$
is closed in $V_j$ and we have $V_j = T \cup (Z \cap V_j)$; as $V_j$ is irreducible,
it follows that either $T = V_j$, which means that $V_i\cap V_j = \emptyset$, 
or $Z\cap V_j = V_j$, which means that $V_j \subset Z$, and in both cases
this leads to a contradiction, q.e.d.

\ssubsection{Locally closed subsets of an affine space}
\label{section-\arabic{subsection}}

Let $r$ be an integer $\geq 0$ and let $X = K^r$ be the \emph{affine space}
of dimension $r$ over the field $K$. We equip $X$ with the \emph{Zariski topology};
recall that a subset of $X$ is closed in this topology if it is the zero set
of a family of polynomials $P^\alpha\in K[X_1, \ldots, X_r]$. Since the ring
of polynomials is Noetherian, $X$ satisfies the condition (A) from the preceding
\no. Moreover, one easily shows that $X$ is an irreducible space.

If $x = (x_1, \ldots, x_r)$ is a point of $X$, we denote by $\Oo_x$ the
\emph{local ring} of $x$; recall that this is the subring of the field
$K(X_1, \ldots, X_r)$ consisting of those fractions which can be
put in the form:\\
\wciecie $R = P/Q$, where $P$ and $Q$ are polynomials and $Q(x)\neq 0$.

Such a fraction is said to be \emph{regular} in $x$; for all points $x\in X$
for which $Q(x)\neq 0$, the function $x\mapsto P(x)/Q(x)$ is a continuous
function with values in $K$ ($K$ being given the Zariski topology) which can
be identified with $R$, the field $K$ being infinite. The $\Oo_x$, $x\in X$
thus form a subsheaf $\Oo$ of the sheaf $\Ff(X)$ of germs of functions
on $X$ with values in $K$ (cf. \no \ref{section-3}); the sheaf $\Oo$ is a sheaf of rings.

We will extend the above to locally closed subspaces of $X$ (we call a subset
of a space $X$ \emph{locally closed} in $X$ if it is an intersection of
a open subset with a closed subset of $X$). Let $Y$ be such a subspace and
let $\Ff(Y)$ be the sheaf of germs of functions on $Y$ with values in $K$;
if $x$ is a point of $Y$, the operation of restriction defines a canonical
homomorphism
\[ \varepsilon_x : \Ff(X)_x \to \Ff(Y)_x . \]
The image of $\Oo_x$ under $\varepsilon_x$ is a subring of $\Ff(Y)_x$ which
we denote by $\Oo_{x, Y}$; the $\Oo_{x, Y}$ form a subsheaf $\Oo_Y$ of $\Ff(Y)$,
which we call the \emph{sheaf of local rings} of $Y$. A section of $\Oo_Y$
over an open subset $V$ of $Y$ is thus, by definition, a function $f:V\to K$
which is equal, in the neighborhood of any point $x\in V$, to a restriction
to $V$ of a rational function regular at $x$; such a function is said to be
\emph{regular} on $V$; it is a continuous function if we equip $V$ with the 
induced topology and $K$ with the Zariski topology. The set of regular functions
at all points of $V$ is a ring, the ring $\Gamma(V, \Oo_Y)$; observe also that,
if $f\in \Gamma(V, \Oo_x)$ and if $f(x)\neq 0$ for all $x\in V$, then $1/f$
also belongs to $\Gamma(V, \Oo_Y)$.

% page 225

We can characterize the sheaf $\Oo_Y$ in another way:

{\bf Proposition 4.} \emph{Let $U$ (resp. $F$) be a open (resp. closed) subspace of $X$
and let $Y= U\cap F$. Let $I(F)$ be the ideal $K[X_1, \ldots, X_r]$ consisting
of polynomials vanishing on $F$. If $x$ is a point of $Y$, the kernel of the
surjection $\varepsilon_x: \Oo_x\to \Oo_{x, Y}$ coincides with the ideal
$I(F)\cdot \Oo_x$ of $\Oo_x$.}

It is clear that each element of $I(F)\cdot\Oo_x$ belongs to the kernel of 
$\varepsilon_x$. Conversely, let $R=P/Q$ be an element of the kernel, $P$ and
$Q$ being two polynomials with $Q(x)\neq 0$. By assumption, there exists an
open neighborhood $W$ of $x$ such that $P(y)=0$ for all $y\in W\cap F$; let
$F'$ be the complement of $W$, which is closed in $X$; since $x\in F'$,
there exists, by the definition of the Zariski topology, a polynomial $P_1$
vanishing on $F'$ and nonzero at $x$; the polynomial $P\cdot P_1$ belongs
to $I(F)$ and we can write $R = P\cdot P_1/Q\cdot P_1$, which shows that
$R\in I(F)\cdot \Oo_x$.

{\bf Corollary.} \emph{The ring $\Oo_{x, Y}$ is isomorphic to the localization
of $K[X_1, \ldots, X_r]/I(F)$ in the maximal ideal defined by the
point $x$.}

This follows immediately from the construction of localization a quotient ring
(cf. for example \cite{8}, Chap. XV, \S 5, th. XI).

\ssubsection{Regular functions}
\label{section-\arabic{subsection}}

Let $U$ (resp. $V$) be a locally closed subspace of $K^r$ (resp. $K^s$). A function
$\phi: U\to V$ is said to be \emph{regular} on $U$ (or simply regular) if:\\
\wciecie $\phi$ is continuous,\\
\wciecie If $x\in U$ and $f\in \Oo_{\phi(x), V}$ then $f\circ\phi\in\Oo_{x, U}$.

Denote the coordinates of the point $\phi(x)$ by $\phi_i(x)$, $1\leq i\leq s$.
We then have:

{\bf Proposition 5.} \emph{A map $\phi: U\to V$ is regular on $U$ if and only
if $\phi_i:U\to K$ are regular on $U$ for all $i$, $1\leq i\leq s$.}

As the coordinate functions are regular on $V$, the condition is necessary.
Conversely, suppose that we have $\phi_i\in \Gamma(U, \Oo_U)$ for each $i$;
if $P(X_1, \ldots, X_s)$ is a polynomial, the function $P(\phi_1, \ldots, \phi_s)$
belongs to $\Gamma(U, \Oo_U)$ since $\Gamma(U, \Oo_U)$ as a ring; it follows that
it is a continuous function on $U$, thus its zero set is closed, which shows
the continuity of $\phi$. If we have $x\in U$ and $f\in \Oo_{\phi(x), V}$,
we can write $f$ locally in the form $f=P/Q$, where $P$ and $Q$ are polynomials
and $Q(\phi(x))\neq 0$. The function $f\circ\phi$ is then equal to 
$P\circ\phi/Q\circ\phi$ in a neighborhood of $x$; from what we gave seen,
$P\circ\phi$ and $Q\circ\phi$ are regular in a neighborhood of $x$. As
$Q\circ\phi(x)\neq 0$, it follows that $f\circ \phi$ is regular in a 
neighborhood of $x$, q.e.d.

A \emph{composition} of two regular maps is regular. A bijection $\phi:U\to V$
is called a \emph{biregular isomorphism} (or simply an isomorphism) if
$\phi$ and $\phi^{-1}$ are regular; or equivalently, if $\phi$ is a homeomorphism
of $U$ to $V$ which transforms the sheaf $\Oo_U$ into the sheaf $\Oo_V$. 

\ssubsection{Products}
\label{section-\arabic{subsection}}

If $r$ and $r'$ are two nonnegative integers, we identify the affine space
$K^{r+r'}$ with the product $K^r\times K^{r'}$. The Zariski topology on
$K^{r+r'}$ is \emph{finer} than the product of the Zariski topologies on
$K^r$ and $K^{r'}$; it is even
%
% page 226
%
strictly finer if $r$ and $r'$ are positive.
In result, if $U$ and $U'$ are locally closed subspaces of $K^r$ and $K^{r'}$,
$U\times U'$ is a locally closed subspace of $K^{r+r'}$ and the sheaf
$\Oo_{U\times U'}$ is well defined.

On the other hand, let $W$ be a locally closed subspace of $K^t$, $t\geq 0$
and let $\phi:W\to U$ and $\phi': W\to U'$ be two maps. As an immediate
result of Proposition 5 we have:

{\bf Proposition 6.} \emph{A map $x\to (\phi(x), \phi'(x))$ is regular
from $W$ to $U\times U'$ if and only if $\phi$ and $\phi'$ are regular.}

As any \emph{constant} function is regular, the preceding Proposition
shows that any \emph{section} $x\mapsto (x, x_0')$, $x_0'\in U'$ is
a regular function from $U$ to $U\times U'$; on the other hand, the
\emph{projections} $U\times U'\to U$ and $U\times U'\to U'$ are obviously
regular.

Let $V$ and $V'$ be locally closed subspaces of $K^s$ and $K^{s'}$
and let $\psi: U\to V$ and $\psi':U'\to V'$ be two mappings. The preceding
remarks, together with Proposition 6, show that we then have
(cf. \cite{1}, Chap. IV):

{\bf Proposition 7.} \emph{A map $\psi\times\psi': U\times U'\to V\times V'$
is regular if and only if $\psi$ and $\psi'$ are regular.}

Hence:

{\bf Corollary.} \emph{A map $\psi\times\psi'$ is a biregular isomorphism
if and only if $\psi$ and $\psi'$ are biregular isomorphisms.}

\ssubsection{Definition of the structure of an algebraic variety}
\label{section-\arabic{subsection}}

{\bf Definition.} \emph{We call an algebraic variety over $K$ (or simply an
algebraic variety) a set $X$ equipped with:\\
\wciecie 1$^\circ$ a topology, \\
\wciecie 2$^\circ$ a subsheaf $\Oo_x$ of the sheaf $\Ff(X)$ of germs of
functions on $X$ with values in $K$,\\
this data being subject to axioms ($VA_I$) and ($VA_{II}$) 
stated below.}

First note that if $X$ and $Y$ are equipped with two structures of the above
type, we have a notion of \emph{isomorphism} of $X$ and $Y$: it is a homeomorphism
of $X$ to $Y$ which transforms $\Oo_X$ to $\Oo_X$. On the other hand, if $X'$
is an open subset of $X$, we can equip $X'$ with the induced topology and
the induced sheaf: we have a notion of an \emph{induced structure} on an open
subset. That being said, we can state the axiom ($VA_I$):

	{\bf ($VA_I$)} --- \emph{There exists a finite open covering 
$\Bb = \{V_i\}_{i\in I}$ of the space $X$ such that each $V_i$, equipped
with the structure induced from $X$, is isomorphic to a locally closed
subspace $U_i$ of an affine space, equipped with the sheaf $\Oo_{U_i}$ defined
in \no \ref{section-31}.}

To simplify the language, we call an \emph{prealgebraic variety} a topological
space $X$ together with a sheaf $\Oo_X$ satisfying the axiom ($VA_I$). An
isomorphism $\phi_i:V_i\to U_i$ is called a \emph{chart} of the open subset 
$V_i$; the condition ($VA_I$) means that it is possible to cover $X$ with
finitely many open subsets possessing charts. Proposition 1 from \no \ref{section-30}
shows that $X$ satisfies condition (A), thus it is quasi-compact and so
are its subspaces.

% page 227

The topology on $X$ is called the ,,Zariski topology'' and the sheaf $\Oo_X$
is called the \emph{sheaf of local rings} of $X$.

{\bf Proposition 8.} \emph{Let $X$ be a set covered by a finite family of subsets
$X_j$, $j\in J$. Suppose that each $X_j$ is equipped with a structure of
a prealgebraic variety and that the following conditions are satisfied: \\
\wciecie (a) $X_i\cap X_j$ is open in $X_i$ for all $i, j\in J$, \\
\wciecie (b) the structures induced by $X_i$ and $X_j$ on $X_i\cap X_j$ coincide
for all $i, j\in J$. \\
Then there exists a unique structure of a prealgebraic variety on $X$ such that
$X_j$ are open in $X$ and such that the structure induced on each $X_i$ is
the given structure.}

The existence and uniqueness of the topology on $X$ and the sheaf $\Oo_X$ are
immediate; it remains to check that this topology and this sheaf satisfy
($VA_I$), which follows from the fact that $X_j$ form a finite family and
satisfy ($VA_I$).

{\bf Corollary.} \emph{Let $X$ and $X'$ be two prealgebraic varieties. There
exists a structure of a prealgebraic variety on $X\times X'$ satisfying the
following condition: If $\phi: V\to U$ and $\phi':V'\to U'$ are charts
($V$ being open in $X$ and $V'$ being open in $X'$), then $V\times V'$
is open in $X\times X'$ and $\phi\times\phi':V\times V'\to U\times U'$ 
is a chart.}

Cover $X$ by a finite number of open $V_i$ having charts $\phi_i: V_i\to U_i$
and let $(V_j', U_j', \phi_j')$ be an analogous system for $X'$. The set 
$X\times X'$ is covered by $V_i\times V_j'$; equip each $V_i\times V_j'$
with the structure of a prealgebraic variety induced from $U_i\times U_j'$
by $\phi_i^{-1}\times \phi_j'^{-1}$; the assumptions (a) and (b) of
Proposition 8 are satisfied for this covering of $X\times X'$, by the
corollary of Proposition 7. We obtain a structure of a prealgebraic variety
on $X\times X'$ which satisfies appropriate conditions.

We can apply the preceding corollary to the particular case $X'=X$; so
$X\times X$ has a structure of a prealgebraic variety, and in particular
a topology. We can now state the axiom ($VA_{II}$):

{\bf ($VA_{II}$)} --- \emph{The diagonal $\Delta$ of $X\times X$ is closed
in $X\times X$.}

Suppose that $X$ is a prealgebraic variety obtained by the ,,gluing'' procedure
of Proposition 8; then the condition ($VA_{II}$) is satisfied if and only if
$X_{ij} = \Delta\cap X_i\times X_j$ is closed in $X_i\times X_j$. Or $X_{ij}$
is the set of $(x, x)$ for $x\in X_i\cap X_j$. Suppose that there exist
charts $\phi: X_i\to U_i$ and let $T_{ij} = \phi\times\phi_j(X_{ij})$; $T_{ij}$
is the set of $(\phi_i(x), \phi_j(x))$ for $x$ running over $X_i\cap X_j$.
The axiom ($VA_{II}$) takes therefore the following form:

{\bf ($VA'_{II}$)} --- \emph{For each pair $(i, j)$, $T_{ij}$ is closed in
$U_i\times U_j$.}

In this form we recognize Weil's axiom (A) (cf. \cite{16}, p. 167), except that
Weil considered only irreducible varieties.

{\bf Examples} of algebraic varieties: Any locally closed subspace $U$ of
an affine space, equipped with the induced topology and the sheaf $\Oo_U$
defined in \no \ref{section-31} is an algebraic variety. Any projective variety is an
algebraic variety (cf. \no \ref{section-51}). Any algebraic fiber space (cf. \cite{17}) whose
base and fiber are algebraic varieties is an algebraic variety.

% page 228

{\bf Remarks.} (1) We observe an analogy between condition ($VA_{II}$) and
the condition of \emph{separatedness} imposed on topological, differential
and analytic varieties.\\
\wciecie (2) Simple examples show that condition ($VA_{II}$) is not a 
consequence of condition ($VA_I$).

\ssubsection{Regular mappings, induced structures, products}
\label{section-\arabic{subsection}}

Let $X$ and $Y$ be two algebraic varieties and let $\phi$ be a function from
$X$ to $Y$. We say that $\phi$ is \emph{regular} if: \\
\wciecie (a) $\phi$ is continuous.\\
\wciecie (b) If $x\in X$ and $f\in \Oo_{\phi(x), Y}$ then $f\circ \phi\in\Oo_{x,X}$.

As in \no \ref{section-32}, the composition of two regular functions is regular and a bijection
$\phi:X\to Y$ is an isomorphism if and only if $\phi$ and $\phi^{-1}$ are regular
functions. Regular functions form a family of \emph{morphisms} for the structure
of an algebraic variety in the sense of \cite{1}, Chap. IV.

Let $X$ be an algebraic variety and let $X'$ be a locally closed subspace of $X$.
We equip $X'$ with the topology induced from $X$ and the sheaf $\Oo_{X'}$ induced
by $\Oo_X$ (to be precise, for all $x\in X'$ we define $\Oo_{x, X'}$ as the image
of $\Oo_{x, X}$ under the canonical homomorphism $\Ff(X)_x\to\Ff(X')_x$). The
axiom ($VA_I$) is satisfied: if $\phi_i: V_i\to U_i$ is a system of charts such
that $X = \bigcup V_i$, we set $V_i' = X'\cap V_i$, $U_i' = \phi_i(V_i')$ 
and $\phi_i : V_i'\to U_i'$ is a system of charts such that $X' = \bigcup V_i'$.
The axiom ($VA_{II}$) is satisfied as well since the topology of $X'\times X'$ 
is induced from $X\times X$ (we could also use ($VA'_{II}$)). We define
the structure of an algebraic variety on $X'$ which is \emph{induced} by that
of $X$; we also say that $X'$ is a \emph{subvariety} of $X$ (in Weil \cite{16}, the term
,,subvariety'' is reserved for what we call here an irreducible closed subvariety).
If $\iota$ denotes the inclusion of $X'$ in $X$, $\iota$ is a regular mapping;
moreover, if $\phi$ is a function from an algebraic variety $Y$ to $X'$ then
$\phi: Y\to X'$ is regular if and only if $\iota\circ\phi: Y\to X$ is regular
(which justifies the term ,,induced structure'', cf. \cite{1}, loc. cit.).

If $X$ and $X'$ are two algebraic varieties, $X\times X'$ is an algebraic variety,
called the \emph{product variety}; it suffices to check that the axiom ($VA'_{II}$)
is satisfied, in other words, that if $\phi_i : V_i\to U_i$ and $\phi_i':V_i'\to
U_i'$ are systems of charts such that $X = \bigcup V_i$ and $X' = \bigcup V_i'$,
then the set $T_{ij}\times T'_{i'j'}$ is closed in $U_i\times U_j\times U'_{i'}
\times V'_{j'}$ (with the notations of \no \ref{section-34}); this follows immediately
from the fact that $T_{ij}$ and $T'_{i'j'}$ are closed in $U_i\times U_j$
and $U'_{i'}\times U'_{j'}$ respectively.

Propositions 6 and 7 are valid without change for arbitrary algebraic varieties.

If $\phi: X\to Y$ is a regular mapping, the graph $\Phi$ of $\phi$ is \emph{closed}
in $X\times Y$, because it is the inverse image of the diagonal $Y\times Y$
by $\phi\times 1 : X\times Y \to Y \times Y$; moreover, the mapping $\psi: X\to \Phi$
defined by $\psi(x) = (x, \phi(x))$ is an isomorphism: indeed, $\psi$ is a regular
mapping, and so is $\psi^{-1}$ (since it is a restriction of the projection
$X\times Y\to X$).

\ssubsection{The field of rational functions on an irreducible variety}
\label{section-\arabic{subsection}}

We first show two lemmas of purely topological nature:

% page 229

{\bf Lemma 1.} \emph{Let $X$ be a connected space, $G$ an abelian group and $\Gg$
a constant sheaf on $X$ isomorphic to $G$. The canonical mapping $G\to \Gamma(X, \Gg)$
is bijective.}

An element of $\Gamma(X, \Gg)$ is just a continuous mapping from $X$ to $G$
equipped with the discrete topology. Since $X$ is connected, any such a mapping
is constant, hence the Lemma.

We call a sheaf $\Ff$ on a space $X$ \emph{locally constant} if any point
$x$ has an open neighborhood $U$ such that $\Ff(U)$ is constant on $U$.

{\bf Lemma 2.} \emph{Any locally constant sheaf on an irreducible space is constant.}

Let $\Ff$ be a sheaf, $X$ a space and set $F = \Gamma(X, \Ff)$; it suffices to
demonstrate that the canonical homomorphism $\rho_x: F\to \Ff_x$ is bijective
for all $x\in X$, because we would thus obtain an isomorphism of the constant
sheaf isomorphic to $F$ with the given sheaf $\Ff$.

If $f\in F$, the set of points $x\in X$ such that $f(x)=0$ is open (by the general
properties of sheaves) and closed (because $\Ff$ is locally constant); since
an irreducible space is connected, this set is either $\emptyset$ or $X$, which
shows that $\rho_x$ is injective.

Now take $m\in \Ff_x$ and let $s$ be a section of $\Ff$ over a neighborhood
$U$ of $x$ such that $s(x)=m$; cover $X$ by nonempty open subsets $U_i$ such that
$\Ff(U_i)$ is constant on $U_i$; since $X$ is irreducible, we have $U\cap U_i
\neq \emptyset$; choose a point $x_i U\cap U_i$; obviously there exists a section
$s_i$ of $\Ff$ over $U_i$ such that $s_i(x_i) = s(x_i)$, and since the sections
$s$ and $s_i$ coincide in $x_i$, they coincide on whole $U\cap U_i$, since
$U\cap U_i$ is irreducible, hence connected; analogously $s_i$ and $s_j$
coincide on $U_i\cap U_j$, since they coincide on $U\cap U_i\cap U_j\neq \emptyset$;
thus the sections $s_i$ define a unique section $s$ of $\Ff$ over $X$ and we have
$\rho_x(s) = m$, which ends the proof.

Now let $X$ be an irreducible algebraic variety. If $U$ is a nonempty open subset
of $X$, set $\Aa_U = \Gamma(U, \Oo_X)$; $\Aa_U$ is an \emph{integral domain}:
indeed, suppose that we have $f\cdot g = 0$, $f$ and $g$ being regular functions
from $U$ to $K$; if $F$ (resp. $G$) denotes the set of $x\in U$ such that
$f(x) = 0$ (resp. $g(x) = 0$), we have $U = F\cup G$ and $F$ and $G$ are closed
in $U$, because $f$ and $g$ are continuous; since $U$ is irreducible, it follows
that $F = U$ or $G=U$, which means exactly that $f$ or $g$ is zero on $U$.
We can therefore form the field of fractions of $\Aa_U$, which we denote by
$\Kk_U$; if $U\subset V$, the homomorphism $\rho_U^V: \Aa_V\to\Aa_U$ is injective,
because $U$ is dense in $V$, and we have a well defined isomorphism
$\phi_U^V$ of $\Kk_V$ to $\Kk_U$; the system of $\{\Kk_U, \phi_U^V\}$ defines
a \emph{sheaf of fields} $\Kk$; then $\Kk_x$ is canonically isomorphic with the
field of fractions of $\Oo_{x, X}$.

{\bf Proposition 9.} \emph{For any irreducible algebraic variety $X$, the sheaf
$\Kk$ defined above is a constant sheaf.}

By Lemma 2, it suffices to show the Proposition when $X$ is a locally closed
subvariety of the affine space $K^r$; let $F$ be the closure of $X$ in $K^r$
and let $I(F)$ be the ideal in $K[X_1, \ldots, X_r]$ of polynomials vanishing
on $F$ (or equivalently on $X$). If we set $A = K[X_1, \ldots, X_r]/I(F)$,
the ring $A$ is an integral domain because $X$ is irreducible; let $K(A)$
be the ring of fractions of $A$. By corollary of Proposition 4, we can identify
$\Oo_{x, X}$
%
% page 230
%
with the localization of $A$ in the maximal ideal defined by $x$;
we thus obtain an isomorphism of the field $K(A)$ with the field of fractions
of $\Oo_{x, X}$ and it is easy to check that it defines an isomorphism
of the constant sheaf equal to $K(A)$ with the sheaf $\Kk$, which shows
the Proposition.

By Lemma 1, the sections of the sheaf $\Kk$ form a field, isomorphic with
$\Kk_x$ for all $x\in X$, which we denote by $K(X)$. We call it the
\emph{field of rational functions} on $X$; it is an extension of finite type\footnote{
i.e. finitely generated} of the field $K$, whose transcendence degree over $K$
is the \emph{dimension} of $X$ (we extend this definition to reducible varieties
by imposing $\dim X = \mathrm{Sup} \dim Y_i$ if $X$ is a union of closed
irreducible varieties $Y_i$). In general, we identify the field $K(X)$ with
the field $\Kk_x$; since we have $\Oo_{x, X}\subset \Kk_x$, we see that
we can view $\Oo_{x, X}$ as a \emph{subring} of $K(X)$ (it is the ring
of specialization of the point $x$ in $K(X)$ in the sense of Weil, \cite{16}, p. 77).
If $U$ is an open subset of $X$, $\Gamma(U, \Oo_X)$ is the intersection in
$K(X)$ of the rings $\Oo_{x, X}$ for $x$ running over $U$.

If $Y$ is a subvariety of $X$, we have $\dim Y\leq \dim X$; if furthermore $Y$
is closed and does not contain any irreducible component of $X$, we have
$\dim Y < \dim X$, as shown by reducing to the case of subvarieties
of $K^r$ (cf. for example \cite{8}, Chap. X, \S 5, th. II).

