% page 246

\ssection{The characteristic function and arithmetic genus}
\setcounter{subsection}{78}

\ssubsection{Euler-Poincare characteristic}
\label{section-\arabic{subsection}}

Let $V$ be a projective variety and $\Ff$ a coherent algebraic sheaf on $V$. Let 
$$h^q(V,\Ff)= dim_K H^q(V,\Ff).$$

We have seen (\no \ref{section-66}, Theorem 1) that $h^q(V,\Ff)$ are \emph{finite} for all integer $q$
and zero for $q > dim V$. 
So we can define an integer $\chi(V,\Ff)$ by:
$$\chi(V,\Ff) = \sum^{\infty}_{q=0} (-1)^q \  h^q(V,\Ff).$$

This is the Euler-Poincare characteristic of $V$ with coefficient in $\Ff$.

{\bf Lemma 1.} \emph{Let $0 \rightarrow L_1 \rightarrow ... \rightarrow L_p \rightarrow 0$
be an exact sequence, where $L_i$ is a finite dimensional vector spaces over $K$,
and homomorphisms $L_i \rightarrow L_{i+1}$ is $K$-linear. Then:}
$$ \sum_{q=1}^{p} \  (-1)^q \ dim_K L_q \  = \  0. $$

To prove the lemma 1, proceed by induction on $p$. The lemma is evident if $p \le 3$. Otherwise, if $L_{p-1}'$
is the kernel of $L_{p-1} \rightarrow L_p$, we have two exact sequences:
$$0 \rightarrow L_1 \rightarrow ... \rightarrow L_{p-1}' \rightarrow 0$$
$$0 \rightarrow L_{p-1}' \rightarrow L_{p-1} \rightarrow L_p \rightarrow 0.$$

Applying induction hypothesis to each sequence, we see that $\sum_{q=1}^{p-2}
\ (-1)^q dim L_q + (-1)^{p-1} dim L_{p-1}' = 0 $, and
$$ dim L_{p-1}' - dim L_{p-1} + dim L_p = 0, $$
which proves the lemma.

{\bf Proposition 1.} \emph{Let $0 \rightarrow \mathcal{A} \rightarrow \mathcal{B} \rightarrow
\mathcal{C} \rightarrow 0$ be an exact sequence of coherent algebraic sheaves on a
projective variety $V$, with homomorphisms $\mathcal{A} \rightarrow \mathcal{B}$ and
$\mathcal{B} \rightarrow \mathcal{C}$ being $K$-linear. Then we have:}
$$ \chi(V, \mathcal{B}) = \chi(V, \mathcal{A}) + \chi(V, \mathcal{C}) .$$

By Corollary 2 of Theorem 5 of \no \ref{section-47}, there is an exact sequence of cohomology:
$$ ... \rightarrow H^q(V, \mathcal{B}) \rightarrow H^q(V, \mathcal{C}) \rightarrow H^{q+1}(V, \mathcal{A})
\rightarrow H^{q+1}(V, \mathcal{B}) \rightarrow ...$$

Applying Lemma 1. to this exact sequence of vector spaces we obtain the Proposition.

%page 247
{\bf Proposition 2.} \emph{Let $0 \rightarrow \Ff_1 \rightarrow ... \rightarrow \Ff_p \rightarrow 0$
be an exact sequence of coherent algebraic sheaves on a projective variety $V$, with
homomorphisms $\Ff_i \rightarrow \Ff_{i+1}$ being algebraic. Then we have:}
$$\sum_{q=1}^{p} (-1)^q \  \chi(V, \Ff_q) = 0.$$

We proceed by induction on $p$. The proposition is a particular case of Proposition 1
if $p \le 3$. If we define $\Ff_{p-1}'$ to be the kernel of $\Ff_{p-1} \rightarrow \Ff_p$,
the sheaf $\Ff_{p-1}'$ is coherent algebraic because $\Ff_{p-1} \rightarrow \Ff_p$ is
an algebraic homomorphism. So we can applicate the induction hypothesis to two exact sequences
$$ 0 \rightarrow \Ff_1 \rightarrow ... \rightarrow \Ff_{p-1}' \rightarrow 0 $$
$$ 0 \rightarrow \Ff_{p-1}' \rightarrow \Ff_{p-1} \rightarrow \Ff_p ,$$
and the Proposition follows.

\ssubsection{Relation with characteristic function of a graded $S$-module}
\label{section-\arabic{subsection}}

Let $\Ff$ be a coherent algebraic sheaf on the space $\mathbb{P}_r(K)$. We write
$\chi(\Ff)$ instead of $\chi(\mathbb{P}_r(K), \Ff)$. We have:

{\bf Proposition 3.} \emph{$\chi(\Ff(n))$ is a polynomial of $n$ of degree $ \le r$.}

By Theorem 2 of \no \ref{section-60}, there exists a graded $S$-module $M$ of finite type, such
that $\mathcal{A}(M)$ is isomorphic to $\Ff$. Applying the Hilbert's syzygy theorem
to $M$ we obtain an exact sequence of graded $S$-modules:
$$ 0 \rightarrow L^{r+1} \rightarrow ... \rightarrow L^0 \rightarrow M \rightarrow 0,$$
where $L^q$ are free of finite type. Applying the functor $\mathcal{A}$ to this sequence,
we obtain an exact sequence of sheaves:
$$ 0 \rightarrow \mathcal{L}^{r+1} \rightarrow ... \rightarrow \mathcal{L}^0 \rightarrow
\Ff \rightarrow 0 ,$$ where each $\mathcal{L}^q$ is isomorphic to a finite direct sum
of shaves $\Oo(n_i)$. The proposition 2 implies that $\chi(\Ff(n))$ is equal to an
alternating sum of $\chi(\mathcal{L}^0(n))$, which brings us to case of the sheaf $\Oo(n_i)$.
Now it follows from \no \ref{section-62} that we have $\chi(\Oo(n))= \binom{n+r}{r} $, which
is a polynomial on $n$ of the degree $\le r$. This implies the Proposition.

{\bf Proposition 4.} \emph{Let $M$ be a graded $S$-module satisfying condition (TF), and
let $\Ff = \mathcal{A}(M)$. For all $n$ large enough, we have $\chi(\Ff(n)) = dim_K M_n.$}

We know (by \no \ref{section-65}) that for $n$ large enough, the homomorphism $\alpha: M_n
\rightarrow H^0(X, \Ff(n))$ is bijective, and $H^q(X, \Ff(n)) = 0$ for $q > 0$. So we have: 
$$\chi(\Ff(n)) = h^0(X, \Ff(n)) = dim_K M_n.$$

We use a well known fact, that $dim_K M_n$ is a polynomial of $n$ for $n$ large enough.
This polynomial, which we denote by $P_M$ is called the \emph{characteristic function} of $M$.
For all $n \in \mathbb{Z}$ we have $P_M(n)= \chi(\Ff(n))$, and in particular for $n=0$,
we see that the \emph{constant term of $P_M$ is equal to $\chi(\Ff)$}.

Apply this to $M= S / I(V)$, $I(V)$ being a homogeneous ideal of $S$ 
%page 276
of polynomials which are zero on a closed subvariety $V$ of $\mathbb{P}_r(K)$.
The constant term of $P_M$ is called in this case the \emph{arithmetic genus} of $V$ (cf. \cite{19}).
Since on the other hand we have $\mathcal{A}(M) = \Oo_V$, we obtain:

{\bf Proposition 5.} \emph{The arithmetic genus of a projective variety $V$ is equal to}
$$ \chi(V, \Oo_V) = \sum_{q=0}^{\infty} (-1)^q dim_K H^q(V, \Oo_V).$$

{\bf Remarks.}\emph{
\begin{enumerate}[(1)]
\item The preceding Proposition makes evident the fact, that the arithmetic genus is
independent of an embedding of $V$ into a projective space, since it's true for $H^q(V, \Oo)$.
\item The virtual arithmetic genus (defined by Zariski in \cite{19}) can also be reduced to
Euler-Poincare characteristic. We return to this question later, by Riemann-Roch theorem.
\item For the reason of convenience, we have adopted the definition of arithmetic genus
different from the classical one (cf. \cite{19}). If all irreducible components of $V$ have
the same dimension $p$, two definitions are related by the following formula:
$\chi(V, \Oo_V)= 1 + (-1)^p p_a(V)$.
\end{enumerate}}

\ssubsection{The degree of the characteristic function}
\label{section-\arabic{subsection}}

If $\Ff$ is a coherent algebraic sheaf on an algebraic variety $V$, we call the
support of $\Ff$, and denote by $Supp(\Ff)$, the set of points $x \in V$ such that
$\Ff_x \neq 0$. By the fact that $\Ff$ is a sheaf of finite type, this set is closed.
If we have $\Ff_x = 0$, the zero section generates $\Ff_x$, then also $\Ff_y$ for $y$
in neighborhood of $x$ (\no \ref{section-12}, Proposition 1), which means that the complement of
$Supp(\Ff)$ is open.

Let $M$ be a graded $S$-module of finite type, and let $\Ff = \mathcal{A}(M)$ be a
sheaf defined by $M$ on $\mathbb{P}_r(K)=X$. We can determine $Supp(\Ff)$ from $M$
in the following manner:

Let $0 = \bigcap_\alpha M^\alpha$ be a decomposition of $0$ as an intersection of
homogeneous primary submodules $M^\alpha$ of $M$. $M^\alpha$ correspond to homogeneous
primary ideals $\mathfrak{p}^\alpha$ (cf. \cite{12}, Chap. IV). We suppose that this
decomposition is 'the shortest possible', i.e. that non of $M^\alpha$ is contained
in an intersection of others.  For all $x \in X$, each $\mathfrak{p}$ defines a primary
ideal $\mathfrak{p}_x^\alpha$ of a local ring $\Oo_x$, and we have $\mathfrak{p}_x^\alpha = \Oo$
if and only if $x$ is not an element of a variety $V^\alpha$ defined by an ideal
$\mathfrak{p}^\alpha$. We have also $0 = \bigcap_\alpha M^\alpha_x$ in $M_x$, and we
verify easily that we thereby obtain a primary decomposition of $0$ in $M_x$. The
$M^\alpha_x$ correspond to primary ideals $\mathfrak{p}^\alpha_x$; if $x \notin V^\alpha$,
we have $M_x^\alpha = M_x$, and if we restrict ourself to consider $M_x^\alpha$ such that
$x \in V^\alpha$, we obtain 'the shortest possible decomposition' (cf. \cite{12}, Chap IV, th 4.).
We conclude that $M_x \neq 0$ if and only if $x$ is an element of $V^\alpha$, thus
$Supp(\Ff) = \bigcup_\alpha V^\alpha$.

{\bf Proposition 6.} \emph{If $\Ff$ is a coherent algebraic sheaf on $\mathbb{P}_r(K)$,
the degree of $\chi(\Ff(n))$ is equal to the dimension of $Supp(\Ff)$.}

We proceed by induction on $r$. The case $r = 0$ is trivial. We suppose that
$\Ff = \mathcal{A}(M)$, where $M$ is a graded $S$-module of finite type. Using notation
introduced below, we have to show that $\chi(\Ff(n))$ is an polynomial of degree
$q = Sup \ dim \ V^\alpha$.

%page 277
Let $t$ be a linear homogeneous form, which do not appear in any of proper prime ideals
$\mathfrak{p}^\alpha$. Such a form exists because the field $K$ is infinite. Let $E$ be a
hyperplane of $X$ with equation $t=0$. Consider the exact sequence:
$$0 \rightarrow \Oo(-1) \rightarrow \Oo \rightarrow \Oo_K \rightarrow 0,$$
where $\Oo \rightarrow \Oo_E$ is a restriction homomorphism, while
$\Oo(-1) \rightarrow \Oo$ is a homomorphism $f \mapsto tf$. Applying tensor product
with $\Ff$, we obtain an exact sequence:
$$\Ff(-1) \rightarrow \Ff \rightarrow \Ff_E \rightarrow 0, \quad\text{with}\quad 
\Ff_E = \Ff \otimes_{\Oo} \Oo_E.$$

On $U_i$, we identify $\Ff(-1)$ with $\Ff$, and this identification transforms
the homomorphism $\Ff(-1) \rightarrow \Ff$ defined above to the multiplication by
$t/t_i$. Because $t$ was chosen outside $\mathfrak{p}^\alpha$, $t/t_i$ doesn't belong to
any prime ideal of $M_x = \Ff_x$ if $x \in U_i$, and then the preceding homomorphism is
injective (cf. \cite{12}, p. 122, th. 7, b")). So we have an exact sequence:
$$ 0 \rightarrow \Ff(-1) \rightarrow F \rightarrow \Ff_E \rightarrow 0,$$
from which, for all $n \in \mathbb{Z}$ the exact sequence:
$$ 0 \rightarrow \Ff(n-1) \rightarrow \Ff(n) \rightarrow \Ff_K(n) \rightarrow 0.$$

Applying Proposition 1, we see that:
$$ \chi(\Ff(n)) - \chi(\Ff(n-1)) = \chi(\Ff_E(n)).$$

But the sheaf $\Ff_E$ is a coherent sheaf of $\Oo_E$-modules, which means that
it is a coherent algebraic sheaf on $E$, which is a projective space of dimension $r-1$.
Moreover $\Ff_{x,E}=0$ means that the endomorphism of $\Ff_x$ defined by multiplication by
$t/t_i$ is surjective, which leads to $F_x =0$ (cf. \cite{6}, Chap VIII, prop 5.1'). It follows
that $Supp(\Ff_K) = E \cap Supp (\Ff)$, and because $E$ does not contain any of varieties
$V^\alpha$, if follows by a known fact, that the dimension of $Supp(\Ff_E)$ is equal to
$q-1$. By the induction hypothesis $\chi(\Ff_E(n))$ is a polynomial of degree $q-1$. As
this difference is prime to the function $\chi(\Ff(n))$, the latter is a polynomial of degree $q$.

{\bf Remarks.}
\begin{enumerate}[(1)]
\item Proposition 6 was well known for $\Ff = \Oo / \mathcal{I}$, $\mathcal{I}$
being a coherent sheaf of ideals. Cf. \cite{9} \no \ref{section-24}.
\item The above proof does not use Proposition 3 and shows it once again.
\end{enumerate}
