\ssection{Applications to coherent algebraic sheaves}
\setcounter{subsection}{72}

\ssubsection{Relations between functors $\Ext_S^q$ and $\Ext_{\Oo_x}^q$}
\label{section-\arabic{subsection}}

Let $M$ and $N$ be two graded $S$-modules. If $x$ is a point of $X=\mathbb{P}_r(K)$,
we have defined in \no \ref{section-57} the $\Oo_x$-modules $M_x$ and $N_x$; we will find relation
between $\Ext_{\Oo_x}^q(M_x,N_x)$ and graded $S$-module $\Ext_S^q(M,N)$.

{\bf Proposition 1.} \emph{Suppose that $M$ is of finite type. Then:}
\begin{enumerate}[(a)]
\item \emph{The sheaf $\mathcal{A}(Hom_S(M,N))$ is isomorphic to the sheaf
$Hom_{\Oo}(\mathcal{A}(M), \mathcal{A}(N))$.}
\item \emph{For all $x \in X$, the $\Oo_x$-module $\Ext_S^q(M,N)_x$ is isomorphic to the
$\Oo_x$-module $\Ext_{\Oo_x}^q(M_x,N_x)$.}
\end{enumerate}

First define a homomorphism $\iota_x:Hom_S(M,N)_x \rightarrow Hom_{\Oo_x}(M_x,N_x)$.
An element of first module is a fraction $\varphi/P$, with $\varphi \in Hom_S(M,N)_n$,
$P \in S(x)$, $P$ is homogeneous of degree $n$; if $m/P'$ is an element of $M_x$,
$\varphi(m)/PP'$ is an element of $N_x$ which does not depend on $\varphi / P$ and
$m / P'$, and the function $m/P' \rightarrow \varphi(m)/PP'$ is a homomorphism
$\iota_x(\varphi/P): M_x \rightarrow N_x$; this defines $\iota_x$. After Proposition 5
of \no \ref{section-14}, $Hom_{\Oo_x}(M_x,N_x)$ can be identified with:
$$ Hom_{\Oo}(\mathcal{A}(M), \mathcal{A}(N))_x;$$
this identification transforms $\iota_x$ into:
$$\iota_x : \mathcal{A}(Hom_S(M,N))_x \rightarrow Hom_{\Oo}(\mathcal{A}(M), \mathcal{A}(N))_x,$$
and we easily verify that the family of $\iota_x$ is a homomorphism
$$\iota: \mathcal{A}(Hom_S(M,N)) \rightarrow Hom_{\Oo}(\mathcal{A}(M), \mathcal{A}(N)).$$

When $M$ is a free module of finite type, $\iota_x$ is a bijection. Indeed,
it suffices to regard $M=S(n)$, for which it is obvious.

If now $M$ is any graded $S$-module of finite type, choose a resolution of $M$:
$$... \rightarrow L^{q+1} \rightarrow L^q \rightarrow ... \rightarrow L^0
\rightarrow M \rightarrow 0 $$ where $L^q$ are free of finite type, and consider
a complex $C$ formed by $Hom_S(L^q,N)$. The cohomology groups of $C$ are $\Ext_S^q(M,N)$; or else
%page 268
if we denote by $B^q$ and $Z^q$ the submodules of $C^q$ formed respectively
by the coboundaries and cocycles, we have the exact sequences:
$$ 0 \rightarrow Z^q \rightarrow C^q \rightarrow B^{q+1} \rightarrow 0$$
and 
$$ 0 \rightarrow B^q \rightarrow Z^q \rightarrow \Ext^q_S(M,N) \rightarrow 0.$$
As the functor $\mathcal{A}(M)$ is exact, the sequences
$$ 0 \rightarrow Z^q_x \rightarrow C^q_x \rightarrow B^{q+1}_x \rightarrow 0$$
and
$$ 0 \rightarrow B^q_x \rightarrow Z^q_x \rightarrow \Ext_S^q(M,N)_S \rightarrow 0$$
are also exact.

But after preceding consideration $C^q_x$ is isomorphic to $Hom_{\Oo_x}(L^q_x,N_x)$;
the $\Ext_S^q(M,N)_x$ are isomorphic to cohomology groups of a complex formed by
the $Hom_{\Oo_x}(L_x^q,N_x)$ and, because the $L_x^q$ are clearly $\Oo_x$-free,
we get back the definition of $\Ext_{\Oo_x}^q(M_x,N_x)$, which shows (b).
For $q=0$ preceding considerations show that $\iota_x$ is bijection, so $\iota$
is an isomorphism, so (a) holds.

\ssubsection{Vanishing of cohomology groups $H^q(X,\Ff(-n))$ for $n \rightarrow +\infty$}
\label{section-\arabic{subsection}}

{\bf Theorem 1.} \emph{Let $\Ff$ be a coherent algebraic sheaf on $X$ and let $q$
be an integer $\ge 0$. The following conditions are equivalent:}
\begin{enumerate}[(a)]
\item \emph{$H^q(X, \Ff(-n)) = 0$ for $n$ large enough.}
\item \emph{$\Ext^{r-q}_{\Oo_x}(\Ff_x, \Oo_x) = 0$ for all $x \in X$.}
\end{enumerate}

After Theorem 2 of \no \ref{section-60}, we can suppose that $\Ff = \mathcal{A}(M)$, where $M$
is a graded $S$-module of finite type, and by the \no \ref{section-64} $H^q(X,\Ff(-n))$ is
isomorphic to $H^q(M(-n)) = B^q(m)_{-n}$, so condition (a) is equivalent to
$$ T^q(M)_n = 0$$ for $n$ large enough, that is to say $T^q(M) \in \mathcal{C} $.
After Theorem 1 of \no \ref{section-72} and the fact that $M^\star \in \mathcal{C}$ as $M$ is of
finite type, this last condition is equivalent to $\Ext^{r-q}_S(M,\Omega) \in \mathcal{C}$;
as $\Ext^{r-q}_S(M,\Omega)$ is a $S$-module of finite type,
$$ \Ext^{r-q}_S(M, \Omega) \in \mathcal{C}$$
is equivalent to $\Ext^{r-q}_S(M,\Omega)_x = 0$ for all $x \in X$, by Proposition 5
of \no \ref{section-58}. Finally the Proposition 1 shows that
$\Ext^{r-q}_S(M, \Omega)_x = \Ext^{r-q}_{\Oo_x}(M_x, \Omega_x)$
and as $M_x$ is isomorphism to $\Ff_x$ and $\Omega_x$ is isomorphic to $\Oo(-r-1)_x$,
so to $\Oo_x$, this completes the proof.

For announcing Theorem 2, we will need the notion of \emph{dimension} of an $\Oo_x$-module.
Recall (\cite{6}, Chap VI) that $\Oo_x$-module of finite type $P$ is of dimension
$\le p$ if there is an exact sequence of $\Oo$-modules:
$$ 0 \rightarrow L_p \rightarrow L_{p-1} \rightarrow ... \rightarrow L_0
\rightarrow P \rightarrow 0,$$ where each $L_p$ is free (this definition is
equivalent to \cite{6}, because all projective $\Oo_x$-modules of finite type are
free (cf \cite{6}, Chap VIII, Th. 6.1.').

%269
All $\Oo_x$-modules of finite type are of dimension $\le r$, by Hilbert's
syzygy theorem. (cf. \cite{6}, Chap VIII, Th. 6.2').

{\bf Lemma 1.} \emph{Let $P$ be an $\Oo_x$-module of finite type and let $p$ be an
integer $\ge 0$. The following two conditions are equivalent:}
\begin{enumerate}[(i)]
 \item \emph{$P$ is of dimension $\le p$.}
 \item \emph{$\Ext^m_{\Oo_x}(P,\Oo_x) = 0$ for all $m > p$.}
\end{enumerate}

It is clear that (i) implies (ii). We will show that (ii) implies (i) by induction
decreasing on $p$. For $p \ge r$ the lemma is trivial, because (i) is always true.
Now pass from $p+1$ to $p$; let $N$ be any $\Oo_x$-module of finite type. We can
find an exact sequence $0 \rightarrow R \rightarrow L \rightarrow N \rightarrow 0$,
where $L$ is free of finite type (because $\Oo_x$ is Noetherian). The exact sequence:
$$\Ext_{\Oo_x}^{p+1}(P,L) \rightarrow \Ext_{\Oo_x}^{p+1}(P,N) \rightarrow \Ext_{\Oo_x}^{p+2}(P,R)$$
shows that $\Ext_{\Oo_x}^{p+1}(P,N) = 0 $, so we have $\Ext_{\Oo_x}^{p+2}(P,L) = 0$
by condition (ii), and $\Ext_{\Oo_x}^{p+2}(P,R) =0 $ as $\dim P \le p + 1$ by
the induction hypothesis. As this property characterizes the modules of finite
dimension $\le p$, the lemma is proved.

By combining Lemma with Theorem 1 we obtain: 

{\bf Theorem 2.} \emph{Let $\Ff$ be a coherent algebraic sheaf on $X$, and let $p$
be an integer $\ge 0$. The following two conditions are equivalent:}
\begin{enumerate}[(i)]
 \item \emph{$H^q(X,\Ff(-n)) = 0$ for all $n$ large enough and $0 \le q < p$.}
 \item \emph{For all $x \in X$ the $\Oo_x$-module $\Ff_x$ is of dimension $\le r -p $.}
\end{enumerate}

\ssubsection{Nonsingular varieties}
\label{section-\arabic{subsection}}

The following results play essential role in extension of the 'duality theorem'
\cite{15} to an arbitrary case.

{\bf Theorem 3.} \emph{Let $V$ be a nonsingular subvariety of projective space
$\mathbb{P}_r(K)$. Suppose that all irreducible components of $V$ have the same
dimension $p$. Let $\Ff$ be a coherent algebraic sheaf on $V$, such that for all
$x \in V$, $\Ff_x$ is a free module over $\Oo_{x,V}$. Then we have $H^q(V,\Ff(-n)) = 0$
for all $n$ large enough and $0 \le q < p$.}

After Theorem 2, it remains to show that $\Oo_{x,V}$ considered as $\Oo_x$-module
is of dimension $\le r -p$. Denote by $g_x(V)$ the kernel of the canonical
homomorphism $\epsilon: \Oo_x \rightarrow \Oo_{x,V}$; since the point $x$ is simple
over $V$, we know (cf. \cite{18}, th 1) that this ideal is generated by $r-p$ elements
$f_1,...,f_{r-p}$, and the theorem of Cohen-Macaulay (cf. \cite{13}, p. 53, prop 2)
shows that we have
$$(f_1,...,f_{i-1}):f_i = (f_1,...,f_{i-1}) \quad \text{for} \quad 1 \le i \le r-p.$$
Denote by $L_q$ a free $\Oo_x$-module which admits a base of elements
$e<i_1...i_q>$ corresponding to sequence $(i_1,...,i_q)$ such that
$$1 \le i_1 < l_2 < ... < i_q \le r-p;$$
for $q=0$, take $L_0 = \Oo_x$ and define:
\begin{align*}
d(e\langle i_1...i_q\rangle) &= \sum_{j=1}^{q} (-1)^j f_{i,j} e\langle i_1,...\widehat{i}_j... i_q\rangle \\
d(e\langle i\rangle )&=f_i
\end{align*}
%270
After \cite{6}, Chap. VIII, prop 4.3, the sequence
$$ 0 \rightarrow L_{r-p} \xrightarrow{d} L_{r-p-1} \xrightarrow{d} ...
\xrightarrow{d} L_0 \xrightarrow{\epsilon_x} \Oo_{x,V} \rightarrow 0$$
is exact, which shows that $\dim_{\Oo_x}(\Oo_{x,V}) \le r-p$, QED.

{\bf Corollary.} \emph{We have $H^q(V,\Oo_V(-n)) = 0$ for $n$ large enough and $0 \le q < p$.}

{\bf Remark.} The above proof applies more generally whenever
the ideal $g_x(V)$ admits a system of $r-p$ generators, that is, if the variety $V$
is a \emph{local complete intersection} at all points.

\ssubsection{Normal Varieties}
\label{section-\arabic{subsection}}

{\bf Lemma 2.} \emph{Let $M$ be a $\Oo_x$ module of finite type and let $f$ be a
noninvertible element of $\Oo_x$, such that the relation $fm = 0$ implies
$m=0$ if $m \in M$. Then the dimension of the $\Oo_x$-module $M/fM$ is equal to
the dimension of $M$ increased by one.}

By assumption, we have an exact sequence $0 \rightarrow M \xrightarrow{\alpha} M
\rightarrow M/fM \rightarrow 0$, where $\alpha$ is multiplication by $f$. If $N $ is
a $\Oo_x$-module of finite type, we have an exact sequence:
$$... \rightarrow \Ext^q_{\Oo_x}(M,N) \xrightarrow{\alpha} \Ext^q_{\Oo_x}(M,N)
\rightarrow \Ext^{q+1}_{\Oo_x}(M/fM,N) \rightarrow \Ext^{q+1}_{\Oo_x}(M,N) \rightarrow ...$$

Denote by $p$ the dimension of $M$. By taking $q=p+1$ in the preceding exact
sequence, we see that $\Ext^{p+2}_{\Oo_x}(M/fM,N) = 0$, which (by \cite{6}, Chap. VI, 2)
implies that $\dim(M/fM) \le p +1$. On the other hand, since $\dim M = p$ we can
choose $N$ such that $\Ext^p_{\Oo_x}(M,N) \neq 0$; by taking $q=p$ in the above
exact sequence, we see that $\\Ext_{\Oo_x}^{p+1}(M/fM, N)$ can be identified with cokernel of
$$ \Ext^p_{\Oo_x}(M,N) \xrightarrow{\alpha} \Ext^p_{\Oo_x}(M,N)'$$
as the last homomorphism is nothing else that multiplication by $f$ and
that $f$ isn't invertible in the local ring $\Oo_x$. If follows from \cite{6}, Chap.
VIII, prop. 5.1' that this cokernel is $\neq 0$, which shows that $\dim M/fM \ge p+1$
and finishes the proof.

We will now show a result, that is related with 'the Enriques-Severi lemma'
of Zariski \cite{19}:

{\bf Theorem 4.} \emph{Let $V$ be an irreducible, normal subvariety of dimension
$\ge 2$, of projective space $\mathbb{P}_r(K)$. Let $\Ff$ be a coherent algebraic
sheaf on $V$, such that for all $x \in V$, $\Ff_x$ is a free module over $\Oo_{x,V}$.
Then we have $H^1(V,\Ff(-n)) = 0$ for $n$ large enough.}

After Theorem 2, it remains to show that $\Oo_{x,V}$, considered as $\Oo_x$-module
is of dimension $\le r-2$. First choose an element $f\in \Oo_x$ such that $f(x)=0$
and that the image of $f$ in $\Oo_{x,V}$ is not zero; this is possible because
$\dim V > 0$. As $V$ is irreducible, $\Oo_{x,V}$ is an integral ring (domain),
and we can apply 
%271
Lemma 2 to the pair $(\Oo,f)$; we then have:
$$\dim \Oo_{x,V} = \dim \Oo_{x,V}/(f) -1, \quad \text{ with } \quad (f)=f\Oo_{x,V}.$$

As $\Oo_{x,V}$ is an integrally closed ring, all prime ideals $\mathfrak{p}^\alpha$
of the principal ideal $(f)$ are minimal (cf. \cite{12} p.136, or \cite{9}, \no \ref{section-37}), and none
of them is equal to the maximal ideal $\mathfrak{m}$ of $\Oo_{x,V}$ (if not we would
have $\dim V \le 1$). So we can find an element $g \in \mathfrak{m}$, not belonging
to any of $\mathfrak{p}^\alpha$; this element $g$ is not divisible by $0$ in the
quotient ring $\Oo_{x,V}/(f)$; we denote by $\widetilde{g}$, a representation of
$g$ in $\Oo_x$. We see that we can apply Lemma to the pair $\Oo_{x,V}/(f),\widetilde{g})$;
we then have:
$$\dim \Oo_{x,V}/(f) = \dim \Oo_{x,V}/(f,g) - 1. $$

But by Hilbert's syzygy theorem, we have $\dim \Oo_{x,V}/(f,g) \le r$, so
$\dim \Oo_{x,V} \le r - 1$ and $\dim \Oo_{x,V} \le r - 2 $ QED.

{\bf Corollary.} \emph{We have $H^1(V,\Oo_V(-n)) = 0 $ for n large enough.}

{\bf Remarks.} \begin{enumerate}[(1)]
 \item The reasoning made before is classic in theory of syzygies. Cf. W. Gr\"obner,
\emph{Moderne Algebraische Geometrie}, 152.6 and 153.1.
 \item If the dimension of $V$ is $> 2 $, we can have $\dim \Oo_{x,V} = r -2 $.
This is in particular the case when $V$ is a cone which hyperplane section $W$
is a normal and irregular projective variety (i.e., $H^1(W,\Oo_W) \neq 0$).
\end{enumerate}

\ssubsection{Homological characterization of varieties $k$-times of first kind}
\label{section-\arabic{subsection}}

Let $M$ be a graded $S$-module of finite type. We show by a reasoning
identical to that of Lemma 1:

{\bf Lemma 3} \emph{$\dim \le k$ if and only if $\Ext^q_S(M,S) = 0$ for $q > k$.}

As $M$ is graded. we have $\Ext^q_S(M,\Omega)= \Ext^q_S(M,S)(-r-1)$, so the
previous condition is equivalent to $\Ext^q_S(M,\Omega) = 0 $ for $q>k$. Given
Theorem 1 of \no \ref{section-72}, we conclude:\\

{\bf Proposition 2.} 
\begin{enumerate}[(a)]
\item \emph{For $\dim M \le r$ it is necessary and sufficient that
$M_n \rightarrow H^0(M(n))$ is injective for all $n \in \mathbb{Z}$.}
 \item \emph{If $k$ is an integer $\ge 1$, for $\dim M \le r-k$ it is necessary
and sufficient that $\alpha : M_n \rightarrow H^0(M(n))$ is bijective
for all $n \in \mathbb{Z}$, and that $H^q(M(n))=0$ for $0 < q < k $ and all
$n \in \mathbb{Z}$.}
\end{enumerate}

Let $V$ be a closed subvariety of $\mathbb{P}_r(K)$, and let $I(V)$ be
an ideal if homogeneous polynomials, which are zero on $V$.

Denote $S(V) = S/I(V)$, this is a graded $S$-module whose associated
sheaf is $\Oo_V$. We say\footnote{Cf. P. Dubreil, \emph{Sur la dimension
des id\'eaux de polynômes}, J . Math. Pures App., 15,
1936, p. 271-283. See also W . Gr\"obner, hloderne Algebraische Geometrie, \S 5.}
 that $V$ is a variety ``$k$-times of first kind''
of $\mathbb{P}_r(K)$ if the dimension of $S$-module $S(V)$ is $\le r -k $.
It is obvious that $\alpha: S(V)_n \rightarrow H^0(V, \Oo_V(n))$ is injective
for all $n \in \mathbb{Z}$, so all varieties are $0$-times of first kind. Using
preceding proposition to $M=S(V)$, we obtain:\\

{\bf Proposition 3.} \emph{Let $k$ be an integer $\ge 1$. For a subvariety $V$ to
be a $k$-times of first kind, it is necessary and sufficient that the following
conditions are satisfied for all $n \in \mathbb{Z}$:
\begin{enumerate}[(i)]
 \item $\alpha : S(V)_n \rightarrow H^0(V, \Oo_V(n))$ is bijective.
 \item $H^q(V, \Oo_V(n)) = 0$ for $0 < q < k$. 
\end{enumerate}}

(The condition (i) can also be expressed by saying that linear series cut on
$V$ by forms of degree $n$ is complete, which is well known.)

By comparing with Theorem 2 (or by direct reasoning), we obtain:

{\bf Corollary.} \emph{If $V$ is $k$-times of first kind, we have $H^q(V,\Oo_V)=0$
for $0 < q <k$ and, for all $x \in V$, the dimension of $\Oo_x$-module $\Oo_{x,V}$ is $\le r - k$.}

If $m$ is an integer $\ge 1$, denote by $\varphi_m$ the embedding of $\mathbb{P}_r(K)$
into a projective space of convenient dimension, given by the monomials of degree
$m$ (cf. \cite{8}, Chap. XVI, 6, or \no \ref{section-52}, proof of Lemma 2). So the preceding corollary
admits following converse:

{\bf Proposition 4.} \emph{Let $k$ be an integer $\ge 1$, and let $V$ be a connected and
closed subvariety of $\mathbb{P}_r(K)$. Suppose that $H^q(V,\Oo_V) = 0$ for
$0 < q < k$, and that for all $x \in V$ the dimension of $\Oo_x$-module $\Oo_{x,V}$ is $\le r-k$.\\
Then for all $m$ large enough, $\varphi_m(V)$ is a subvariety $k$-times of first kind.}

Because $V$ is connected, we have $H^0(V,\Oo_V)=K$. So, if $V$ is irreducible, it's
evident (if not, $H^0(V,\Oo_V)$ contains a polynomial algebra and is not of finite
dimension over $K$); if $V$ is reducible, all elements $f \in H^0(V,\Oo_V)$ induce a
constant on each of irreducible components of $V$, and this constants are the same,
because of connectivity of $V$.

By the fact that $\dim \Oo_{x,V} \le r-1$, the algebraic dimension of each of
irreducible components of $V$ is at least equal to $1$. So it follows that
$$H^0(V,\Oo_V(-n)) = 0$$
for all $n > 0$ (because if $f \in H^0(V, \Oo_V(-n))$ and $f \neq 0$, the $f^kg$
with $g \in S(V)_{nk}$ form a vector subspace of $H^0(V,\Oo_V)$  of dimension $> 1$).\\

That being said, denote by $V_m$ the subvariety $\varphi_m(V)$; we obviously have:
$$\Oo_{V_m}(n) = \Oo_V(nm).$$
For $m$ large enough the following conditions are satisfied:

 {\bf (a)} \emph{$\alpha:S(V)_{nm} \rightarrow H^0(V,\Oo_V(nm))$ is bijective for all $n \ge 1$.}

This follows from Proposition 5 of \no \ref{section-65}.

 {\bf (b)} \emph{$H^q(V,\Oo_V(mn)) = 0$ for $0 < q < k$ and for all $n \ge 1$.}

This follows from Proposition of \no \ref{section-65}.

 {\bf (c)} \emph{$H^q(V,\Oo_V(nm)) = 0$ for $0 < q < k$ and for all $n \le -1$.}

This follows from Theorem 2 of \no \ref{section-74}, and hypothesis made on $\Oo_{x,V}$.

On the other hand, we have $H^0(V,\Oo_V)=K$, $H^0(V, \Oo_V(nm)) = 0$ for all $n \le -1$,
and $H^q(V, \Oo_V) = 0$ for $0 < q < k$, by the hypothesis. It follows that $V_m$ satisfies
all the hypothesis of Proposition 3, QED.

{\bf Corollary.} \emph{Let $k$ be an integer $\ge 1$, and let $V$ be a projective variety
without singularities, of dimension $\ge k$. For $V$ being birationally isomorphic
to a subvariety $k$-times of first kind of a convenient projective space, it is
necessary and sufficient that $V$ is connected and that $H^q(V,\Oo_V)=0$ for $0 < q < k$.}

The necessity is evident, by Proposition 3. To show sufficiency, it suffices to
remark that $\Oo_{x,V}$ is of dimension $\le r-k$ (cf. \no \ref{section-75}) and to apply
the previous proposition.

\ssubsection{Complete intersections}
\label{section-\arabic{subsection}}

A subvariety $V$ of dimension $p$ of projective space $\mathbb{P}_r(K)$ is a \emph{complete
intersection} if the ideal $I(V)$ of polynomials zero at $V$ admits a system of $r-p$
generators $P_1,...,P_{r-p}$; in this case, all irreducible components of $V$ have the
dimension $p$, by the theorem of Macaulay (cf. \cite{9}, \no 17). It is known, that this
variety is $p$-times of first kind, which implies that $H^q(V,\Oo_V(n)) = 0$ for
$0 < q < p$, as we have just seen. We will determine $H^p(V,\Oo_V(n))$ as a function
of degree $m_1,...,m_{r-p}$ of homogeneous polynomials $P_1,...,P_{r-p}$.\\

Let $S(V)=S/I(V)$ be a ring of projective coordinates of $V$. By theorem 1 of \no \ref{section-72}
all it is left, is to determine the $S$-module $\Ext^{r-p}_S(S(V),\Omega)$. We have a
resolution, analogous to that of \no \ref{section-75}: we take $L^q$ the graded free $S$-module,
admitting for a base the elements $e\langle i_1,...,i_q\rangle$, corresponding to sequences
$(i_1,...,i_q)$ such that $1 \le i_1 < i_2 < ... < i_q \le r- p$ and of degree
$\sum^q_{j=1} m_j$; for $L^0$ we take $S$. We set:
\begin{align*}
 d(e\langle i_1,...,i_q\rangle)&=\sum_{j=1}^q(-1)^i P_{i_j} e\langle i_1... \widehat{i_j}... i_q\rangle \\
 d(e\langle i\rangle) &= P_i.
\end{align*}

The sequence $0 \rightarrow L^{r-p} \xrightarrow{d} ... \xrightarrow{d} L^0
\rightarrow S(V) \rightarrow 0$ is exact (\cite{6}, Chap. VIII, Prop. 4.3).
It follows that the $\Ext^q_S(S(V), \Omega)$ are the cohomology groups of the
complex formed by the $Hom_S(L^q,\Omega)$; but we can identify an element of 
$Hom_S(L^q, \Omega)_n$ with a system $f\langle i_1,...i_q\rangle$, where the $f\langle i_1,...,i_q\rangle$
are homogeneous polynomials of degree $m_{i_1} + ... + m_{i_q} + n - r -1$;
after this identification is made, the operator of coboundary is given by usual formula:
$$ (df)\langle i_1...i_{q+1}\rangle = \sum_{j=1}^q (-1)^j P_{i_j} f\langle i_1...\widehat{i_j}... i_{q+1}\rangle.$$

The theorem of Macaulay implies that we are in conditions of \cite{11}, and we
obtain that $\Ext^q_S(S(V),\Omega) =0 $ for $q \neq r - p$. On the other hand,
$\Ext_S^{r-p}(S(V), \Omega)_n$ is isomorphic to a vector subspace of $S(V)$
formed by homogeneous elements of degree $N + n$, where $N=\sum_{i=1}^{r-p} m_i -r -1 $.
Using Theorem 1 of \no \ref{section-72} we obtain:

{\bf Proposition 5.} \emph{Let $V$ be a complete intersection, defined by the
homogeneous polynomials $P_1,...,P_{r-p}$ of degrees $m_1,...,m_{r-p}$.
\begin{enumerate}[(a)]
\item The function $\alpha:S(V)_n \rightarrow H^0(V,\Oo_{V}(n))$ is
bijective for all $n \in \mathbb{Z}$.
\item $H^q(V,\Oo_V(n)) = 0$ for $0 < q < p$ and all $n \in \mathbb{Z}$.
\item $H^q(V,\Oo_V(n))$ is isomorphic to a dual vector space to $H^0(V,\Oo_V(N-n))$,
with $N=\sum_{i=1}^{r-p} m_i -r -1$.
\end{enumerate}}

We see that in particular $H^p(V,\Oo_V)$ is zero if $N<0$.
