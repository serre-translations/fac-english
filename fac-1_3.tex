\ssection{Cohomology of a space with values in a sheaf}
\setcounter{subsection}{17}

In this paragraph, $X$ is a topological space, separated or not. By a \emph{covering}
of $X$ we will always mean an open covering. 

\ssubsection{Cochains of a covering}
\label{section-\arabic{subsection}}

Let $\Uu = \{ U_i \}_{i\in I}$ be a covering of $X$. If $s=(i_0, \ldots, i_p)$ is
a finite sequence of elements of $I$, we put
\[ U_s = U_{i_0\ldots i_p} = U_{i_0} \cap \ldots \cap U_{i_p}. \]

Let $\Ff$ be a sheaf of abelian groups on the space $X$. If $p$ is an integer $\geq 0$,
we call a \emph{$p$-cochain of $\Uu$ with values in $\Ff$} a function $f$ assigning
to every $s = (i_0, \ldots, i_p)$ of $p+1$ elements of $I$ a section 
$f_s = f_{i_0\ldots i_p}$ of $\Ff$ over $U_{i_0\ldots i_p}$. The $p$-cochains form
an abelian group, denoted by $C^p(\Uu, \Ff)$; it is the product group 
$\prod\Gamma(U_s, \Ff)$, the product being over all sequences $s$ of $p+1$ elements
of $I$. The family of $C^p(\Uu, \Ff)$, $p=0, 1, \ldots$ is denoted by
$C(\Uu, \Ff)$. A $p$-cochain is also called a cochain of degree $p$.

A $p$-cochain is said to be \emph{alternating} if:\\
\wciecie {\bf (a)} $f_{i_0\ldots i_p} = 0$ whenever any two of the indices $i_0, \ldots, i_p$ 
are equal, \\
\wciecie {\bf (b)} $f_{i_{\sigma 0}\ldots i_{\sigma p}} = \eps_\sigma f_{i_0\ldots i_p}$
if $\sigma$ is a permutation of the set $\{0, \ldots, p\}$ ($\eps_\sigma$ denotes
the sign of $\sigma$).

The alternating cochains form a subgroup $C'^p(\Uu, \Ff)$ of the group $C^p(\Uu, \Ff)$;
the family of the $C'^p(\Uu, \Ff)$ is denoted by $C'(\Uu, \Ff)$.

\ssubsection{Simplicial operations}
\label{section-\arabic{subsection}}

Let $S(I)$ be the simplex with the set $I$ as its set of vertices; an (ordered)
simplex of $S(I)$ is a sequence $s=(i_0, \ldots, i_p)$ of elements of $I$;
$p$ is called the dimension of $s$. Let $K(I) = \bigoplus_{p=0}^\infty K_p(I)$ be the complex
defined by $S(I)$; by definition, $K_p(I)$ is a free group with the set of simplexes
of dimension $p$ of $S(I)$ as its base.

If $s$ is a simplex of $S(I)$, we denote by $|s|$ the set of vertices of $s$.

A mapping $h: K_p(I)\to K_q(I)$ is called a \emph{simplicial endomorphism} if\\
{\bf (i)} $h$ is a homomorphism, \\
{\bf (ii)} For any simplex $s$ of dimension $p$ of $S(I)$ we have
\[ h(s) = \sum_{s'} c_{s}^{s'} \cdot s', \quad \mathrm{with }\, c_s^{s'}\in\mathbb{Z},\]
the sum being over all simplexes $s'$ of dimension $q$ such that $|s'|\subset|s|$.

Let $h$ be a simplicial endomorphism, and let $f\in C^q(\Uu, \Ff)$ be a cochain of
%
% page 213
%
degree $q$. For any simplex $s$ of dimension $p$ put:
\[ (\tu hf)_s = \sum_{s'} c_s^{s'} \cdot \rho_s^{s'}(f_{s'}), \]
$\rho^{s'}$ denoting the restriction homomorphism: $\Gamma(U_{s'}, \Ff)\to
\Gamma(U_s, \Ff)$, which makes sense because $|s'|\subset|s|$. The mapping $s\mapsto(\tu hf)_s$
is a $p$-cochain, denoted by $\tu hf$. The mapping $f\mapsto\tu hf$ is a homomorphism
\[ \tu h : C^q(\Uu, \Ff) \to C^p(\Uu, \Ff), \]
and one verifies immediately the formulas:
\[ \tu(h_1+h_2) = \tu h_1 + \tu h_2, \quad \tu(h_1\circ h_2)=\tu h_2 \circ \tu h_1, \quad
	\tu 1 = 1. \]

\emph{Note.} In practice, we often do not write the restriction homomorphism $\rho_s^{s'}$.

\ssubsection{Complexes of cochains}
\label{section-\arabic{subsection}}

We apply the above to the simplicial endomorphism
\[ \partial : K_{p+1}(I) \to K_p(I), \]
defined by the usual formula:
\[ \partial(i_0, \ldots, i_{p+1}) = \sum_{j=0}^{j=p+1} (-1)^j (i_0, \ldots, \hat{i}_j, \ldots,
  i_{p+1}), \]
the sign $\hat{}$ meaning, as always, that the symbol below it should be omitted.

We thus obtain a homomorphism $\tu\partial: C^p(\Uu, \Ff)\to C^{p+1}(\Uu, \Ff)$, which we
denote by $d$; from definition, we have that
\[ (df)_{i_0 \ldots i_{p+1}} = \sum_{j=0}^{j=p+1} (-1)^j \rho_j(f_{i_0\ldots\hat{i}_j
\ldots i_{p+1}}), \]
where $\rho_j$ denotes the restriction homomorphism
\[ \rho_j : \Gamma(U_{i_0\ldots \hat{i}_j\ldots i_{p+1}}, \Ff)\to 
\Gamma(U_{i_0\ldots i_{p+1}}, \Ff). \]
Since $\partial\circ\partial=0$, we have $d\circ d = 0$. Thus we find that $C(\Uu, \Ff)$ 
is equipped with a coboundary operator making it a complex. The $q$-th cohomology group
of the complex $C(\Uu, \Ff)$ will be denoted by $\HH^q(\Uu, \Ff)$. We have:

{\bf Proposition 1.} $\Hz(\Uu, \Ff) = \Gamma(X, \Ff)$. 

A $0$-cochain is a system $(f_i)_{i\in I}$ with every $f_i$ being a section of $\Ff$ over
$U_i$. It is a cocycle if and only if it satisfies $f_i - f_j = 0$ over $U_i \cap U_j$,
or in other words, if there is a section $f$ of $\Ff$ on $X$ coinciding with $f_i$ on $U_i$
for all $i\in I$. Hence the Proposition.

(Thus $\Hz(\Uu, \Ff)$ is independent of $\Uu$; of course this is not true for $\Hq(\Uu, \Ff)$
in general).

We see immediately that $df$ is alternating if $f$ is alternating; in other words, $d$
preserves $C'(\Uu, \Ff)$ which forms a subcomplex of $C(\Uu, \Ff)$. The cohomology groups
of $C'(\Uu, \Ff)$ are denoted by $\HH'^q(\Uu, \Ff)$.

% page 214

{\bf Proposition 2.} \emph{The inclusion of $C'(\Uu, \Ff)$ in $C(\Uu, \Ff)$ induces an isomorphism
of $\HH^q(\Uu, \Ff)$ and $\HH^q(\Uu, \Ff)$, for every $q\geq 0$.}

We equip the set $I$ with a structure of a total order, and let $h$ be a simplicial 
endomorphism of $K(I)$ defined in the following way:\\
\wciecie $h((i_0, \ldots, i_q)) = 0$ if any two indices $i_0, \ldots, i_q$ are equal,\\
\wciecie $h((i_0, \ldots, i_q)) = \eps_\sigma(i_{\sigma 0}\ldots i_{\sigma q})$ if all
indices $i_0, \ldots, i_q$ are distinct and $\sigma$ is a permutation of $\{0, \ldots, q\}$
for which $i_{\sigma 0} < i_{\sigma 1} < \ldots < i_{\sigma q}$ .

We verify right away that $h$ commutes with $\partial$ and that $h(s)=s$ if $\dim(s)=0$;
in result (cf. \cite{7}, Chapter VI, \S 5) there exists a simplicial endomorphism
$k$, raising the dimension by one, such that $1-h = \partial\circ k + k\circ\partial$.
Hence, by passing to $C(\Uu, \Ff)$,
\[ 1 - \tu h = \tu k \circ d + d \circ \tu k . \]

But we check immediately that $\tu h$ is a \emph{projection} from $C(\Uu, \Ff)$ onto
$C'(\Uu, \Ff)$; since the preceding formula shows that it is a homotopy operator, the
Proposition is proved. (Compare with \cite{7}, Chapter VI, theorem 6.10).

{\bf Corollary.} \emph{$\HH^q(\Uu, \Ff) = 0$ for $q>\dim(\Uu)$. }

By the definition of $\dim(\Uu)$, we have $U_{i_0\ldots i_q} = \emptyset$ for $q>\dim(\Uu)$,
if the indices $i_0, \ldots, i_q$ are distinct; hence $C'^q(\Uu, \Ff) = 0$, which shows that
\[ \HH^q(\Uu, \Ff) = \HH'^q(\Uu, \Ff) = 0. \]

\ssubsection{Passing to a finer covering}
\label{section-\arabic{subsection}}

A covering $\Uu=\{U_i\}_{i\in I}$ is said to be \emph{finer} than the covering
$\Bb = \{ V_j \}_{j\in J}$ if there exists a mapping $\tau: I\to J$ such that 
$U_i\subset V_{\tau i}$ for all $i\in I$. If $f \in C^q(\Bb, \Ff)$, put
\[ (\tau f)_{i_0, \ldots, i_q} = \rho_U^V(f_{\tau i_0 \ldots \tau i_q}), \]
$\rho_U^V$ denoting the restriction homomorphism defined by the inclusion of
$U_{i_0\ldots i_q}$ in $V_{\tau i_0 \ldots \tau i_q}$. The mapping $f\mapsto \tau f$
is a homomorphism from $C^q(\Bb, \Ff)$ to $C^q(\Uu, \Ff)$, defined for all $q\geq 0$
and commuting with $d$, thus it defines homomorphisms
\[ \tau^\ast: \HH^q(\Bb, \Ff) \to \HH^q(\Uu, \Ff). \]

{\bf Proposition 3.} \emph{The homomorphisms $\tau^\ast: \HH^q(\Bb, \Ff)\to \HH^q(\Uu, \Ff)$
depend only on $\Uu$ and $\Bb$ and not on the chosen mapping $\tau$.}

Let $\tau$ and $\tau'$ be two mappings from $I$ to $J$ such that $U_i \subset V_{\tau i}$
and $U_i \subset V_{\tau'_i}$ ; we have to show that $\tau^\ast = \tau'\ast$. 

Let $f\in C^q(\Bb, \Ff)$; set
\[ (kf)_{i_0\ldots i_{q-1}} = \sum_{h=0}^{h=q-1} (-1)^h \rho_h(f_{\tau i_0 \ldots
\tau i_j \tau' i_h \ldots \tau' i_{q-1}} ), \]
where $\rho_h$ denotes the restriction homomorphism defined by the inclusion of
$U_{i_0\ldots i_{q-1}}$ in $V_{\tau i_0 \ldots \tau i_j \tau' i_h \ldots \tau' i_{q-1}}$.

We verify by direct computation (cf. \cite{7}, Chapter VI, \S 3) that we have
\[ dkf + k\,df = \tau'f - \tau f, \]
which ends the proof of the Proposition.

% page 215

Thus, if $\Uu$ is finer than $\Bb$, there exists for every integer $q\geq 0$ a canonical
homomorphism from $\HH^q(\Bb, \Ff)$ to $\HH^q(\Uu, \Ff)$. From now on, this homomorphism
will be denoted by $\sigma(\Uu, \Bb)$.

\ssubsection{Cohomology groups of $X$ with values in a sheaf $\Ff$}
\label{section-\arabic{subsection}}

The relation ``$\Uu$ is finer than $\Bb$" (which we denote henceforth by $\Uu \prec \Bb$) is
a relation of a \emph{preorder}\footnote{i.e. quasiorder} between coverings of $X$; moreover, 
this relation is
\emph{filtered}\footnote{i.e. directed}, since if $\Uu=\{U_i\}_{i\in I}$ and 
$\Bb = \{ V_j \}_{j\in J}$ 
are two coverings, $\mathfrak{W} = \{ U_i\cap V_j \}_{(i,j)\in I\times J}$ is a covering
finer than $\Uu$ and than $\Bb$.

We say that two coverings $\Uu$ and $\Bb$ are equivalent if we have $\Uu\prec \Bb$ and
$\Bb \prec \Uu$. Any covering $\Uu$ is equivalent to a covering $\Uu'$ whose set of indices
is a subset of $\mathfrak{P}(X)$; in fact, we can take for $\Uu'$ the \emph{set} of open
subsets of $X$ belonging to the \emph{family} $\Uu$. We can thus speak of the set of classes
of coverings with respect to this equivalence relation; this is an ordered filtered set.
\footnote{To the contrary, we cannot speak about the "set" of coverings, because a covering
is a family whose set of indices is arbitrary.}

If $\Uu\prec\Bb$, we have defined at the end of the preceding \no a well defined homomorphism
$\sigma(\Uu, \Bb): \HH^q(\Bb, \Ff)\to\HH^q(\Uu, \Ff)$, defined for every integer $q\geq 0$
and every sheaf $\Ff$ on $X$. It is clear that $\sigma(\Uu, \Uu)$ is the identity and that
$\sigma(\Uu, \Bb)\circ\sigma(\Bb, \mathfrak{W}) = \sigma(\Uu, \mathfrak{W})$ if
$\Uu\prec\Bb\prec\mathfrak{W}$. It follows that, if $\Uu$ is equivalent to $\Bb$, then
$\sigma(\Uu, \Bb)$ and $\sigma(\Bb, \Uu)$ are inverse isomorphisms; in other words,
$\Hq(\Ff, \Uu)$ depends only on the class of the covering $\Uu$.

{\bf Definition. } We call the $q$-th cohomology group of $X$ with values in a sheaf $\Ff$,
and denote by $\Hq(X, \Ff)$, the inductive limit of groups $\Hq(\Uu, \Ff)$, where $\Uu$
runs over the filtered ordering of classes of coverings of $X$, with respect to the
homomorphisms $\sigma(\Uu, \Bb)$.

In other words, an element of $\Hq(X, \Ff)$ is just a pair $(\Uu, x)$ with $x\in\Hq(\Uu, \Ff)$,
and we identify two such pairs $(\Uu, x)$ and $(\Bb, y)$ whenever there exists a $\mathfrak{W}$
with $\mathfrak{W}\prec \Uu$, $\mathfrak{W}\prec \Bb$ and $\sigma(\mathfrak{W}, \Uu)(x)
= \sigma(\mathfrak{W}, \Bb)(y)$ in $\Hq(\mathfrak{W}, \Ff)$. Any covering $\Uu$ in $X$ is thus
associated a canonical homomorphism $\sigma(U): \Hq(\Uu, \Ff)\to\Hq(X, \Ff)$.

We will see that $\Hq(X, \Ff)$ can also be defined by an inductive limit of $\Hq(\Uu, \Ff)$
where $\Uu$ runs over a {\bf cofinal} family of coverings. Thus, if $X$ is quasi-compact
(resp. quasi-paracompact), we can consider only finite (resp. locally finite) coverings.

When $q=0$, by Proposition 1 we have:

{\bf Proposition 4.} $\Hz(X, \Ff) = \Gamma(X, \Ff)$.

\ssubsection{Homomorphisms of sheaves}
\label{section-\arabic{subsection}}

Let $\phi$ be a homomorphism from a sheaf $\Ff$ to a sheaf $\Gg$. If $\Uu$ is a covering
of $X$, we can assign to any $f\in C^q(\Uu, \Ff)$ an element $\phi f\in C^q(\Uu, \Gg)$
defined by the formula $(\phi f)_s = \phi(f_s)$. The mapping $f\mapsto \phi f$ is a homomorphism
from $C(\Uu, \Ff)$ to $C(\Uu, \Gg)$ commuting with the coboundary, thus it defines homomorphisms
$\phi^\ast: \Hq(\Uu, \Ff)\to\Hq(\Uu, \Gg)$. We have $\phi^\ast\circ\sigma(\Uu, \Bb)
= \sigma(\Uu, \Bb)\circ\psi^\ast$, hence, by passing to the limit, the homomorphisms
\[ \phi^\ast : \Hq(X, \Ff) \to \Hq(X, \Gg). \]

% page 216

When $q=0$, $\phi^\ast$ coincides with the homomorphism from $\Gamma(X, \Ff)$ to 
$\Gamma(X, \Gg)$ induced in the natural way by $\phi$.

In general, the homomorphisms $\phi^\ast$ satisfy usual formal properties:
\[ (\phi+\psi)^\ast = \phi^\ast + \psi^\ast, \quad (\phi\circ\psi)^\ast, 
\quad 1^\ast = 1. \]

In other words, for all $q\geq 0$, $\Hq(X, \Ff)$ is a covariant additive functor of $\Ff$.
Hence we gather that if $\Ff$ is the direct sum of two sheaves $\Gg_1$ and $\Gg_2$, then
$\Hq(X, \Ff)$ is the direct sum of $\Hq(X, \Gg_1)$ and $\Hq(X, \Gg_2)$.

Suppose that $\Ff$ is a sheaf of $\Aa$-modules. Any section of $\Aa$ on $X$ defines
an endomorphism of $\Ff$, therefore of $\Hq(X, \Ff)$. It follows that $\Hq(X, \Ff)$
are modules over the ring $\Gamma(X, \Aa)$.

\ssubsection{Exact sequence of sheaves: the general case}
\label{section-\arabic{subsection}}

Let $0\to\Aa\xto{\alpha}\Ba\xto{\beta}\Ca\to 0$ be an exact sequence of sheaves.
If $\Uu$ is a covering of $X$, the sequence
\[ 0\to C(\Uu, \Aa) \xto{\alpha} C(\Uu, \Ba) \xto{\beta} C(\Uu, \Ca) \]
is obviously exact, but the homomorphism $\beta$ need not be surjective in general.
Denote by $C_0(\Uu, \Ca)$ the image of this homomorphism; it is a subcomplex
of $C(\Uu, \Ca)$ whose cohomology groups will be denoted by $\Hq_0(\Uu, \Ca)$.
The exact sequence of complexes:
\[ 0\to C(\Uu, \Aa) \to C(\Uu, \Ba) \to C_0(\Uu, \Ca) \to 0 \]
giving rise to an exact sequence of cohomology:
\[ \ldots \to \Hq(\Uu, \Ba) \to \Hq_0(\Uu, \Ca) \xto{d} \HH^{q+1}(\Uu, \Aa)
   \to \HH^{q+1}(\Uu, \Ba) \to \ldots, \]
where the coboundary operator $d$ is defined as usual.

Now let $\Uu = \{U_i\}_{i\in I}$ and $\Bb = \{V_j\}_{j\in J}$ be two coverings
and let $\tau: I\to J$ be such that $U_i\subset V_{\tau i}$ ; we thus have $\Uu\prec\Bb$.
The commutative diagram:
\[
\begin{tikzpicture}[description/.style={fill=white,inner sep=2pt}]
    \matrix (m) [matrix of math nodes, row sep=3em,
    column sep=2.5em, text height=1.5ex, text depth=0.25ex]
    {   0 & C(\Bb, \Aa) & C(\Bb, \Ba) & C(\Bb, \Ca) \\
        0 & C(\Uu, \Aa) & C(\Uu, \Ba) & C(\Uu, \Ca) \\ };
    \path[->,font=\scriptsize]
    (m-1-1) edge node[auto] {$ $} (m-1-2)
    (m-1-2) edge node[auto] {$ $} (m-1-3)
    (m-1-3) edge node[auto] {$ $} (m-1-4);
    \path[->,font=\scriptsize]
    (m-2-1) edge node[auto] {$ $} (m-2-2)
    (m-2-2) edge node[auto] {$ $} (m-2-3)
    (m-2-3) edge node[auto] {$ $} (m-2-4);
    \path[->,font=\scriptsize]
    (m-1-2) edge node[auto] {$ \tau $} (m-2-2);
    \path[->,font=\scriptsize]
    (m-1-3) edge node[auto] {$ \tau $} (m-2-3);
    \path[->,font=\scriptsize]
    (m-1-4) edge node[auto] {$ \tau $} (m-2-4);
\end{tikzpicture}
\]
shows that $\tau$ maps $C_0(\Bb, \Ca)$ into $C_0(\Uu, \Ca)$, thus defining the
homomorphisms $\tau^\ast: \Hq_0(\Bb, \Ca)\to\Hq_0(\Uu, \Ca)$. Moreover, the
homomorphisms $\tau^\ast$ are independent of the choice of the mapping $\tau$:
this follows from the fact that, if $f\in C_0^q(\Bb, \Ca)$, we have 
$kf\in C_0^{q-1}(\Uu, \Ca)$, with the notations of the proof of Proposition 3.
We have thus obtained canonical homomorphisms $\sigma(\Uu, \Bb): \Hq_0(\Bb, \Ca)
\to \Hq_0(\Uu, \Ca)$; we might then define $\Hq_0(X, \Ca)$ as the inductive
limit of the groups $\Hq_0(\Uu, \Ca)$.

% page 217

Because an inductive limit of exact sequences is an exact sequence (cf. \cite{7},
Chapter VIII, theorem 5.4), we obtain:

{\bf Proposition 5.} \emph{The sequence}
\[ \ldots \to \Hq(X, \Ba) \xto{\beta^\ast} \Hq_0(X, \Ca) \xto{d} \HH^{q+1}(X, \Aa)
   \xto{\alpha^\ast} \HH^{q+1}(X, \Ba) \to \ldots \]
\emph{is exact.}

($d$ denotes the homomorphism obtained by passing to the limit with the homomorphisms
$d: \Hq_0(\Uu, \Ca) \to \HH^{q+1}(\Uu, \Aa)$).

To apply the preceding Proposition, it is convenient to compare
the groups $\Hq_0(X, \Ca)$ and $\Hq(X, \Ca)$. The inclusion of $C_0(\Uu, \Ca)$
in $C(\Uu, \Ca)$ defines the homomorphisms $\Hq_0(\Uu, \Ca)\to\Hq(\Uu, \Ca)$, hence,
by passing to the limit with $\Uu$, the homomorphisms:
\[ \Hq_0(X, \Ca)\to \Hq(X, \Ca). \]

{\bf Proposition 6.} \emph{The canonical homomorphism $\Hq_0(X, \Ca)\to \Hq(X, \Ca)$
is bijective for $q=0$ and injective for $q=1$.}

We will prove the following lemma:

{\bf Lemma 1.} \emph{Let $\Bb = \{V_j\}_{j\in J}$ be a covering and let $f=(f_j)$ be
an element of $C^0(\Bb, \Ca)$. There exists a covering $\Uu=\{U_i\}_{i\in I}$ and a 
mapping $\tau:I\to J$ such that $U_i\subset V_{\tau i}$ and $\tau f\in C_0^0(\Uu, \Ca)$.}

For any $x\in X$, take a $\tau x \in J$ such that $x\in V_{\tau x}$. Since $f_{\tau x}$
is a section of $\Ca$ over $V_{\tau x}$, there exists an open neighborhood $U_x$ of $x$,
contained in $V_{\tau x}$ and a section $b_x$ of $\Ba$ over $U_x$ such that
$\beta(b_x)=f_{\tau x}$ on $U_x$. The $\{U_x\}_{x\in X}$ form a covering $\Uu$ of $X$,
and the $b_x$ form a $0$-chain $b$ of $\Uu$ with values in $\Bb$; since
$\tau f = \beta(b)$, we have that $\tau f\in C_0^0(\Uu, \Ca)$.

We will now show that $\HH_0^1(X, \Ca)\to \HH^1(X, \Ca)$ is injective. An element
of the kernel of this mapping may be represented by a $1$-cocycle $z=(z_{j_0j_1})
\in C_0'(\Bb, \Ca)$ such that there exists an $f = (f_j)\in C^0(\Bb, \Ca)$ with
$df=z$; applying Lemma 1 to $f$ yields a covering $\Uu$ such that $\tau f \in
 C_0^0(\Uu, \Ca)$, which shows that $\tau z$ is cohomologous to 0 in $C_0(\Uu, \Ca)$,
thus its image in $\HH_0^1(X, \Ca)$ is 0. This shows that $\HH_0^0(X, \Ca)\to
\HH^0(X, \Ca)$ is bijective. 

{\bf Corollary 1.} \emph{We have an exact sequence:}
\[ 0\to \HH^0(X, \Aa) \to \HH^0(X, \Ba) \to \HH^0(X, \Ca) \to \HH^1(X, \Aa)
	\to \HH^1(X, \Ba) \to \HH^1(X, \Ca). \]

This is an immediate consequence of Propositions 5 and 6.

{\bf Corollary 2.} \emph{If $\HH^1(X, \Aa)=0$, then $\Gamma(X, \Ba)\to\Gamma(X, \Ca)$
is surjective.}

\ssubsection{Exact sequence of sheaves: the case of $X$ paracompact}
\label{section-\arabic{subsection}}

Recall that a space $X$ is said to be paracompact if it is separated and if any
covering of $X$ admits a locally finite finer covering. On paracompact spaces,
we can extend Proposition 6 for all values of $q$ (I do not know whether that extension
is possible for nonseparated spaces):

% page 218

{\bf Proposition 7.} \emph{If $X$ is paracompact, the canonical homomorphism}
\[ \Hq_0(X, \Ca) \to \Hq(X, \Ca) \]
\emph{is bijective for all $q\geq 0$.}

This Proposition is an immediate consequence of the following lemma, analogous
to Lemma 1:

{\bf Lemma 2.} \emph{Let $\Bb = \{V_j\}_{j\in J}$ be a covering, and let
$f = (f_{j_0\ldots j_q})$ be an element of $C^q(\Bb, \Ca)$. There exists
a covering $\Uu=\{U_i\}_{i\in I}$ and a mapping $\tau: I\to J$ such that
$U_i\subset V_{\tau i}$ and $\tau f\in C_0^q(\Uu, \Ca)$. }

Since $X$ is paracompact, we might assume that $\Bb$ is locally finite. Then there
exists a covering $\{ W_j \}_{j\in J}$ such that $W_j\subset V_j$. For every
$x\in X$, choose an open neighborhood $U_x$ of $x$ such that\\
\wciecie (a) If $x\in V_j$ (resp. $x\in W_j$), then $U_x \subset V_j$ 
(resp. $U_x\subset W_j$),\\
\wciecie (b) If $U_x\cap W_j \neq \emptyset$, then $U_x\subset W_j$,\\
\wciecie (c) If $x\in V_{j_0\ldots j_q}$, there exists a section $b$ of $\Ba$ over
$U_x$ such that $\beta(b) = f_{j_0\ldots j_q}$ over $U_x$.

The condition (c) can be satisfied due to the definition of the quotient sheaf
and to the fact that $x$ belongs to a finite number of sets $V_{j_0\ldots j_q}$.
Having (c) satisfied, it suffices to restrict $U_x$ appropriately to satisfy (a)
and (b).

The family $\{U_x\}_{x\in X}$ forms a covering $\Uu$; for any $x\in X$, choose
$\tau x\in J$ such that $x\in W_{\tau x}$. We now check that $\tau f$ belongs
to $C_0^q(U, \Ca)$, in other words, that $f_{\tau x_0 \ldots \tau x_q}$ is the
image by $\beta$ of a section of $\Ba$ over $U_{x_0}\cap\ldots\cap U_{x_q}$.
If $U_{x_0}\cap\ldots\cap U_{x_q}$ is empty, this is obvious; if not,
we have $U_{x_0}\cap U_{x_k}\neq\emptyset$ for $0\leq k\leq q$, and since
$U_{x_k}\subset U_{\tau x_k}$, we have $U_{x_0}\cap W_{\tau x_k}\neq\emptyset$,
which implies by (b) that $U_{x_0}\subset V_{\tau x_k}$, hence 
$x_0\in V_{\tau x_0\ldots \tau x_q}$; we then apply (c), seeing that
there exists a section $b$ of $\Ba$ over $U_{x_0}$ such that
$\beta(b)_x = f_{\tau x_0\ldots\tau x_q}$ on $U_{x_0}$, so also on
$U_{x_0}\cap\ldots\cap U_{x_q}$, which ends the proof.

{\bf Corollary.} \emph{If $X$ is paracompact, we have an exact sequence: }
\[ \ldots \to \Hq(X, \Ba) \xto{\beta^\ast} \Hq(X, \Ca)
   \xto{d} \HH^{q+1}(X, \Aa) \xto{\alpha^\ast} \HH^{q+1}(X, \Ba) \to \ldots \]

(the map $d$ being defined as the composition of the inverse of the isomorphism
$\HH_0^q (X, \Ca)\to \Hq(X, \Ca)$ with $d: \HH_0^q(X, \Ca)\to\HH^{q+1}(X, \Aa)$).

The exact sequence mentioned above is called the \emph{exact sequence of
cohomology} defined by a given exact sequence of sheaves $0\to\Aa\to\Ba\to\Ca\to 0$.
More generally, it exists whenever we can show that $\HH_0^q(X, \Ca)\to \HH^q(X, \Ca)$
is bijective (we will see in \no \ref{section-47} that this is the case when $X$ is an algebraic
variety and when $\Aa$ is an algebraic coherent sheaf).

\ssubsection{Cohomology of a closed subspace}
\label{section-\arabic{subsection}}

Let $\Ff$ be a sheaf over a space $X$, and let $Y$ be a subspace of $Y$. Let
$\Ff(Y)$ be the sheaf induced by $\Ff$ on $Y$, in the sense of \no \ref{section-4}.
If $\Uu = \{ U_i \}_{i\in I}$ is a covering of $X$, the sets $U'_i = Y\cap U_i$
form a covering $\Uu'$ of $Y$; if $f_{i_0\ldots i_Q}$ is a section of $\Ff$
over $U_{i_0\ldots i_q}$, the restriction of $f_{i_0\ldots i_q}$ to 
$U'_{i_0\ldots i_q} = Y\cap U_{i_0\ldots i_q}$ is a section of $\Ff(Y)$.
The operation of restriction is a homomorphism $\rho: C(\Uu, \Ff)\to C(\Uu', \Ff(Y))$,
commuting
%
% page 219
%
with $d$, thus defining $\rho^\ast: \Hq(\Uu, \Ff)\to\Hq(\Uu', \Ff(Y))$. If $\Uu\prec\Bb$,
we have $\Uu'\prec\Bb'$, and $\rho^\ast\circ\sigma(\Uu,\Bb) = \sigma(\Uu', \Bb')\circ
\rho^\ast$; thus the homomorphisms $\rho^\ast$ define, by passing to the limit with
$\Uu$, homomorphisms $\rho^\ast: \Hq(X, \Ff)\to\Hq(Y, \Ff(Y))$.

{\bf Proposition 8.} \emph{Assume that $Y$ is closed in $X$ and that $\Ff$ is zero
outside $Y$. Then $\rho^\ast: \Hq(X, \Ff)\to\Hq(Y, \Ff(Y))$ is bijective for
all $q\geq 0$. }

The Proposition is implied by the following facts:\\
\wciecie (a) Any covering $\mathfrak{W} = \{ W_i \}_{i\in I}$ of $Y$ is of the form $\Uu'$
for some covering $\Uu$ of $X$. \\
\wciecie Indeed, it suffices to put $U_i=W_i\cup (X-Y)$, since $Y$ is closed in $X$.\\
\wciecie (b) For any covering $\Uu$ of $X$, $\rho: C(\Uu, \Ff)\to C(\Uu', \Ff(Y))$
is bijective.
\wciecie Indeed, the result follows from Proposition 5 of \no \ref{section-5}, applied to
$U_{i_0\ldots i_q}$ and the sheaf $\Ff$. 

We can also express Proposition 8 in the following manner: If $\Gg$ is a sheaf
on $Y$, and if $\Gg^X$ is the sheaf obtained by extending $\Gg$ by 0 outside $Y$,
we have $\Hq(Y, \Gg) = \Hq(X, \Gg^X)$ for all $q\geq 0$; in other words,
the identification of $\Gg$ with $\Gg^X$ is compatible with passing to cohomology
groups.

