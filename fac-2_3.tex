% page 233

\ssection{Coherent algebraic sheaves on affine varieties}
\setcounter{subsection}{41}

\ssubsection{Affine varieties}
\label{section-\arabic{subsection}}

An algebraic variety $V$ is said to be \emph{affine} if it
is isomorphic to a closed subvariety of an affine space. The product of
two 
%
% page 234
%
affine varieties is an affine variety; any closed subvariety of an affine
variety is an affine variety.

An open subset $U$ of an algebraic variety $V$ is said to be \emph{affine}
if, equipped with the structure of an algebraic variety induced from $X$,
it is an affine variety.

{\bf Proposition 1.} \emph{Let $U$ and $V$ be two open subsets of an algebraic
variety $X$. If $U$ and $V$ are affine, $U\cap V$ is affine.}

Let $\Delta$ be the diagonal of $X\times X$; by \no \ref{section-35}, the mapping
$x\mapsto (x, x)$ is a biregular isomorphism from $X$ onto $\Delta$; thus
the restriction of this map to $U\cap V$ is a biregular isomorphism 
of $U\cap V$ onto $\Delta\cap U\times V$. Since $U$ and $V$ are
affine varieties, $U\times V$ is also an affine variety; on the other hand,
$\Delta$ is closed in $X\times X$ by the axiom ($VA_{II}$), thus $\Delta
\cap U\times V$ is closed in $U\times V$, hence affine, q.e.d.

(It is easily seen that this Proposition is false for prealgebraic 
varieties; the axiom ($VA_{II}$) plays an essential role).

Let us now introduce a notation which will be used thorough the rest of
this paragraph: if $V$ is an algebraic variety and $f$ is a regular
function on $V$, we denote by $V_f$ the open subset of $V$ consisting 
of all points $x\in V$ for which $f(x)\neq 0$.

{\bf Proposition 2.} \emph{If $V$ is an affine algebraic variety and $f$
is a regular function on $V$, the open subset $V_f$ is affine.}

Let $W$ be the subset of $V\times K$ consisting of pairs $(x, \lambda)$
such that $\lambda\cdot f(x)=1$; it is clear that $W$ is closed in 
$V\times K$, thus it is an affine variety. For all $(x, \lambda)\in W$
set $\pi(x, \lambda) = x$; the mapping $\pi$ is a regular mapping
from $W$ to $V_f$. Conversely, for all $x\in V_f$, set $\omega(x)
= (x, 1/f(x))$; the mapping $\omega: V_f\to W$ is regular and we have
$\pi\circ\omega = 1$, $\omega\circ\pi = 1$, thus $V_f$ and $W$
are isomorphic, q.e.d.

{\bf Proposition 3.} \emph{Let $V$ be a closed subvariety of $K^r$, $F$
be a closed subset of $V$ and let $U = V - F$. The open subsets $V_P$ form
a base for the topology of $U$ when $P$ runs over the set of polynomials
vanishing on $F$.}

Let $U' = V - F'$ be an open subset of $U$ and let $x \ in U'$; we must show
that there exists a $P$ for which $V_P\subset U'$ and $x\in V_P$; in other words,
$P$ has to be zero on $F'$ and nonzero in $x$; the existence of such a polynomial
follows simply from the definition of the topology of $K^r$.

{\bf Theorem 1.} \emph{The open affine subsets of an algebraic variety $X$ form
an open base for the topology of $X$.}

The question being local, we can assume that $X$ is a locally closed subspace
of an affine space $K^r$; in this case, the theorem follows immediately
from Propositions 2 and 3.

{\bf Corollary.} \emph{The coverings of $X$ consisting of open affine subsets
are arbitrarily fine.}

We note that if $\Uu = \{U_i\}_{i\in I}$ is such a covering, the
$U_{i_0\ldots i_p}$ are also open affine subsets, by Proposition 1.

\ssubsection{Some preliminary properties of irreducible varieties}
\label{section-\arabic{subsection}}

Let $V$ be a 
%
% page 235
%
closed subvariety of $K^r$ and let $I(V)$ be the ideal of $K[X_1, \ldots, X_r]$
consisting of polynomials vanishing on $V$; let $A$ be the quotient ring
$K[X_1, \ldots, X_r]/I(V)$; we have a canonical homomorphism
\[ \iota : A \to \Gamma(V, \Oo_V) \]
that is injective by the definition of $I(V)$.

{\bf Proposition 4.} \emph{If $V$ is irreducible, $\iota: A\to \Gamma(V, \Oo_V)$
is bijective.}

(In fact, this holds for any closed subvariety of $K^r$, as will be shown in
the next \no).

Let $K(V)$ be the field of fractions of $A$; by \no \ref{section-36}, we can identify $\Oo_{x, V}$
with the localization of $A$ in the maximal ideal $\mm_x$ consisting of polynomials
vanishing in $x$, and we have $\Gamma(V, \Oo_V) = A = \bigcap_{x\in V} \Oo_{x, V}$
(all $\Oo_{x, V}$ being considered as subrings of $K(V)$). But all maximal ideals
of $A$ are $\mm_x$, since $K$ is algebraically closed (Hilbert's theorem of zeros);
it follows immediately (cf. \cite{8}, Chap. XV, \S 5, th. X) that
$A = \bigcap_{x\in V} \Oo_{x, V} = \Gamma(V, \Oo_V)$, q.e.d.

{\bf Proposition 5.} \emph{Let $X$ be an irreducible algebraic variety, $Q$
a regular function on $X$ and $P$ a regular function on $X_Q$. Then, for $n$
sufficiently large, the rational function $Q^n P$ is regular on the whole of $X$.}

By quasi-compactness of $X$, the question is local; by Theorem 1, we can thus
suppose that $X$ is a closed subvariety of $K^r$. The above Proposition 
shows that then $Q$ is an element of $A = K[X_1,\ldots, X_r]/(I(X))$.
The assumption made on $P$ means that for any point $x\in X_Q$ we can write
$P = P_x/Q_x$ with $P_x$ and $Q_x$ in $A$ and $Q_x(x)\neq 0$; if $\ai$
denotes the ideal of $A$ generated by all $Q_x$, the variety of zeros of $\ai$
is contained in the variety of zeros of $Q$; by Hilbert's theorem of zeros,
this leads to $Q^n\in\ai$ for $n$ sufficiently large, hence $Q^n = \sum R_x Q_x$
and $Q^n P = \sum R_x P_x$ with $R_x \in A$, which shows that $Q^n P$ is regular
on $X$.

(We could also use the fact that $X_Q$ is affine if $X$ is and apply Proposition 4
to $X_Q$).

{\bf Proposition 6.} \emph{Let $X$ be an irreducible algebraic variety, $Q$
a regular function on $X$, $\Ff$ a coherent algebraic sheaf on $X$ and $s$
a section of $\Ff$ over $X$ whose restriction to $X_Q$ is zero. Then for
$n$ sufficiently large the section $Q^n s$ is zero on the whole of $X$.}

The question being again local, we can assume: \\
\wciecie (a) that $X$ is a closed subvariety of $K^r$, \\
\wciecie (b) that $\Ff$ is isomorphic to a cokernel of a homomorphism $\phi: \Oo_X^p
\to \Oo_X^q$, \\
\wciecie (c) that $s$ is the image of a section $\sigma$ of $\Oo_X^q$.

(Indeed, all the above conditions are satisfied locally).

Set $A = \Gamma(X, \Oo_X) = K[X_1, \ldots, X_r]/I(X)$. The section $\sigma$ can be
identified with a system of $q$ elements of $A$. Let on the other hand
\[ t_1 = \phi(1, 0, \ldots, 0), \ldots, t_p = \phi(0, \ldots, 0, 1); \]
the $t_i$, $1\leq i\leq p$ are sections of $\Oo_X^q$ over $X$, thus can be
identified with systems of $q$ elements of $A$. The assumption made on $s$
means that 
%
% page 236
%
for all $x\in X_Q$ we have $\sigma(x)\in\phi(\Oo_{x, X}^p)$, that is, $\sigma$
can be written in the form $\sigma = \sum_{i=1}^{i=p} f_i\cdot t_i$ with
$f_i\in\Oo_{x, X}$; or, by clearing denominators, that there exist $Q_x\in A$,
$Q_x(x)\neq 0$ for which $Q_x\cdot\sigma = \sum_{i=1}^{i=p} R_i\cdot t_i$ with
$R_i \in A$. The reasoning used above shows then that, for $n$ sufficiently large,
$Q^n$ belongs to the ideal generated by $Q_x$, hence $Q^n \sigma(x)\in
\phi(\Oo_{x,X}^p)$ for all $x\in X$, which means that $Q^n s$ is zero on
the whole of $X$.

\ssubsection{Vanishing of certain cohomology groups}
\label{section-\arabic{subsection}}

{\bf Proposition 7.} \emph{Let $X$ be an irreducible algebraic variety, $Q_i$
a finite family of regular functions on $X$ that do not vanish simultaneously
and $\Uu$ the open covering of $X$ consisting of $X_{Q_i}= U_i$. If $\Ff$ is
a coherent algebraic subsheaf of $\Oo_X^p$, we have $\Hq(\Uu, \Ff) = 0$
for all $q>0$.}

Possibly replacing $\Uu$ by an equivalent covering, we can assume that none
of the functions $Q_i$ vanishes identically, in other words that we have
$U_i\neq \emptyset$ for all $i$.

Let $f = (f_{i_0\ldots i_q})$ be a $q$-cocycle of $\Uu$ with values in $\Ff$.
Each $f_{i_0\ldots i_q}$ is a section of $\Ff$ over $U_{i_0\ldots i_q}$, thus
can be identified with a system of $p$ regular functions on $U_{i_0\ldots i_q}$;
applying Proposition 5 to $Q = Q_{i_0}\ldots Q_{i_q}$ we see that, for $n$
sufficiently large, $g_{i_0\ldots i_q} = (Q_{i_0}\ldots Q_{i_q})^n f_{i_0\ldots i_q}$
is a system of $p$ regular functions on $X$. Choose an integer $n$ for which this
holds for all systems $i_0,\ldots, i_q$, which is possible because there is a finite
number of such systems. Consider the image of $g_{i_0\ldots i_q}$ in the coherent
sheaf $\Oo_X^p/\Ff$; this is a section vanishing on $U_{i_0\ldots i_q}$; then
applying Proposition 6 we see that for $m$ sufficiently large, the product of
this section with $(Q_{i_0}\ldots Q_{i_q})^m$ is zero on the whole of $X$.
Setting $N = m + n$, we see that we have constructed sections $h_{i_0\ldots i_q}$
of $\Ff$ over $X$ which coincide with $(Q_{i_0}\ldots Q_{i_q})^N f_{i_0\ldots i_q}$
on $U_{i_0\ldots i_q}$.

As the $Q_i^N$ do not vanish simultaneously, there exist functions
\[ R_i \in \Gamma(X, \Oo_X) \]
such that $\sum R_i Q_i^N = 1$. Then for any system $i_0, \ldots, i_{q-1}$ set
\[ k_{i_0\ldots i_{q-1}} = \sum_i R_i h_{i i_0 \ldots i_{q-1}}/(Q_{i_0}\ldots
Q_{i_{q-1}})^N, \]
which makes sense because $Q_{i_0}\ldots Q_{i_{q-1}}$ is nonzero on
$U_{i_0\ldots i_{q-1}}$.

We have thus defined a cochain $k\in C^{q-1}(\Uu, \Ff)$. I claim that 
$f = dk$, which will show the Proposition.

We must check that $(dk)_{i_0\ldots i_q} = f_{i_0\ldots i_q}$; it suffices to
show that these two sections coincide on $U = \bigcap U_i$, since they will
coincide everywhere, because they are systems of $p$ rational functions on $X$
and $U\neq 0$. Now over $U$, we can write
\[ k_{i_0\ldots i_{q-1}} = \sum_i R_i \cdot Q_i^N \cdot f_{ii_0
\ldots i_q}, \]
hence
\[ (dk)_{i_0\ldots i_q} = \sum_{j=0}^{j=q} (-1)^q
   \sum_i R_i \cdot Q_i^N \cdot f_{ii_0\ldots\hat i_j\ldots i_q} \]
and taking into account that $f$ is a cocycle,
\[ (dk)_{i_0\ldots i_q} = \sum_i R_i\cdot Q_i^N\cdot f_{i_0\ldots i_q}
 	= f_{i_0\ldots i_q}, \eqno q.e.d. \]

{\bf Corollary 1.} \emph{$\Hq(X, \Ff) = 0$ for $q>0$.}

Indeed, Proposition 3 shows that coverings of the type used in Proposition 7
are arbitrarily fine.

{\bf Corollary 2.} \emph{The homomorphism $\Gamma(X, \Oo_X^p)\to\Gamma(X,\Oo_X^p/\Ff)$
is surjective.}

This follows from Corollary 1 above and from Corollary 2 to Proposition 6 from
\no \ref{section-24}.

{\bf Corollary 3.} \emph{Let $V$ be a closed subvariety of $K^r$ and let
\[ A = K[X_1,\ldots, X_r]/I(V). \]
Then the homomorphism $\iota: A\to \Gamma(V, \Oo_V)$ is bijective. }

We apply Corollary 2 above to $X = K^r$, $p=1$, $\Ff=\Jj(V)$, the sheaf of
ideals defined by $V$; we obtain that every element of $\Gamma(V, \Oo_V)$ is
the restriction of a section of $\Oo$ on $X$, that is, a polynomial, by
Proposition 4 applied to $X$.

\ssubsection{Sections of a coherent algebraic sheaf on an affine variety}
\label{section-\arabic{subsection}}

{\bf Theorem 2.} \emph{Let $\Ff$ be a coherent algebraic sheaf on an affine variety
$X$. For every $x\in X$, the $\Oo_{x, X}$--module $\Ff_x$ is generated by elements
of $\Gamma(X, \Ff)$.}

Since $X$ is affine, it can be embedded as a closed subvariety of an affine space
$K^r$; by extending the sheaf $\Ff$ by 0 outside $X$, we obtain a coherent algebraic
sheaf on $K^r$ (cf. \no \ref{section-39}) and we are led to prove the theorem for the new sheaf.
In other words, we can suppose that $X = K^r$.

By the definition of a coherent sheaf, there exists a covering of $X$ consisting
of open subsets on which $\Ff$ is isomorphic with a quotient of the sheaf $\Oo^p$.
Applying Proposition 3, we see that there exists a finite number of polynomials
$Q_i$ that do not vanish simultaneously and such that on every $U_i = X_{Q_i}$
there exists a surjective homomorphism $\phi_i: \Oo^{p_i}\to\Ff$; we can
furthermore assume that none of the polynomials is identically zero.

The point $x$ belongs to one $U_i$, say $U_0$; it is clear that $\Ff_x$ is
generated by sections of $\Ff$ over $U_0$; as $Q_0$ is invertible in $\Oo_x$,
it suffices to prove the following lemma:

{\bf Lemma 1.} \emph{If $s_0$ is a section of $\Ff$ over $U_0$, there exists
an integer $N$ and a section $s$ of $\Ff$ over $X$ such that $s = Q_0^N\cdot s_0$
over $U_0$.}

By Proposition 2, $U_i\cap U_0$ is an affine variety, obviously irreducible;
by applying Corollary 2 of Proposition 7 to this variety and to
$\phi_i : \Oo^{p_i}\to \Ff$, we see that there exists a section $\sigma_{0i}$
of $\Oo^{p_i}$ on $U_i\cap U_0$ such that $\phi_i(\sigma_{0i}) = s_0$
on $U_i\cap U_0$; as $U_i\cap U_0$ is the set of points of $U_i$ in which
$Q_0$ does not vanish, we can apply Proposition 5 to $X = U_i$, $Q = Q_0$
and we see that there exists, for $n$ sufficiently large, a section $\sigma_i$
of $\Oo^{p_i}$ over $U_i$ which coincides with $Q_0^n\cdot \sigma_{0i}$
over $U_i\cap U_0$; by setting $s'_i = \phi_i(\sigma_i)$, we
%
% page 238
%
obtain a section of $\Ff$ over $U_i$ that coincides with $Q_0^n\cdot s_0$
over $U_i\cap U_0$. The sections $s'_i$ and $s'_j$ coincide on $U_i\cap 
U_j \cap U_0$; applying Proposition 6 to $s'_i - s'_j$, we see that for $m$
sufficiently large we have $Q_0^m(s'_i - s'_j) = 0$ on the whole of
$U_i\cap U_j$. The $Q_0^m\cdot s'_i$ then define a unique section $s$ of
$\Ff$ over $X$, and we have $s = Q_0^{n+m} s_0$ on $U_0$, which shows the lemma
and completes the proof of Theorem 2.

{\bf Corollary 1.} \emph{The sheaf $\Ff$ is isomorphic to a quotient sheaf
of the sheaf $\Oo_X^p$.}

Because $\Ff_x$ is an $\Oo_{x, X}$--module of finite type, it follows from the
above theorem that there exists a finite number of sections of $\Ff$ generating
$\Ff_x$; by Proposition 1 of \no \ref{section-12}, these sections generate $\Ff_y$ for $y$
sufficiently close to $x$. The space $X$ being quasi-compact, we conclude that
there exists a finite number of sections $s_1, \ldots, s_p$ of $\Ff$ generating
$\Ff_x$ for all $x\in X$, which means that $\Ff$ is isomorphic to a quotient
sheaf of the sheaf $\Oo_X^p$.

{\bf Corollary 2.} \emph{Let $\Aa\xto{\alpha}\Ba\xto{\beta}\Ca$ be an exact
sequence of coherent algebraic sheaf on an affine variety $X$. The sequence
$\Gamma(X, \Aa)\xto{\alpha} \Gamma(X, \Ba)\xto{\beta}\Gamma(X, \Ca)$ is also
exact.}

We can suppose, as in the proof of Theorem 2, that $X$ is an affine space $K^r$,
thus is irreducible. Set $\Jj = \Im(\alpha) = \Ker(\beta)$; everything reduces
to seeing that $\alpha: \Gamma(X, \Aa)\to \Gamma(X, \Jj)$ is surjective.
Now, by Corollary 1, we can find a surjective homomorphism $\phi: \Oo_X^p
\to \Aa$ and, by Corollary 2 to Proposition 7, $\alpha\circ\phi: \Gamma(X, \Oo_X^p)
\to \Gamma(X, \Jj)$ is surjective; this is a fortiori the same for $\alpha: \Gamma(X, \Aa)
\to \Gamma(X, \Jj)$, q.e.d. 

\ssubsection{Cohomology groups of an affine variety with values in a coherent
algebraic sheaf}
\label{section-\arabic{subsection}}

{\bf Theorem 3.} \emph{Let $X$ be an affine variety, $Q_i$ a finite family of
regular functions on $X$ that do not vanish simultaneously and let
$\Uu$ be the open covering of $X$ consisting of $X_{Q_i} = U_i$. If $\Ff$
is a coherent algebraic sheaf on $X$, we have $\Hq(\Uu, \Ff) = 0$ for 
all $q>0$.}

Assume first that $X$ is irreducible. By Corollary 1 to Theorem 2, we can
find an exact sequence
\[ 0 \to \Rr \to \Oo_X^p \to \Ff \to 0. \]
The sequence of complexes: $0\to C(\Uu, \Rr)\to C(\Uu, \Oo_X^p)\to C(\Uu, \Ff)\to 0$
is \emph{exact}; indeed, this reduces to saying that every section of $\Ff$ over
$U_{i_0\ldots i_q}$ is the image of a section of $\Oo_X^p$ over $U_{i_0\ldots i_q}$,
which follows from Corollary 2 to Proposition 7 applied to the irreducible variety
$U_{i_0\ldots i_q}$. This exact sequence gives birth to an exact sequence
of cohomology:
\[ \ldots \to \Hq(\Uu, \Oo_X^p) \to \Hq(\Uu, \Ff) \to \HH^{q+1}(\Uu, \Rr)\to \ldots,\]
and as $\Hq(\Uu, \Oo_X^p) = \HH^{q+1}(\Uu, \Rr) = 0$ for $q>0$ by Proposition 7,
we conclude that $\Hq(\Uu, \Ff) = 0$.

% page 239

We proceed now to the general case. We can embed $X$ as a closed subvariety
of an affine space $K^r$; by Corollary 3 to Proposition 7, the functions
$Q_i$ are induced by polynomials $P_i$; let on the other hand $R_j$ be
a finite system of generators of the ideal $I(X)$. The functions $P_i$, $R_j$
do not vanish simultaneously on $K^r$, thus define an open covering $\Uu'$
of $K^r$; let $\Ff'$ be the sheaf obtained by extending $\Ff$ by 0 outside
$X$; applying what we have proven to the space $K^r$, the functions $P_i$, $R_j$
and the sheaf $\Ff'$, we see that $\Hq(\Uu', \Ff') = 0$ for $q>0$. As we can
immediately verify that the complex $C(\Uu', \Ff')$ is isomorphic to the
complex $C(\Uu, \Ff)$, it follows that $\Hq(\Uu, \Ff) = 0$, q.e.d.

{\bf Corollary 1.} \emph{If $X$ is an affine variety and $\Ff$ a coherent 
algebraic sheaf on $X$, we have $\Hq(X, \Ff) = 0$ for all $q>0$.}

Indeed, the coverings used in the above theorem are arbitrarily fine.

{\bf Corollary 2.} \emph{Let $0\to\Aa\to\Ba\to\Ca\to 0$ be an exact sequence
of sheaves on an affine variety $X$. If the sheaf $\Aa$ is coherent algebraic,
the homomorphism $\Gamma(X, \Bb) \to \Gamma(X, \Ca)$ is surjective.}

This follows from Corollary 1, by setting $q=1$.

\ssubsection{Coverings of algebraic varieties by open affine subsets}
\label{section-\arabic{subsection}}

{\bf Proposition 8.} \emph{Let $X$ be an affine variety and let
$\Uu = \{U_i\}_{i\in I}$ be a finite covering of $X$ by open affine subsets.
If $\Ff$ is a coherent algebraic sheaf on $X$, we have $\Hq(\Uu, \Ff) = 0$
for all $q>0$.}

By Proposition 3, there exist regular functions $P_j$ on $X$ such that
the covering $\Bb = \{X_{P_j}\}$ is finer than $\Uu$. For every $(i_0,\ldots, i_p)$,
the covering $\Bb_{i_0, \ldots, i_p}$ induced by $\Bb$ on $U_{i_0\ldots i_p}$
is defined by restrictions of $P_j$ to $U_{i_0\ldots i_p}$; as $U_{i_0\ldots i_p}$
is an affine variety by Proposition 1, we can apply Theorem 3 to it and conclude
that $\Hq(\Bb_{i_0\ldots i_p}, \Ff) = 0$ for all $q>0$. Applying then Proposition 5
of \no \ref{section-29}, we see that
\[ \Hq(\Uu, \Ff) = \Hq(\Bb, \Ff), \]
and, as $\Hq(\Bb, \Ff) = 0$ for $q>0$ by Theorem 3, the Proposition is proven.

{\bf Theorem 4.} \emph{Let $X$ be an algebraic variety, $\Ff$ a coherent algebraic
sheaf on $X$ and $\Uu = \{U_i\}_{i\in I}$ a finite covering of $X$ by open affine
subsets. The homomorphism $\sigma(\Uu) : \HH^n(\Uu, \Ff)\to \HH^n(X, \Ff)$
is bijective for all $n\geq 0$.}

Consider the family $\Bb^\alpha$ of all finite coverings of $X$ by open affine subsets.
By the corollary of Theorem 1, these coverings are arbitrarily fine. On the other
hand, for every system $(i_0, \ldots, i_p)$ the covering $\Bb^\alpha_{i_0\ldots i_p}$
induced by $\Bb^\alpha$ on $U_{i_0\ldots i_p}$ is a covering by open affine subsets,
by Proposition 1; by Proposition 8, we thus have $\Hq(\Bb^\alpha_{i_0\ldots i_p}, \Ff)
=0$ for $q>0$. The conditions (a) and (b) of Theorem 1, \no \ref{section-29} are satisfied
and the theorem follows.

{\bf Theorem 5.} \emph{Let $X$ be an algebraic variety and $\Uu=\{U_i\}_{i\in I}$
%
% page 240
%
a finite covering of $X$ by open affine subsets. Let $0\to \Aa\to\Ba\to\Ca\to 0$
be an exact sequence of sheaves on $X$, the sheaf $\Aa$ being coherent algebraic.
The canonical homomorphism $\Hq_0(\Uu, \Ca)\to \Hq(\Uu, \Ca)$ (cf. \no \ref{section-24}) is
bijective for all $q\geq 0$.}

It obviously suffices to prove that $C_0(\Uu, \Ca)=C(\Uu, \Ca)$, that is,
that every section of $\Ca$ over $U_{i_0\ldots i_q}$ is the image of a section
of $\Ba$ over $U_{i_0\ldots i_q}$, which follows from Corollary 2 of Theorem 3.

{\bf Corollary 1.} \emph{Let $X$ be an algebraic variety and let 
$0\to \Aa\to \Ba\to\Ca\to 0$ be an exact sequence of sheaves on $X$, the sheaf
$\Aa$ being coherent algebraic. The canonical homomorphism
$\Hq_0(X, \Ca) \to \Hq(X, \Ca)$ is bijective for all $q\geq 0$.}

This is an immediate consequence of Theorems 1 and 5.

{\bf Corollary 2.} \emph{We have an exact sequence:
\[ \ldots \to \Hq(X, \Ba) \to \Hq(X, \Ca) \to \HH^{q+1}(X, \Aa)\to \HH^{q+1}(X, \Ba)
\to \ldots \]
}





