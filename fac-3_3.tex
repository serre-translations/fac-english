% page 253

\ssection{Cohomology of the projective space with values in a coherent algebraic sheaf}
\setcounter{subsection}{60}

\ssubsection{The complexes $C_k(M)$ and $C(M)$}
\label{section-\arabic{subsection}}

We preserve the notations of \nos \ref{section-51} and \ref{section-56}. In particular, $I$ will denote the
interval $\{0, 1, \ldots, r\}$ and $S$ will denote the graded algebra
$K[t_0, \ldots, t_r]$.

Let $M$ be a graded $S$--module, $k$ and $q$ two integers $\geq 0$; we
define a group $C_k^q(M)$: an element of $C_k^q(M)$ is a mapping
\[ (i_0, \ldots, i_q) \mapsto m\langle i_0 \ldots i_q\rangle \]
which associates to every sequence $(i_0, \ldots, i_q)$ of $q+1$ elements of $I$
a homogeneous element of degree $k(q+1)$ of $M$, depending in an alternating
way on $i_0, \ldots, i_q$. In particular, we have $m\langle i_0\ldots i_q\rangle
= 0$ if two of the indices $i_0, \ldots, i_q$ are equal. We define addition
in $C_k^q(M)$ in the obvious way. the same with multiplication by an element
$\lambda\in K$, and $C_k^q(M)$ is a \emph{vector space over $K$}.

If $m$ is an element of $C_k^q(M)$, we define $dm\in C_k^{q+1}(M)$ by
the formula:
\[ (dm)\langle i_0\ldots i_{q+1}\rangle 
	= \sum_{j=0}^{j=q+1} (-1)^j t_{i_j}^k\cdot m\langle 
	i_0\ldots \hat{i}_j\ldots i_{q+1}\rangle. \]

We verify by a direct calculation that $d\circ d = 0$; thus, the direct
sum $C_k(M) = \bigoplus_{q=0}^{q=r} C_k^q(M)$, equipped with the coboundary
operator $d$, is a \emph{complex}, whose $q$-th cohomology group is denoted
by $\Hq_k(M)$.

(We note, after \cite{11}, another interpretation of the elements of $C_k^q(M)$:
introduce $r+1$ differential symbols $dx_0, \ldots, dx_r$ and associate
to every $m\in C_k^q(M)$ a ,,differential form'' of degree $q+1$:
\[ \omega_m = \sum_{i_0<\ldots <i_q} m\langle i_0\ldots i_q\rangle
	dx_{i_0}\wedge\ldots\wedge dx_{i_q}. \]
If we put $\alpha_k = \sum_{i=0}^{i=r} t_i^k dx_i$, we see that we have:
\[ \omega_{dm} = \alpha_k\wedge\omega_m, \]
%
% page 254
%
in other words, the coboundary operation is transformed into the exterior
multiplication by the form $\alpha_k$).

If $h$ is an integer $\geq k$, let $\rho_k^h: C_k^q(M)\to C_h^q(M)$ be
the homomorphism defined by the formula:
\[ \rho_k^h(m)\langle i_0\ldots i_q\rangle = 
	(t_{i_0}\ldots t_{i_q})^{h-k} m\langle i_0\ldots i_q\rangle.\]

We have $\rho_k^h\circ d = d\circ \rho_k^h$ and $\rho_h^l\circ\rho_k^j=\rho_k^l$
if $k\leq h\leq l$. We can thus define a complex $C(M)$, the inductive limit
of the system $(C_k(M), \rho_k^h)$ for $k\to +\infty$. The cohomology groups of
this complex are denoted $H^q(M)$. Because cohomology commutes with inductive
limits (cf. \cite{6}, Chap. V, Prop. 9.3*), we have:
\[ H^q(M) = \lim_{k\to\infty} H_k^q(M). \]

Every homomorphism $\phi: M\to M'$ defines a homomorphism
\[ \phi: C_k(M) \to C_k(M') \]
by the formula: $\phi(m)\langle i_0\ldots i_q\rangle = \phi(m\langle i_0
\ldots i_q\rangle)$, hence, by passing to the limit, $\phi:C(M)\to C(M')$;
moreover, these homomorphisms commute with boundary and thus define 
the homomorphisms
\[ \phi: Hq_k(M)\to Hq_k(M') \quad\text{and}\quad \phi:\Hq(M)\to\Hq(M').\]
If we have an exact sequence $0\to M\to M'\to M''\to 0$, we have an exact
sequence of complexes $0\to C_k(M)\to C_k(M')\to C_k(M'')\to 0$, hence an
exact sequence of cohomology:
\[ \ldots \Hq_k(M')\to \Hq_k(M'')\to \HH^{q+1}_k(M)\to \HH^{q+1}_k(M')\to\ldots \]
The same results for $C(M)$ and $\Hq(M)$.

{\bf Remark.} We shall see later (cf. \no \ref{section-69}) that we can express $\Hq_k(M)$
in terms of $\Ext_S^q$.

\ssubsection{Calculation of $\Hq_k(M)$ for certain modules $M$}
\label{section-\arabic{subsection}}

Let $M$ be a graded $S$--module and $m\in M$ a homogeneous element of degree 0.
The system of $(t_i^k\cdot m)$ is a 0-cocycle of $C_k(M)$, which we denote
by $\alpha^k(m)$ and identify with its cohomology class. We so obtain
a $K$-linear homomorphism $\alpha^k:M_0\to \HH^0_k(M)$; as $\alpha^h
=\rho_k^h\circ\alpha^k$ if $h\geq k$, the $\alpha^k$ define by passing to
the limit a homomorphism $\alpha:M_0\to \HH^0(M)$.

Let us introduce two more notations:

If $(P_0, \ldots, P_h)$ are elements of $S$, we denote by $(P_0, \ldots, P_h)M$
the submodule of $M$ consisting of the elements $\sum_{i=0}^{i=h} P_i\cdot m_i$
with $m_i\in M$; if the $P_i$ are homogeneous, this submodule is homogeneous.

If $P$ is an element of $S$ and $N$ a submodule of $M$, we denote by $N:P$
the submodule of $M$ consisting of the elements $m\in M$ such that $P\cdot m
\in N$; we clearly have $N:P\supset N$; if $N$ and $P$ are homogeneous,
so is $N:P$.

Having specified these notations, we have:

% page 255

{\bf Proposition 1.} \emph{Let $M$ be a graded $S$--module and $k$ an integer
$\geq 0$. Assume that for all $i\in I$ we have:
\[ (t_0^k, \ldots, t_{i-1}^k)M:t_i^k = (t_0^k, \ldots, t_{i-1}^k)M.\]
Then:\\
\wciecie (a) $\alpha^k: M_0\to H_k^0(M)$ is bijective (if $r\geq 1$),\\
\wciecie (b) $H_k^q(M) = 0$ for $0<q<r$.}

(For $i=0$, the assumption means that $t_0^k\cdot m = 0$ implies $m=0$.)

This Proposition is a special case of a result of de Rham \cite{11} (the de Rham's
result being also valid even if we do not assume that the $m\langle i_0
\ldots i_q\rangle$ are homogeneous). See also \cite{6}, Chap. VIII, \P 4 for
a particular case, sufficient for our purposes. 

We now apply Proposition 1 to the graded $S$--module $S(n)$:

{\bf Proposition 2.} \emph{Let $k$ be an integer $\geq 0$, $n$ an arbitrary
integer. Then:\\
\wciecie (a) $\alpha^k: S_n\to H_k^0(S(n))$ is bijective (if $r\geq 1$),\\
\wciecie (b) $H_k^q(S(n)) = 0$ for $0<q<r$,\\
\wciecie (c) $H_k^r(S(n))$ admits a base (over $K$) consisting of the cohomology
classes of the monomials $t_0^{\alpha_0}\ldots t_r^{\alpha_r}$ with
$0\leq \alpha_i < k$ and $\sum_{i=0}^{i=r} \alpha_i = k(r+1) + n$.}

It is clear that the $S$--module $S(n)$ satisfies the assumptions of Proposition 1,
which shows (a) and (b). On the other hand, for every graded $S$--module $M$, we
have $H_k^r(M) = M_{k(r+1)}/(t_0^k, \ldots, t_r^k)M_{kr}$; now the monomials
\[ t_0^{\alpha_0}\ldots t_r^{\alpha_r}, \alpha_i\geq 0,
	\sum_{i=0}^{i=r} \alpha_i = k(r+1) + n, \]
form a basis of $S(n)_{k(r+1})$ and those for which at least $\alpha_i$ is
$\geq k$ form a basis of $(t_0^k, \ldots, t_r^k)S(n)_{kr}$; hence (c).

It is convenient to write the exponents $\alpha_i$ in the form 
$\alpha_i=k-\beta_i$. The conditions of (c) are then written:
\[ 0<\beta_i\leq k\quad\text{and}\quad \sum_{i=0}^{i=r} \beta_i = -n. \]

The second condition, together with $\beta_i>0$, implies $\beta_i\leq -n-r$;
if thus $k\geq -n-r$, the condition $\beta_i\leq k$ is a consequence of the
preceding two. Hence:

{\bf Corollary 1.} \emph{For $k\geq -n-r$, $H_k^r(S(n))$ admits a basis
formed of the cohomology classes of monomials $(t_0\ldots t_r)^k/t_0^{\beta_0}
\ldots t_r^{\beta_r}$ with $\beta_i>0$ and $\sum_{i=0}^{i=r} \beta_i=-n$.}

We also have:

{\bf Corollary 2.} \emph{If $h\geq k\geq -n-r$, the homomorphism
\[ \rho_k^h: H_k^q(S(n))\to H_k^q(S(n)) \]
is bijective for all $q\geq 0$.}

For $q\neq r$, this follows from the assertions (a) and (b) of Proposition 2.
For $q=r$, this follows from Corollary 1, given that $\rho_k^h$ transforms
\[ (t_0\ldots t_r)^k/t_0^{\beta_0} \ldots t_r^{\beta_r} \quad
	\text{into}\quad (t_0\ldots t_r)^h/t_0^{\beta_0} \ldots t_r^{\beta_r}.\]

{\bf Corollary 3.} \emph{The homomorphism $\alpha:S_n\to H^0(S(n))$ is bijective
if $r\geq 1$ or if $n\geq 0$. We have $\Hq(S(n)) = 0$ for $0<q<r$ and 
$\HH^r(S(n))$ is a vector space of dimension $\binom{-n-1}{r}$ over $K$.}

The assertion pertaining to $\alpha$ follows from Proposition 2, (a), in the
case when $r\geq 1$; it is clear if $r=0$ and $n\geq 0$. The rest of the
Corollary is an obvious consequence of Corollaries 1 and 2 (seeing that
the binomial coefficient $\binom{a}{r}$ is zero for $a<r$).

\ssubsection{General properties of $\Hq(M)$}
\label{section-\arabic{subsection}}

{\bf Proposition 3.} \emph{Let $M$ be a graded $S$--module satisfying the
condition (TF). Then: \\
\wciecie (a) There exists an integer $k(M)$ such that $\rho_k^h: \Hq_k(M)\to\Hq_h(M)$
is bijective for $h\geq k\geq k(M)$ and every $q$.\\
\wciecie (b) $\Hq(M)$ is a vector space of finite dimension over $K$ for all
$q\geq 0$.\\
\wciecie (c) There exists an integer $n(M)$ such that for $n\geq n(M)$, 
$\alpha:M_n\to\HH^0(M(n))$ is bijective and that $\Hq(M(n))$ is zero for
all $q>0$.}

This is immediately reduced to the case when $M$ is of finite type. We
say that $M$ is of \emph{dimension} $\leq s$ ($s$ being an integer $\geq 0$)
if there exists an exact sequence:
\[ 0\to L^s \to L^{s-1}\to\ldots\to L^0\to M\to 0, \]
where $L^i$ are free graded $S$--modules of finite type. By the Hilbert 
syzygy theorem (cf. \cite{6}, Chap. VIII, th. 6.5), this dimension is always
$\leq r+1$.

We prove the Proposition by induction on the dimension of $M$. If it is 0,
$M$ is free of finite type, i.e. a direct sum of modules $S(n_i)$ and
the Proposition follows from Corollaries 2 and 3 and Proposition 2.
Assume that $M$ is of dimension $\leq s$ and let $N$ be the kernel of
$L^0\to M$. The graded $S$--module $N$ is of dimension $\leq s-1$
and we have an exact sequence:
\[ 0\to N\to L^0\to M\to 0. \]

By the induction assumption, the Proposition is true for $N$ and $L^0$.
Applying the five lemma (\cite{7}, Chap. I, Lemme 4.3) to the commutative
diagram:
\[
\begin{tikzpicture}[description/.style={fill=white,inner sep=2pt}]
    \matrix (m) [matrix of math nodes, row sep=3em,
    column sep=2.5em, text height=1.5ex, text depth=0.25ex]
    {   H_k^q(N) & H_k^q(L^0) & H_k^q(M) & H_k^{q+1}(N) & H_k^{q+1}(L^0) \\
        H_h^q(N) & H_h^q(L^0) & H_h^q(M) & H_h^{q+1}(N) & H_h^{q+1}(L^0), \\ };
    \path[->,font=\scriptsize]
    (m-1-1) edge node[auto] {$ $} (m-1-2)
    (m-1-2) edge node[auto] {$ $} (m-1-3)
    (m-1-3) edge node[auto] {$ $} (m-1-4)
    (m-1-4) edge node[auto] {$ $} (m-1-5);
    \path[->,font=\scriptsize]
    (m-2-1) edge node[auto] {$ $} (m-2-2)
    (m-2-2) edge node[auto] {$ $} (m-2-3)
    (m-2-3) edge node[auto] {$ $} (m-2-4)
    (m-2-4) edge node[auto] {$ $} (m-2-5);
    \path[->,font=\scriptsize]
    (m-1-1) edge node[auto] {$ $} (m-2-1);
    \path[->,font=\scriptsize]
    (m-1-2) edge node[auto] {$ $} (m-2-2);
    \path[->,font=\scriptsize]
    (m-1-3) edge node[auto] {$ $} (m-2-3);
    \path[->,font=\scriptsize]
    (m-1-4) edge node[auto] {$ $} (m-2-4);
    \path[->,font=\scriptsize]
    (m-1-5) edge node[auto] {$ $} (m-2-5);
\end{tikzpicture}
\]
where $h\geq k\geq \Sup(k(N), k(L^0)$, we show (a), thus also (b), because
the $\Hq_k(M)$ are of finite dimension over $K$. On the other hand, the
exact sequence
\[ \Hq(L^0(n)) \to \Hq(M(n)) \to \HH^{q+1}(N(n)) \]
shows that $\Hq(M(n)) = 0$ for $n\geq \Sup(n(L^0), n(N))$. Finally, consider
the commutative diagram:
\[
\begin{tikzpicture}[description/.style={fill=white,inner sep=2pt}]
    \matrix (m) [matrix of math nodes, row sep=3em,
    column sep=2.5em, text height=1.5ex, text depth=0.25ex]
    {   0 & N_n & L_n & M_n & 0 \\
        0 & H^0(N(n)) & H^0(L^0(n)) & H^0(M(n)) & H^1(N(n)); \\ };
    \path[->,font=\scriptsize]
    (m-1-1) edge node[auto] {$ $} (m-1-2)
    (m-1-2) edge node[auto] {$ $} (m-1-3)
    (m-1-3) edge node[auto] {$ $} (m-1-4)
    (m-1-4) edge node[auto] {$ $} (m-1-5);
    \path[->,font=\scriptsize]
    (m-2-1) edge node[auto] {$ $} (m-2-2)
    (m-2-2) edge node[auto] {$ $} (m-2-3)
    (m-2-3) edge node[auto] {$ $} (m-2-4)
    (m-2-4) edge node[auto] {$ $} (m-2-5);
    \path[->,font=\scriptsize]
    (m-1-1) edge node[auto] {$ $} (m-2-1);
    \path[->,font=\scriptsize]
    (m-1-2) edge node[auto] {$ \alpha $} (m-2-2);
    \path[->,font=\scriptsize]
    (m-1-3) edge node[auto] {$ \alpha $} (m-2-3);
    \path[->,font=\scriptsize]
    (m-1-4) edge node[auto] {$ \alpha $} (m-2-4);
    \path[->,font=\scriptsize]
    (m-1-5) edge node[auto] {$ $} (m-2-5);
\end{tikzpicture}
\]
% 
% page 257
%
for $n\geq n(N)$, we have $\HH^1(N(n)) = 0$; we deduce that $\alpha:M_n
\to \HH^0(M(n))$ is bijective for $n\geq \Sup(n(L^0), n(N))$, which completes
the proof of the Proposition.

\ssubsection{Comparison of the groups $\Hq(M)$ and $\Hq(X, \Aa(M))$}
\label{section-\arabic{subsection}}

Let $M$ be a graded $S$--module and let $\Aa(M)$ be the algebraic sheaf
on $X=\Pp_r(K)$ defined by $M$ by the procedure of \no \ref{section-57}. We will now
compare $C(M)$ with $C'(\Uu, \Aa(M))$, the complex of alternating
cochains of the covering $\Uu=\{U_i\}_{i\in I}$ with values in the
sheaf $\Aa(M)$.

Let $m\in C_k^q(M)$ and let $(i_0,\ldots, i_q)$ be a sequence of $q+1$
elements of $I$. The polynomial $(t_{i_0}\ldots t_{i_q})^k$ belongs
obviously to $S(U_{i_0\ldots i_q})$, with the notations of \no \ref{section-57}.
It follows that $m\langle i_0\ldots i_q\rangle/(t_{i_0}\ldots t_{i_q})^k$
belongs to $M_U$, where $U = U_{i_0\ldots i_q}$, thus defines a section
of $\Aa(M)$ over $U_{i_0\ldots i_q}$. When $(i_0, \ldots, i_q)$ varies,
the system consisting of this sections is an alternating cochain of $\Uu$
with values in $\Aa(M)$, which we denote by $\iota_k(m)$. We immediately
see that $\iota_k$ commutes with $d$ and that $\iota_k=\iota_h\circ\rho_k^h$
if $h\geq k$. By passing to the inductive limit, the $\iota_k$ thus define
a homomorphism $\iota: C(M)\to C'(\Uu, \Aa(M))$, commuting with $d$.

{\bf Proposition 4.} \emph{If $M$ satisfies the condition (TF), $\iota:
C(M)\to C'(\Uu, \Aa(M))$ is bijective.}

If $M\in\Ca$, we have $M_n=0$ for $n\geq n_0$, so $C_k(M) = 0$ for $k\geq n_0$
and $C(M) = 0$. As every $S$--module satisfying (TF) is $\Ca$-isomorphic to
a module of finite type, this shows that we can restrict ourselves to the
case when $M$ is of finite type. We can then find an exact sequence
$L^1\to L^0\to M\to 0$, where $L^1$ and $L^0$ are free of finite type.
By Propositions 3 and 5 from \no \ref{section-58}, the sequence
\[ \Aa(L^1) \to \Aa(L^0) \to \Aa(M) \to 0 \]
is an exact sequence of coherent algebraic sheaves; as the $U_{i_0\ldots i_q}$
are affine open subsets, the sequence
\[ C'(\Uu, \Aa(L^1))\to C'(\Uu, \Aa(L^0)\to C'(\Uu, \Aa(M))\to 0\]
is exact (cf. \no \ref{section-45}, Corollary 2 to Theorem 2). The commutative diagram
\[
\begin{tikzpicture}[description/.style={fill=white,inner sep=2pt}]
    \matrix (m) [matrix of math nodes, row sep=3em,
    column sep=2.5em, text height=1.5ex, text depth=0.25ex]
    {   C(L^1) & C(L^0) & C(M) &  0 \\
        C'(\Uu, \Aa(L^1)) & C'(\Uu, \Aa(L^0)) & C'(\Uu, \Aa(M)) &  0 \\ };
    \path[->,font=\scriptsize]
    (m-1-1) edge node[auto] {$ $} (m-1-2)
    (m-1-2) edge node[auto] {$ $} (m-1-3)
    (m-1-3) edge node[auto] {$ $} (m-1-4);
    \path[->,font=\scriptsize]
    (m-2-1) edge node[auto] {$ $} (m-2-2)
    (m-2-2) edge node[auto] {$ $} (m-2-3)
    (m-2-3) edge node[auto] {$ $} (m-2-4);
    \path[->,font=\scriptsize]
    (m-1-1) edge node[auto] {$ \iota $} (m-2-1);
    \path[->,font=\scriptsize]
    (m-1-2) edge node[auto] {$ \iota $} (m-2-2);
    \path[->,font=\scriptsize]
    (m-1-3) edge node[auto] {$ \iota $} (m-2-3);
    \path[->,font=\scriptsize]
    (m-1-4) edge node[auto] {$ $} (m-2-4);
\end{tikzpicture}
\]
then shows that if the Proposition is true for the module $L^1$ and $L^0$,
so it is for $M$. We are thus reduced to the special case of a free module
of finite type, then, by the decomposition into direct summands, to the case
when $M = S(n)$.

In this case, we have $\Aa(S(n)) = \Oo(n)$; a section $f_{i_0\ldots i_q}$
of $\Oo(n)$ over $U_{i_0\ldots i_q}$ is, by the sole definition of this sheaf,
a regular function on $V_{i_0}\cap\ldots\cap V_{i_q}$ and homogeneous of
degree $n$. As $V_{i_0}\cap\ldots\cap V_{i_q}$ as the set of points
%
% page 258
%
of $K^{r+1}$ where the function $t_{i_0}\ldots t_{i_q}$ is $\neq 0$, there
exists an integer $k$ such that
\[ f_{i_0\ldots i_q} = P\langle i_0\ldots i_q\rangle/(t_{i_0}\ldots t_{i_q})^k,\]
$P\langle i_0\ldots i_q\rangle$ being a homogeneous polynomial of degree
$n+k(q+1)$, that is, of degree $k(q+1)$ in $S(n)$. Thus, every alternating
cochain $f\in C'(\Uu, \Oo(n))$ defines a system $P\langle i_0\ldots i_q\rangle$
that is an element of $C_k(S(n))$; hence a homomorphism
\[ \nu: C'(\Uu, \Oo(n)) \to C(S(n)). \]

As we verify immediately that $\iota\circ\nu = 1$ and $\nu\circ\iota = 1$,
it follows that $\iota$ is bijective, which completes the proof.

{\bf Corollary.} \emph{$\iota$ defines an isomorphism of $\Hq(M)$ with
$\Hq(X, \Aa(M))$ for all $q\geq 0$.}

Indeed, we know that $\HH'^q(\Uu, \Aa(M)) = \Hq(\Uu, \Aa(M))$ (\no \ref{section-20},
Proposition 2) and that $\Hq(\Uu, \Aa(M)) = \Hq(X, \Aa(M))$ (\no \ref{section-52}, 
Proposition 2, which applies because $\Aa(M)$ is coherent).

{\bf Remark.} It is easy to see that $\iota:C(M)\to C'(\Uu, \Aa(M))$ is
\emph{injective} even if $M$ does not satisfy the condition (TF).

\ssubsection{Applications}
\label{section-\arabic{subsection}}

{\bf Proposition 5.} \emph{If $M$ is a graded $S$--module satisfying the
condition (TF), the homomorphism $\alpha:M\to \Gamma(\Aa(M))$, defined
in \no \ref{section-59}, is $\Ca$-bijective.}

We must observe that $\alpha:M_n\to \Gamma(X, \Aa(M(n)))$ is bijective
for $n$ sufficiently large. Then, by Proposition 4, $\Gamma(X, \Aa(M(n)))$
is identified with $\HH^0(M(n))$; the Proposition follows thus from
Proposition 3, (c), given the fact that the homomorphism $\alpha$ is
transformed by the above identification to a homomorphism defined
at the beginning of \no \ref{section-62}, also denoted by $\alpha$.

{\bf Proposition 6.} \emph{Let $\Ff$ be a coherent algebraic sheaf on $X$.
The graded $S$--module $\Gamma(\Ff)$ satisfies the condition (TF) and
the homomorphism $\beta:\Aa(\Gamma(\Ff))\to\Ff$ defined in \no \ref{section-59} is
bijective.}

By Theorem 2 of \no \ref{section-60}, we can assume that $\Ff=\Aa(M)$, where $M$ is
a module satisfying (TF). By the above Proposition, $\alpha:M\to\Gamma(\Aa(M))$
is $\Ca$-bijective; as $M$ satisfies (TF), it follows that $\Gamma(\Aa(M))$
satisfies it also. Applying Proposition 6 from \no \ref{section-58}, we see that 
$\alpha:\Aa(M)\to\Aa(\Gamma(\Aa(M)))$ is bijective. Since the composition
$\Aa(M)\xto{\alpha}\Aa(\Gamma(\Aa(M)))\xto{\beta}\Aa(M)$ is the identity
(\no \ref{section-59}, Proposition 7), it follows that $\beta$ is bijective, q.e.d.

{\bf Proposition 7.} \emph{Let $\Ff$ be a coherent algebraic sheaf on $X$.
The groups $\Hq(X, \Ff)$ are vector spaces of finite dimension over $K$
for all $q\geq 0$ and we have $\Hq(X, \Ff(n)) =0$ for $q>0$ and $n$ 
sufficiently large.}

We can assume, as above, that $\Ff=\Aa(M)$ where $M$ is a module satisfying
(TF). The Proposition then follows from Proposition 3 and the corollary
to Proposition 4.

{\bf Proposition 8.} \emph{We have $\Hq(X, \Oo(n)) = 0$ for $0<q<r$ and
$\HH^r(X, \Oo(n))$ is a vector space of dimension $\binom{-n-1}{r}$ over $K$,
admitting a base consisting of the cohomology classes
%
% page 259
%
of the alternating cocycles of $\Uu$
\[ f_{01\ldots r} = 1/t_0^{\beta_0}\ldots t_r^{\beta_r}\quad
	\text{with}\quad \beta_i>0 \quad\text{and}\quad 
	\sum_{i=0}^{i=r} \beta_i = -n. \]
}

We have $\Oo(n) = \Aa(S(n))$, hence $\Hq(X, \Oo(n)) = \Hq(S(n))$, by the
corollary to Proposition 4; the Proposition follows immediately from this
and from the corollaries of Proposition 2.

We note that in particular $\HH^r(X, \Oo(-r-1))$ is a vector space
of dimension 1 over $K$, with a base consisting of the cohomology class
of the cocycle $f_{01\ldots r} = 1/t_0\ldots t_r$.

\ssubsection{Coherent algebraic sheaves on projective varieties}
\label{section-\arabic{subsection}}

Let $V$ be a closed subvariety of the projective space $X = \Pp_r(K)$
and let $\Ff$ be a coherent algebraic sheaf on $V$. By extending $\Ff$
by 0 outside $V$, we obtain a coherent algebraic sheaf on $X$ (cf. \no \ref{section-39})
denoted $\Ff^X$; we know that $\Hq(X, \Ff^X) = \Hq(V, \Ff)$. The results
of the preceding \no thus apply to the groups $\Hq(V, \Ff)$. We obtain
immediately (given \no \ref{section-52}):

{\bf Theorem 1.} \emph{The groups $\Hq(V, \Ff)$ are vector spaces of finite
dimension over $K$, zero for $q>\dim V$.}

In particular, for $q=0$ we have:

{\bf Corollary.} \emph{$\Gamma(V, \Ff)$ is a vector space of finite dimension
over $K$.}

(It is natural to conjecture whether the above theorem holds for all 
\emph{complete} varieties, in the sense of Weil \cite{16}.)

Let $U'_i = U_i\cap V$; the $U'_i$ form an open covering $\Uu'$ of $V$.
If $\Ff$ is an algebraic sheaf on $V$, let $\Ff_i = \Ff(U'_i)$ and let
$\theta_{ij}(n)$ be the isomorphism of $\Ff_j(U'_i\cap U'_j)$ to
$\Ff_i(U'_i\cap U'_j)$ defined by multiplication by $(t_j/t_i)^n$. We
denote by $\Ff(n)$ the sheaf obtained by gluing the $\Ff_i$ with respect
to $\theta_{ij}(n)$. The operation $\Ff(n)$ has the same properties
as the operation defined in \no \ref{section-54} and generalizes it; in particular,
$\Ff(n)$ is canonically isomorphic to $\Ff\otimes \Oo_V(n)$.

We have $\Ff^X(n) = \Ff(n)^X$. Applying then Theorem 1 of \no \ref{section-55}, together
with Proposition 7 from \no \ref{section-65}, we obtain:

{\bf Theorem 2.} \emph{Let $\Ff$ be a coherent algebraic sheaf on $V$. There
exists an integer $m(\Ff)$ such that we have, for all $n\geq m(\Ff)$:\\
\wciecie (a) For all $x\in V$, the $\Oo_{x, V}$--module $\Ff(n)_x$ is
generated by the elements of $\Gamma(V, \Ff(n))$, \\
\wciecie (b) $\Hq(V, \Ff(n)) = 0$ for all $q>0$.}

{\bf Remark.} It is essential to observe that the sheaf $\Ff(n)$ \emph{does not 
depend solely on $\Ff$ and $n$}, but also on the \emph{embedding} of $V$ into
the projective space $X$. More precisely, let $P$ be the principal bundle
$\pi^{-1}(V)$ with the structural group $K^*$; with $n$ an integer, we make
$K^*$ act on $K$ by the formula:
\[ (\lambda, \mu) \mapsto \lambda^{-n}\mu \quad\text{if}\quad 
	\lambda\in K^*\quad\text{and}\quad \mu\in K.\]
%
% page 260
%
Let $E^n = P\times_{K^*} K$ be the fiber space associated to $P$ and the fiber
$K$, equipped with the above action; let $\Ss(E^n)$ be the sheaf of germs of
sections of $E^n$ (cf. \no \ref{section-41}). Taking into account the fact that $t_i/t_j$
form a system of transition maps of $P$, we verify immediately that $\Ss(E^n)$
is canonically isomorphic to $\Oo_V(n))$. The formula $\Ff(n)=\Ff\otimes\Oo_V(n)
= \Ff\otimes\Ss(E^n)$ shows then that the operation $\Ff\to\Ff(n)$ depends
only on the \emph{class of the principal bundle $P$ defined by the embedding
$V\to X$.} In particular, if $V$ is normal, $\Ff(n)$ depends only on the
class of linear equivalence of hyperplane sections of $V$ in the considered
embedding (cf. \cite{17}).

\ssubsection{A supplement}
\label{section-\arabic{subsection}}

If $M$ is a graded $S$--module satisfying (TF), we denote by $M^\natural$
the graded $S$--module $\Gamma(\Aa(M))$. We have seen in \no \ref{section-65} that
$\alpha:M\to M^\natural$ is $\Ca$-bijective. We shall now give conditions
for $\alpha$ to be bijective.

{\bf Proposition 9.} \emph{$\alpha:M\to M^\natural$ is bijective if and only
if the following conditions are satisfied:\\
\wciecie (i) If $m\in M$ is such that $t_i\cdot m = 0$ for all $i\in I$, then
	$m = 0$,\\
\wciecie (ii) If elements $m_i\in M$, homogeneous of the same degree, satisfy
	$t_j\cdot m_i = t_i\cdot m_j = 0$ for every couple $(i, j)$, there
	exists an $m\in M$ such that $m_i = t_i\cdot m$.}

Let us show that the conditions (i) and (ii) are satisfied by $M^\natural$,
which will prove the necessity. For (i), we can assume that $m$ is
homogeneous, that is, it is a section of $\Aa(M(n))$; in this case,
the condition $t_i\cdot m = 0$ implies that $m$ is zero on $U_i$, and
since this occurs for all $i\in I$, we have $m = 0$. For (ii), let $n$
be the degree of $m_i$; we thus have $m_i\in\Gamma(\Aa(M(n)))$; as $1/t_i$
is a section of $\Oo(-1)$ over $U_i$, $m_i/t_i$ is a section of $\Aa(M(n-1))$
over $U_i$ and the condition $t_j\cdot m_i - t_i\cdot m_j$ shows that these
various sections are the restrictions of a unique section $m$ of $\Aa(M(n-1))$
over $X$; it remains to compare the sections $t_i\cdot m$ and $m_i$; to show
that they coincide on $U_j$, it suffices to observe that $t_j(t_i\cdot m - m_i)
= 0$ on $U_j$, which follows from the formula $t_j \cdot m_i = t_i\cdot m_j$
and the definition of $m$.

We will now show that (i) implies that $\alpha$ is injective. For $n$
sufficiently large, we know that $\alpha: M_n\to M_n^\natural$ is bijective
and we can thus proceed by descending induction on $n$. If $\alpha(m) = 0$
with $m\in M_n$, we have $t_i\alpha(m) = \alpha(t_i\cdot m) = 0$ and 
the induction assumption, applicable since $t_i\cdot m\in M_{n+1}$,
shows that $m = 0$. Finally, let us show that (i) and (ii) imply that
$\alpha$ is surjective. We can, as before, proceed by descending induction
on $n$. If $m'\in M_n^\natural$,
the induction assumption shows that there exist $m_i\in M_{n+1}$ such that
$\alpha(m_i) = t_i\cdot m'$; we have $\alpha(t_j\cdot m_i - t_i\cdot m_j) = 0$,
hence $t_j\cdot m_i- t_i\cdot m_j = 0$, because $\alpha$ is injective.
The condition (ii) then implies that there exists an $m\in M_n$ such that 
$t_i\cdot m = m_i$; we have $t_i(m' - \alpha(m)) = 0$, which shows that
$m' = \alpha(m)$ and completes the proof.

{\bf Remarks.} (1) The proof shows that the condition (i) is sufficient
and necessary for $\alpha$ to be injective.\\
\wciecie (2) We can express (i) and (ii) as: the homomorphism $\alpha^1: M_n
\to \HH^0_q(M(n))$ is bijective for all $n\in\Zz$. Besides, Proposition 4
shows that we can identify $M^\natural$ with the $S$--module
$\bigoplus_{n\in\Zz} \HH^0(M(n))$ and it would be easy to provide a purely
algebraic proof of Proposition 9 (without using the sheaf $\Aa(M)$).










