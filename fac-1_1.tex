\ssection{Operations on sheaves}

\ssubsection{Definition of a sheaf} 
\label{section-\arabic{subsection}}

Let $X$ be a topological space. A \emph{sheaf of abelian groups} on $X$ 
(or simply a \emph{sheaf}) consists of:

{\bf (a)} \emph{A function $x\to \Ff_x$, giving for all $x\in X$ an abelian group $\Ff_x$},

{\bf (b)} \emph{A topology on the set $\Ff$, the sum of the sets $\Ff_x$.}

If $f$ is an element of $\Ff_x$, we put $\pi(f)=x$; we call the mapping
of $\pi$ the \emph{projection} of $\Ff$ onto $X$; the family in $\Ff\times\Ff$ 
consisting of pairs $(f, g)$ such that $\pi(f)=\pi(g)$ is denoted by $\Ff+\Ff$.

Having stated the above definitions, we impose two axioms on the data (a)
and (b): 

{\bf (I)} \emph{For all $f \in \Ff$ there exist open neighborhoods $V$ of $f$ and
$U$ of $\pi(f)$ such that the restriction of $\pi$ to $V$ is a homeomorphism
of $V$ and $U$.} 

(In other words, $\pi$ is a local homeomorphism).

{\bf (II)} \emph{The mapping $f\mapsto -f$ is a continuous mapping from $\Ff$ to $\Ff$,
and the mapping $(f, g) \mapsto f+g$ is a continuous mapping from $\Ff+\Ff$ to $\Ff$.}

We shall see that, even when $X$ is separated (which we do not assume), $\Ff$ is
not necessarily separated, which is shown by the example of the sheaf of germs
of functions (cf. \no \ref{section-3}).

{\bf Example} of a sheaf. For $G$ an abelian group, set $\Ff_x=G$ for all $x\in X$;
the set $\Ff$ can be identified with the product $X \times G$ and, if it is equipped
with the product topology of the topology of $X$ by the discrete topology on $G$,
one obtains a sheaf, called the \emph{constant sheaf} isomorphic with $G$, often
identified with $G$.

\ssubsection{Sections of a sheaf}
\label{section-\arabic{subsection}}

Let $\Ff$ be a sheaf on a space $X$, and let $U$ be 
a subset of $X$. By a \emph{section} of $\Ff$ over $U$ we mean a continuous
mapping $s: U\to\Ff$ for which $\pi\circ s$ coincides with the identity on $U$.
We therefore have $s(x)\in \Ff_x$ for all $x\in U$. The set of sections of $\Ff$
over $U$ is denoted by $\Gamma(U, \Ff)$; axiom (II) implies that $\Gamma(U, \Ff)$
is an abelian group. If $U\subset V$, and if $s$ is a section over $V$, the restriction
of $s$ to $U$ is a section over $U$; hence we have a homomorphism $\rho_U^V: \Gamma(V, \Ff)
\to \Gamma(U, \Ff)$.

If $U$ is open in $X$, $s(U)$ is open in $\Ff$, and if $U$ runs over a base of
the topology of $X$, then $s(U)$ runs over a 
%
% page 200
%
base of the topology of $\Ff$; this is
only another wording of axiom (I).

Note also one more consequence of axiom (I): for all $f \in \Ff_x$, there exists
a section $s$ over an open neighborhood of $x$ for which $s(x)=f$, and two sections
with this property coincide on an open neighborhood of $x$. In other words, $\Ff_x$
is an \emph{inductive limit} of $\Gamma(U, \Ff)$ for $U$ running over the filtering
order of all open neighborhoods
of $x$. 

\ssubsection{Construction of sheaves}
\label{section-\arabic{subsection}}

Given for all open $U\subset X$ an abelian group $\Ff_U$ and for all pairs of
open $U\subset V$ a homomorphism $\phi_U^V: \Ff_V\to\Ff_U$, satisfying the
transitivity condition $\phi_U^V\circ\phi_V^W = \phi_U^W$ whenever $U\subset V\subset W$.

The collection $(\Ff_U, \phi_U^V)$ allows us to define a sheaf $\Ff$ in the following
way:

{\bf (a)} Put $\Ff_x = \lim \Ff_U$ (inductive limit of the system of open 
neighborhoods of $x$). If $x$ belongs to an open subset $U$, we have
a canonical morphism $\phi_x^U: \Ff_U \to \Ff _x$.

{\bf (b)} Let $t\in \Ff_U$ and denote by $[t, U]$ the set of $\phi_x^U(t)$ for $x$
running over $U$ ; we have $[t, U]\subset \Ff$ and we give $\Ff$ the topology generated
by $[t, U]$. Moreover, an element $f\in \Ff_x$ has a base of neighborhoods consisting
of the sets $[t, U]$ for $x\in U$ and $\phi_x^U(t) = f$.

One verifies immediately that the data (a) and (b) satisfy the axioms
(I) and (II), in other words, that $\Ff$ is a sheaf. We say that this is
the sheaf \emph{defined by the system} $(\Ff_U, \phi_U^V)$.

If $f \in \Ff_U$, the mapping $x\mapsto \phi_x^U(t)$ is a section of $\Ff$ over $U$ ;
hence we have a canonical morphism $\iota : \Ff_U\to \Gamma(U, \Ff)$.

{\bf Proposition 1.} \emph{$\iota: \Ff_U\to\Gamma(U, \Ff)$ is injective
\footnote{Recall (cf. \cite{1}) that a function $f:E\to E'$ is \emph{injective} if
$f(e_1)=f(e_2)$ implies $e_1=e_2$, \emph{surjective} if $f(E)=E'$, \emph{bijective}
when it is both injective and surjective. An injective (resp. surjective, bijective) mapping
is called an \emph{injection} (resp. a \emph{surjection}, a \emph{bijection}).} 
if and only if the following condition holds: }\\
\wciecie \emph{If an element $t\in\Ff_U$ is such that there exists an open covering $\{U_i\}$ of $U$
with $\phi_{U_i}^U(t) = 0$ for all $i$, then $t=0$. }

If $t\in \Ff_U$ satisfies the condition above, we have
\[ \phi_x^U(t) = \phi_x^{U_i} \circ \psi_{U_i}^U (t) = 0 \quad \mathrm{if}\, x\in U_i, \]
which means that $\iota(t)=0$. Conversely, suppose that $\iota(t)=0$ with $t\in \Ff_U$ ;
since $\phi_x^U(t) = 0$ for $x\in U$, there exists an open neighborhood $U(x)$ of $x$
such that $\phi_{U(x)}^U(t) = 0$, by the definition of an inductive limit. The sets
$U(x)$ form therefore an open covering of $U$ satisfying the condition stated above.

{\bf Proposition 2.} \emph{Let $U$ be an open subset of $X$, and let $\iota: \Ff_V
\to \Gamma(V, \Ff)$ be injective for all open $V\subset U$. Then $\iota: \Ff_U
\to \Gamma(U, \Ff)$ is surjective\footnotemark[1] (and therefore bijective) if and only
if the following condition is satisfied: }\\
\wciecie \emph{For all open coverings $\{U_i\}$ of $U$, and all systems $\{ t_i \}$, $t_i \in \Ff_{U_i}$
%
% page 201
%
such that $\phi_{U_i\cap U_j}^{U_i}(t_i) = \phi_{U_i\cap U_j}^{U_j}(t_j)$ for all pairs
$(i, j)$, there exists a $t \in \Ff_U$ with $\phi_{U_i}^U(t)=t_i$ for all $i$. }

The condition is necessary: every $t_i$ defines a section $s_i = \iota(t_i)$ over
$U_i$, and we have $s_i = s_j$ over $U_i\cap U_j$; so there exists a section $s$
over $U$ which coincides with $s_i$ over $U_i$ for all $i$; if $\iota: \Ff_U\to
\Gamma(U, \Ff)$ is surjective, there exists $t\in \Ff_U$ such that $\iota(t)=s$.
If we put $t_i' = \phi^{U}_{U_i}(t)$, the section defined by $t_i'$ over
$U_i$ does not differ from $s_i$; since $\iota(t_i - t_i')=0$, which implies
$t_i = t_i'$ for $\iota$ was supposed injective.

The condition is sufficient: if $s$ is a section of $\Ff$ over $U$, there exists an
open covering $\{U_i\}$ of $U$ and elements $t_i\in \Ff_{U_i}$ such that
$\iota(t_i)$ coincides with the restriction of $s$ to $U_i$; it follows that
the elements $\phi_{U_i\cap U_j}^{U_i}(t_i)$ and $\phi_{U_i\cap U_j}^{U_j}(t_j)$ 
define the same section over $U_i\cap U_j$, so, by the assumption made on $\iota$,
they are equal. If $t\in \Ff_U$ satisfies $\phi^{U}_{U_i}(t) = t_i$, $\iota(t)$
coincides with $s$ over each $U_i$, so also over $S$, q.e.d.

{\bf Proposition 3.} \emph{If $\Ff$ is a sheaf of abelian groups on $X$, the sheaf
defined by the system $(\Gamma(U, \Ff), \rho_U^V)$ is canonically isomorphic with $\Ff$. }

This is an immediate result of properties of sections stated in \no \ref{section-2}.

Proposition 3 shows that every sheaf can be defined by an appropriate system
$(\Ff_U, \phi_U^V)$. We will see that different systems can define the same sheaf
$\Ff$; however, if we impose on $(\Ff_U, \phi_U^V)$ the conditions of Propositions
1 and 2, we shall have only one (up to isomorphism) possible system: the one given
by $(\Gamma(U, \Ff), \rho_U^V)$.

{\bf Example.} Let $G$ be an abelian group and denote by $\Ff_U$ the set of functions
on $U$ with values in $G$; define $\phi_U^V:\Ff_V\to\Ff_U$ by \emph{restriction}
of such functions. We thus obtain a system $(\Ff_U, \phi_U^V)$, and hence a sheaf $\Ff$,
called the \emph{sheaf of germs of functions} with values in $G$. One checks immediately
that the system $(\Ff_U, \phi_U^V)$ satisfies the conditions of Propositions 1 and 2;
we thus can identify sections of $\Ff$ over an open $U$ with the elements of $\Ff_U$.

\ssubsection{Gluing sheaves}
\label{section-\arabic{subsection}}

Let $\Ff$ be a sheaf on $X$, and let $U$ be a subset of $X$; the set $\pi^{-1}(U)\subset\Ff$,
with the topology induced from $\Ff$, forms a sheaf over $U$, called a sheaf \emph{induced}
by $\Ff$ on $U$, end denoted by $\Ff(U)$ (or just $\Ff$, when it does not cause confusion).

We see that conversely, we can define a sheaf on $X$ by means of sheaves on open subsets
covering $X$:

{\bf Proposition 4.} \emph{Let $\Uu = \{U_i\}_{i\in I}$ be an open covering of $X$ and, for
all $i\in I$, let $\Ff_i$ be a sheaf over $U_i$; for all pairs $(i, j)$ let $\theta_{ij}$
be an isomorphism from $\Ff_j(U_i\cap U_j)$ to $\Ff_i(U_i \cap U_j)$; suppose that
we have $\theta_{ij}\circ\theta_{jk} = \theta_{ik}$ at each point of $U_i\cap U_j\cap U_k$
for all triples $(i, j, k)$.}

\emph{Then there exists a sheaf $\Ff$ and for all $i$ an isomorphism $\eta_i$ from $\Ff(U_i)$
to $\Ff_i$, such that $\theta_{ij} = \eta_i\circ \eta_j^{-1}$ at each point of
$U_i\cap U_j$. Moreover, $\Ff$ and $\eta_i$ are determined up to isomorphism by the
preceding conditions. }

The uniqueness of $\{\Ff, \eta_i\}$ is evident; for the proof of existence, we could define
%
% page 202
%
$\Ff$ as a quotient space of the sum of $\Ff_i$, but we will rather use the methods of \no \ref{section-3}:
if $U$ is an open subset of $X$, let $\Ff_U$ be the group whose elements are systems
$\{s_k\}_{k\in I}$ with $s_k \in \Gamma(U\cap U_k, \Ff_k)$ and $s_k = \theta_{kj}(s_j)$
on $U\cap U_j\cap U_k$ ; if $U\subset V$, we define $\phi_U^V$ in an obvious way. The
sheaf defined by the system $(\Ff_U, \phi_U^V)$ is the sheaf $\Ff$ we look for; moreover,
if $U\in U_i$, the mapping sending a system $\{s_k\}\in \Ff_U$ to the element
$s_i \in \Gamma(U_i, \Ff_i)$ is an isomorphism from $\Ff_U$ to $\Gamma(U, \Ff_i)$, because
of the transitivity condition; we so obtain an isomorphism $\eta_i: \Ff(U_i)\to \Ff_i$,
which obviously satisfies the stated condition.

We say that the sheaf $\Ff$ is obtained by \emph{glueing} the sheaves $\Ff_i$ by means
of the isomorphisms $\theta_{ij}$.

\ssubsection{Extension and restriction of a sheaf}
\label{section-\arabic{subsection}}

Let $X$ be a topological space, $Y$ its closed subspace and $\Ff$ a sheaf on $X$. 
We say that $\Ff$ is \emph{concentrated on} $Y$, or that it is \emph{zero outside of} $Y$
if we have $\Ff_x = 0$ for all $x \in X - Y$.

{\bf Proposition 5.} \emph{If a sheaf $\Ff$ is concentrated on $Y$, the homomorphism
\[ \rho_Y^X : \Gamma(X, \Ff) \to \Gamma(Y, \Ff(Y)) \]
is bijective.}

If a section of $\Ff$ over $X$ is zero over $Y$, it is zero everywhere since $\Ff_x = 0$
if $x \notin Y$, which shows that $\rho_Y^X$ is injective. Conversely, let $s$ be a section
of $\Ff(Y)$ over $Y$, and extend $s$ onto $X$ by putting $s(x)=0$ for $x\notin Y$ ; 
the mapping $x\mapsto s(x)$ is obviously continuous on $X-Y$ ; on the other hand, if
$x \in Y$, there exists a section $s'$ of $\Ff$ over an open neighborhood $U$ of $x$
for which $s'(x) = s(x)$; since $s$ is continuous on $Y$ by assumption, there exists
an open neighborhood $V$ of $x$, contained in $U$ and such that $s'(y)=s(y)$ for all
$y \in V\cap Y$; since $\Ff_y = 0$ if $y \notin Y$, we also have that $s'(y)=s(y)$
for $y \in V - (V\cap Y)$; hence $s$ and $s'$ coincide on $V$, which proves that $s$
is continuous in a neighborhood of $Y$, so it is continuous everywhere. This shows
that $\rho_Y^X$ is surjective, which ends the proof.

We shall now prove that the sheaf $\Ff(Y)$ determines the sheaf $\Ff$ uniquely:

{\bf Proposition 6.} \emph{Let $Y$ be a closed subspace of $X$, and let $\mathcal{G}$ be
a sheaf on $Y$. Put $\Ff_x = \mathcal{G}_x$ if $x\in Y$, $\Ff_x = 0$ if $x\notin Y$,
and let $\Ff$ be the sum of the sets $\Ff_x$. Then $\Ff$ admits a unique structure of a sheaf over $X$ such that $\Ff(Y) = \mathcal{G}$. }

Let $U$ be an open subset of $X$; if $s$ is a section of $\mathcal{G}$ on $U\cap Y$, 
extend $s$ by 0 on $U-(U\cap Y)$; when $s$ runs over $\Gamma(U\cap Y, \mathcal{G})$,
we obtain this way a group $\Ff_U$ of mappings from $U$ to $\Ff$. Proposition 5
then shows that if $\Ff$ is equipped a structure of a sheaf such that $\Ff(Y) = \mathcal{G}$,
we have $\Ff_U = \Gamma(U, \Ff)$, which proves the uniqueness of the structure
in question. The existence is proved using the methods of \no \ref{section-3} applied to $\Ff_U$
and the restriction homomorphisms $\phi_U^V: \Ff_U\to \Ff_V$. 

We say that a sheaf $\Ff$ is obtained by \emph{extension of the sheaf $\mathcal{G}$ by 0
outside $Y$}; we denote this sheaf by $\mathcal{G}^X$, or simply $\mathcal{G}$ if
it does not cause confusion.

\ssubsection{Sheaves of rings and sheaves of modules}
\label{section-\arabic{subsection}}

The notion of a sheaf defined in \no \ref{section-1} is that of a sheaf of \emph{abelian groups}.
It is clear that there exist analogous definitions for all algebraic structures
%
% page 203
%
(we could even define ``sheaves of rings", where $\Ff_x$ would not admit an algebraic
structure, and we only require axiom {\bf (I)}). From now on, we will encounter mainly
sheaves of \emph{rings} and sheaves of \emph{modules}:

A sheaf of rings $\Aa$ is a sheaf of abelian groups $\Aa_x$, $x\in X$, where each
$\Aa_x$ has a structure of a ring such that the mapping $(f, g)\mapsto f\cdot g$ is
a continuous mapping from $\Aa+\Aa$ to $\Aa$ (the notation being that of \no \ref{section-1}). 
We shall always assume that $\Aa_x$ has a unity element, varying continuously with $x$.

If $\Aa$ is a sheaf of rings satisfying the preceding condition, $\Gamma(U, \Aa)$ is
a ring with unity, and $\rho_U^V: \Gamma(V, \Aa) \to \Gamma(U, \Aa)$ is a homomorphism
of rings preserving unity if $U\subset V$. Conversely, given rings $\Aa_U$ with unity and 
homomorphisms $\phi_U^V : \Aa_V \to \Aa_U$ preserving unity and satisfying 
$\phi_U^V\circ \phi_V^W = \phi_U^W$, the sheaf $\Aa$ defined by the system 
$(\Aa_U, \phi_U^V)$ is a sheaf of rings. For example, if $G$ is a ring with unity,
the ring of germs of functions with values in $G$ (defined in \no \ref{section-3}) is a sheaf of rings.

Let $\Aa$ be a sheaf of rings. A sheaf $\Ff$ is called a \emph{sheaf of $\Aa$-modules}
if every $\Ff_x$ carries a structure of a left unitary\footnote{i.e. with the unity acting
as identity} $\Aa_x$-module, varying ``continuously" with $x$, in the following sense: if 
$\Aa + \Ff$ is the subspace
of $\Aa\times\Ff$ consisting of the pairs $(a, f)$ with $\pi(a)=\pi(f)$, the mapping
$(a, f)\mapsto a\cdot f$ is a continuous mapping from $\Aa+\Ff$ to $\Ff$.

If $\Ff$ is a sheaf of $\Aa$-modules, $\Gamma(U, \Ff)$ is a unitary module over 
$\Gamma(U, \Aa)$. Conversely, if $\Aa$ is defined by the system $(\Aa_U, \phi_U^V)$
as above, and let $\Ff$ be a sheaf defined by the system $(\Ff, \psi_U^V)$, where
every $\Ff_U$ is a unitary $\Aa_U$-module, with $\psi_U^V(a\cdot f) = \phi_U^V(a)\cdot
\psi_U^V(f)$; then $\Ff$ is a sheaf of $\Aa$-modules.

Every sheaf of abelian groups can be considered a sheaf of $\mathbb{Z}$-modules,
$\mathbb{Z}$ being the constant sheaf isomorphic to the ring of integers. This will
allow us to narrow our study to sheaves of modules from now on.

\ssubsection{Subsheaf and quotient sheaf}
\label{section-\arabic{subsection}}

Let $\Aa$ be a sheaf of rings, $\Ff$ a sheaf of $\Aa$-modules. For all $x\in X$,
let $\Gg_x$ be a subset of $\Ff_x$. We say that $\Gg = \bigcup \Gg_x$ is a \emph{subsheaf}
of $\Ff$ if:

{\bf (a)} \emph{$\Gg_x$ is a sub-$\Aa_x$-module of $\Ff_x$ for all $x\in X$,} 

{\bf (b)} \emph{$\Gg$ is an open subset of $\Ff$.} 

\noindent Condition (b) can be also expressed as:

{\bf (b')} \emph{If $x$ is a point of $X$, and if $s$ is a section of $\Ff$ over
a neighborhood of $x$ such that $s(x) \in \Gg_x$, we have $s(y)\in \Gg_y$ for all
$y$ close enough to $x$.}

It is clear that, if these conditions are satisfied, $\Gg$ is a sheaf of $\Aa$-modules.

Let $\Gg$ be a subsheaf of $\Ff$ and put $\Kk_x = \Ff_x / \Gg_x$ for all $x\in X$. Give
$\Kk = \bigcup \Kk_x$ the quotient topology of $\Ff$; we see easily that we also obtain 
a sheaf of $\Aa$-modules, called the \emph{quotient sheaf} of $\Ff$ by $\Gg$, and denoted
by $\Ff/\Gg$. We can give another definition, using the methods of \no \ref{section-3}: 
%
% page 204
%
if $U$ is
an open subset of $X$, set $\Kk_U = \Gamma(U, \Ff)/\Gamma(U, \Gg)$ and let $\phi_U^V$ 
a homomorphism obtained by passing to the quotient with $\rho_U^V: \Gamma(V, \Ff)\to
\Gamma(U, \Ff)$; the sheaf defined by the system $(\Kk_U, \phi_U^V)$ coincides with $\Kk$.

The second definition of $\Kk$ shows that, if $s$ is a section of $\Kk$ over a neighborhood
of $x$, there exists a section $t$ of $\Ff$ over a neighborhood of $x$ such that
the class of $t(y) \mod \Gg_y$ is equal to $s(y)$ for all $y$ close enough to $x$. 
Of course, this does not hold globally in general: if $U$ is an open subset of $X$ we
only have an exact sequence
\[ 0 \to \Gamma(U, \Gg) \to \Gamma(U, \Ff) \to \Gamma(U, \Kk), \]
the homomorphism $\Gamma(U, \Ff)\to \Gamma(U, \Kk)$ not being surjective in general
(cf. \no \ref{section-24}).

\ssubsection{Homomorphisms} 
\label{section-\arabic{subsection}}

Let $\Aa$ be a sheaf of rings, $\Ff$ and $\Gg$ two sheaves of $\Aa$-modules. An
\emph{$\Aa$-homomorphism} (or an $\Aa$-linear homomorphism, or simply a homomorphism)
from $\Ff$ to $\Gg$ is given by, for all $x\in X$, an $\Aa_x$-homomorphism
$\phi_x: \Ff_x\to\Gg_x$, such that the mapping $\phi: \Ff\to\Gg$ defined by the $\phi_x$
is continuous. This condition can also be expressed by saying that, if $s$ is a section
of $\Ff$ over $U$, $x\mapsto \phi_x(s(x))$ is a section of $\Gg$ over $U$ (we denote
this section by $\phi(s)$, or $\phi\circ s$). For example, if $\Gg$ is a subsheaf
of $\Ff$, the injection $\Gg\to \Ff$ and the projection $\Ff\to\Ff/\Gg$ both 
are homomorphisms.

{\bf Proposition 7.} \emph{Let $\phi$ be a homomorphisms from $\Ff$ to $\Gg$. For all
$x \in X$, let $\Nn_x$ be the kernel of $\phi_x$  and let $\Ii_x$ be the image of
$\phi_x$. Then $\Nn = \bigcup \Nn_x$ is a subsheaf of $\Ff$, $\Ii=\bigcup \Ii_x$
is a subsheaf of $\Gg$ and $\phi$ defines an isomorphism of $\Ff/\Nn$ and $\Ii$. }

Since $\phi_x$ is an $\Aa_x$-homomorphism, $\Nn_x$ and $\Ii_x$ are submodules of
$\Ff$ and $\Gg$ respectively, and $\phi_x$ defines an isomorphism of $\Ff_x/\Nn_x$
with $\Ii_x$. If on the other hand $s$ is a local section of $\Ff$, such that
$s(x) \in \Nn_x$, we have $\phi\circ s(x) = 0$, hence $\phi\circ s(y)=0$ for $y$
close enough to $x$, so $s(y)\in \Nn_y$, which shows that $\Nn$ is a subsheaf
of $\Ff$. If $t$ is a local section of $\Gg$, such that $t(x)\in \Ii_x$, there exists
a local section $s\in \Ff$, such that $\phi\circ s(x) = t(x)$, hence $\phi\circ s=t$
in the neighborhood of $x$, showing that $\Ii$ is a subsheaf of $\Gg$, isomorphic
with $\Ff/\Nn$.

The sheaf $\Nn$ is called the \emph{kernel} of $\phi$ and denoted by $\Ker(\phi)$;
the sheaf $\Ii$ is called the \emph{image} of $\phi$ and denoted by $\Img(\phi)$; the
sheaf $\Gg/\Ii$ is called the \emph{cokernel} of $\phi$ and denoted by $\Coker(\phi)$. 
A homomorphism $\phi$ is called \emph{injective}, or one-to-one, if each $\phi_x$ is injective, or equivalently if $\Ker(\phi)=0$; it is called \emph{surjective} if
each $\phi_x$ is surjective, or equivalently if $\Coker(\phi)=0$; it is called
\emph{bijective} if it is both injective and surjective, and Proposition 7 shows
that it is an isomorphism of $\Ff$ and $\Gg$ and that $\phi^{-1}$ is a homomorphism.
All the definitions related to homomorphisms of modules translate naturally to sheaves
of modules; for example, a sequence of homomorphisms is called \emph{exact} if
the image of each homomorphisms coincides with the kernel of the homomorphism
following it. If $\phi: \Ff\to\Gg$ is a homomorphism, the sequences:
\[ 0 \to \Ker(\phi) \to \Ff \to \Img(\phi) \to 0  \]
\[ 0 \to Im(\phi) \to \Gg \to \Coker(\phi) \to 0  \]
are exact.

% page 205

If $\phi$ is a homomorphism from $\Ff$ to $\Gg$, the mapping $s\mapsto \phi\circ s$
is a $\Gamma(U, \Aa)$-homomorphism from $\Gamma(U, \Ff)$ to $\Gamma(U, \Gg)$. Conversely,
if $\Aa$, $\Ff$, $\Gg$ are defined by the systems $(\Aa_U, \phi_U^V)$, $(\Ff_U, \psi_U^V)$,
$(\Gg_U, \chi_U^V)$ as in \no \ref{section-6}, and take for every open $U\subset X$ an 
$\Aa_U$-homomorphism $\phi_U:\Ff_U\to\Gg_U$ such that $\chi_U^V\circ\phi_V = \phi_U\circ
\psi_U^V$ ; by passing to the inductive limit, the $\phi_U$ define a homomorphism
$\phi: \Ff\to\Gg$.

\ssubsection{The direct sum of two sheaves}
\label{section-\arabic{subsection}}

Let $\Aa$ be a sheaf of rings, $\Ff$ and $\Gg$ two sheaves of $\Aa$-modules; for all
$x\in X$, form the module $\Ff_x \ds \Gg_x$, the \emph{direct sum} of $\Ff_x$ and $\Gg_X$;
an element of $\Ff_x \ds \Gg_x$ is a pair $(f, g)$ with $f\in \Ff_x$ and $g\in \Gg_x$. 
Let $\Kk$ be the sum of the sets $\Ff_x \ds \Gg_x$ for $x\in X$; we can identify
$\Kk$ with the subset of $\Ff\times \Gg$ consisting of the pairs $(f, g)$ with
$\pi(f)=\pi(g)$. We give $\Kk$ the topology induced from $\Ff\times\Gg$ and verify
immediately that $\Kk$ is a sheaf of $\Aa$-modules; we call this sheaf the \emph{direct
sum} of $\Ff$ and $\Gg$, and denote it by $\Ff\ds\Gg$. A section of $\Ff\ds\Gg$ is
of the form $x\mapsto (s(x), t(x))$, where $s$ and $t$ are sections of $\Ff$ and $\Gg$
over $U$; in other words, $\Gamma(U, \Ff\ds\Gg)$ is isomorphic to the direct sum
$\Gamma(U, \Ff)\ds\Gamma(U, \Gg)$. 

The definition of the direct sum extends by recurrence to a finite number of $\Aa$-modules.
In particular, a direct sum of $p$ sheaves isomorphic to one sheaf $\Ff$ is denoted
by $\Ff^p$.

\ssubsection{The tensor product of two sheaves}
\label{section-\arabic{subsection}}

Let $\Aa$ be a sheaf of rings, $\Ff$ a sheaf right of $\Aa$-modules, $\Gg$ a sheaf
of left $\Aa$-modules. For all $x\in X$ we set $\Kk_x = \Ff_x\otimes\Gg_x$, the
tensor product being taken over the ring $\Aa_x$ (cf. for example \cite{6}, Chapter II,
\S 2); let $\Kk$ be the sum of the sets $\Kk_x$.

{\bf Proposition 8.} \emph{There exists a structure of a sheaf on $\Kk$, unique with the
property that if $s$ and $t$ are sections of $\Ff$ and $\Gg$ over an open subset $U$,
the mapping $x\mapsto s(x)\otimes t(x)\in \Kk_x$ gives a section of $\Kk$ over $U$.}

\emph{The sheaf $\Kk$ thus defined is called the tensor product (over $\Aa$) of $\Ff$
and $\Gg$, and denoted by $\Ff\otimes_{\Aa}\Gg$; if the rings $\Aa_x$ are commutative,
it is a sheaf of $\Aa$-modules.}

If $\Kk$ has a structure of a sheaf satisfying the above condition, and if $f_i$ and $g_i$
are sections of $\Ff$ and $\Gg$ over an open $U\subset X$, the mapping 
$x \mapsto \sum s_i(x)\otimes t_i(x)$ is a section of $\Kk$ on $U$. In fact, all $h\in \Kk_x$
can be expressed in the form $h=\sum f_i\otimes g_i$, $f_i\in \Ff_X$, $g_i\in \Gg_x$, therefore also the form $\sum s_i(x)\otimes t_i(x)$, where $s_i$ and $t_i$ are defined
in an open neighborhood $U$ of $x$; in result, every section of $\Kk$ can be locally
expressed in the preceding form, which shows the uniqueness of the structure of a sheaf
on $\Kk$.

Now we show the existence. We might assume that $\Aa$, $\Ff$, $\Gg$ are defined by the systems $(\Aa_U, \phi_U^V)$, $(\Ff_U, \psi_U^V)$,
$(\Gg_U, \chi_U^V)$ as in \no \ref{section-6}. Now set $\Kk_U = \Ff_U\otimes \Gg_U$, the tensor product
being taken over $\Aa_U$ ; the homomorphisms $\psi_U^V$ and $\chi_U^V$ define, by
passing to the tensor product, a homomorphism $\eta_U^V: \Kk_V\to\Kk_U$; besides,
we have $\lim_{x\in U} \Kk_U = \lim_{x\in U} \Ff_U\otimes \lim_{x\in U}\Gg_U = \Kk_x$,
the tensor product being taken over $\Aa_x$ (for the commutativity of the tensor product
with inductive limits, see for example \cite{6}, Chapter VI, Exercise 18). The sheaf
defined by the system $(\Kk_U, \eta_U^V)$ can be identified with $\Kk$, and $\Kk$ is 
thus given a structure of a sheaf obviously satisfying the imposed condition. Finally,
%
% page 206
%
if the $\Aa_x$ are commutative, we can suppose that the $\Aa_U$ are also commutative
(it suffices to take for $\Aa_U$ the ring $\Gamma(U, \Aa)$), so $\Kk_U$ is a $\Aa_U$-module,
and $\Kk$ is a sheaf of $\Aa-modules$.

Now let $\phi$ be an $\Aa$-homomorphism from $\Ff$ to $\Ff'$ and let $\psi$ be an 
$\Aa$-homomorphism form $\Gg$ to $\Gg'$; in that case $\phi_x\otimes \psi_x$ is
a homomorphism (of abelian groups in general -- of $\Aa_x$-modules, if $\Aa_x$ is commutative) and the definition of $\Ff\otimes_\Aa \Gg$ shows that the collection
of $\phi_x\otimes \psi_x$ is a homomorphism from $\Ff\otimes_\Aa \Gg$ to
$\Ff'\otimes_\Aa \Gg'$; this homomorphism is denoted by $\phi\otimes\psi$; if $\psi$
is the identity, we write $\phi$ instead of $\phi \otimes 1$.

All of the usual properties of the tensor product of two modules translate to the
tensor product of two sheaves of modules. For example, all exact sequences:
\[ \Ff \to \Ff' \to \Ff'' \to 0 \]
give rise to an exact sequence:
\[ \Ff\otimes_\Aa \Gg \to \Ff'\otimes_\Aa \Gg \to \Ff''\otimes_\Aa \Gg \to 0. \]

We have the canonical isomorphisms:
\[ \Ff \otimes_\Aa (\Gg_1 \ds \Gg_2) \isom \Ff \otimes_\Aa \Gg_1 \ds \Ff \otimes_\Aa \Gg_2,
\quad \Ff \otimes_\Aa \Aa \isom \Ff, \]
and (supposing that $\Aa_x$ are commutative, to simplify the notation):
\[ \Ff \otimes_\Aa \Gg \isom \Gg \otimes_\Aa \Ff, \quad 
   \Ff \otimes_\Aa (\Gg \otimes_\Aa \Kk) \isom (\Ff \otimes_\Aa \Gg) \otimes_\Aa \Kk. \]

\ssubsection[The sheaf of germs of homomorphisms]{The sheaf of germs of homomorphisms
from one sheaf to another}
\label{section-\arabic{subsection}}

Let $\Aa$ be a sheaf of rings, $\Ff$ and $\Gg$ two sheaves of $\Aa$-modules. Let $U$
be an open subset of $X$. Let $\Kk_U$ be the group of homomorphisms from $\Ff(U)$
to $\Gg(U)$ (we also write ``homomorphism from $\Ff$ to $\Gg$ over $U$" in place of
``homomorphism from $\Ff(U)$ to $\Gg(U)$"). The operation of restricting a homomorphism
defines $\phi_U^V: \Kk_V\to\Kk_U$ ; the sheaf defined by $(\Kk_U, \phi_U^V)$ is called
the \emph{sheaf of germs of homomorphisms from $\Ff$ to $\Gg$} and denoted by
$\Hom_\Aa(\Ff, \Gg)$. If $\Aa_x$ are commutative, $\Hom_\Aa(\Ff, \Gg)$ is a sheaf
of $\Aa$-modules.

An element of $\Hom_\Aa(\Ff, \Gg)$, being a germ of a homomorphism from $\Ff$ to $\Gg$
in a neighborhood of $x$, defines an $\Aa_x$-homomorphism from $\Ff_x$ to $\Gg_x$ ;
hence a canonical homomorphism
\[ \rho: \Hom_\Aa(\Ff, \Gg)_x \to \Hom_{\Aa_x}(\Ff_x, \Gg_x). \]
But, contrary to what happened with the operations studied up to now, the homomorphism
$\rho$ is not a bijection in general; we will give in \no \ref{section-14} a sufficient condition
for that.

If $\phi: \Ff'\to\Ff$ and $\psi:\Gg\to\Gg'$ are homomorphisms, we define another homomorphism
\[ \Hom_\Aa(\phi, \psi): \Hom_\Aa(\Ff, \Gg) \to \Hom_\Aa(\Ff', \Gg'). \]

Every exact sequence $0\to\Gg\to\Gg'\to\Gg''$ gives rise to an exact sequence:
\[ 0 \to \Hom_\Aa(\Ff, \Gg) \to \Hom_\Aa(\Ff, \Gg') \to \Hom_\Aa(\Ff, \Gg''). \]

We also have the canonical isomorphisms: $\Hom_\Aa(\Aa, \Gg) \isom \Gg$, 
\[ \Hom_\Aa(\Ff, \Gg_1\ds\Gg_2) \isom \Hom_\Aa(\Ff, \Gg_1) \ds \Hom_\Aa(\Ff, \Gg_2) \]
\[ \Hom_\Aa(\Ff_1\ds\Ff_2, \Gg) \isom \Hom_\Aa(\Ff_1, \Gg) \ds \Hom_\Aa(\Ff_2, \Gg). \]

